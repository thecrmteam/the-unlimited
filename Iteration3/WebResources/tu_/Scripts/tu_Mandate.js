if (typeof (CrmJs) == "undefined") {
    CrmJs = { __namespace: true };
}

if (typeof (CrmJs.Sales) == "undefined") {
    CrmJs.Sales = { __namespace: true };
}
CrmJs.Sales.Mandate = {
    __namespace: true,
    FormContextObject: null,
    XrmObject: function () {
        if (typeof (Xrm) == "undefined")
            return parent.Xrm;
        else
            return Xrm;
    },
    OnLoad: function (executionContext) {
        var payer = this.XrmObject().Page.getAttribute("tu_payer");
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(payer)) {
                if (CrmJs.Common.GetFormType(executionContext) == 1) {
                    payer.setValue(null);
                }
                
            }
        }       
    },
    SetPayerDetails: function () {
        var isPrimaryPayer = this.XrmObject().Page.getAttribute("tu_isprimarypayer");
        var customer = this.XrmObject().Page.getAttribute("tu_customer");
        var payer = this.XrmObject().Page.getAttribute("tu_payer");
        var bank = this.XrmObject().Page.getAttribute("tu_bankdetails");
        var custId = ""; var filter = ""; var columns = ""; var bankingDetails = "";
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(isPrimaryPayer) && CrmJs.Common.IsValidAttributeControl(customer)) {
                if (isPrimaryPayer.getValue() == true) {
                    if (customer.getValue() != null) {
                        custId = customer.getValue()[0].id;
                        columns = "_tu_customer_value, tu_accountnumber,statecode";
                        filter = "_tu_customer_value eq '" + custId + "' and statecode eq 0";
                        bankingDetails = CrmJs.Common.RetrieveMultiple("tu_bankaccountdetailses", filter, columns);
                        if (bankingDetails != "undefined") {
                            if (bankingDetails.value.length == 1) {
                                if (CrmJs.Common.IsValidAttributeControl(bank)) {
                                    CrmJs.Common.SetLookupValue(bank, bankingDetails.value[0].tu_bankaccountdetailsid,
                                        bankingDetails.value[0].tu_accountnumber, "tu_bankaccountdetails");
                                    CrmJs.Common.SetDisabled(true, bank.getName());
                                    
                                }
                            }
                            else {
                                CrmJs.Common.SetDisabled(false, bank.getName());
                            }
                        }
                        if (CrmJs.Common.IsValidAttributeControl(payer)) {
                            CrmJs.Common.SetLookupValue(payer,customer.getValue()[0].id, customer.getValue()[0].name,
                                customer.getValue()[0].entityType);
                            CrmJs.Common.SetDisabled(true, payer.getName());
                            
                        }
                    }
                }
                else {
                    if (CrmJs.Common.IsValidAttributeControl(bank)) {
                        bank.setValue(null);
                    }
                    if (CrmJs.Common.IsValidAttributeControl(payer)) {
                        payer.setValue(null);
                        CrmJs.Common.SetDisabled(false, payer.getName());
                    }
                }
            }
        }
    },

    ValidateDebitDate: function () {
        var debitDate = this.XrmObject().Page.getAttribute("tu_debitdate");
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(debitDate) && CrmJs.Common.IsValidAttributeControl(debitDate.getValue())) {
                var date = new Date();
                if (debitDate.getValue() < date) {
                    this.XrmObject().Page.getControl("tu_debitdate").setNotification("DEBIT DATE CANNOT BE IN THE PAST");
                }
                else {
                    this.XrmObject().Page.getControl("tu_debitdate").clearNotification();
                }
            }
        }
    },

    OnChangePayer: function () {
        var payer = this.XrmObject().Page.getAttribute("tu_payer");
        var bank = CrmJs.Sales.Mandate.XrmObject().Page.getAttribute("tu_bankdetails");
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(payer) && CrmJs.Common.IsValidAttributeControl(bank)) {
                CrmJs.Common.SetDisabled(false, bank.getName());
                if (payer.getValue() != null) {
                    this.XrmObject().Page.getControl(bank.getName()).addPreSearch(CrmJs.Sales.Mandate.BankPreSearchHandler);
                }
                else {
                    this.XrmObject().Page.getControl(bank.getName()).removePreSearch(CrmJs.Sales.Mandate.BankPreSearchHandler);
                    bank.setValue(null);
                }
            }
        }
    },

    BankPreSearchHandler: function () {
        var bank = CrmJs.Sales.Mandate.XrmObject().Page.getAttribute("tu_bankdetails");
        var payer = CrmJs.Sales.Mandate.XrmObject().Page.getAttribute("tu_payer");
        var fetchXML = "";
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(bank) && CrmJs.Common.IsValidAttributeControl(payer)) {
                fetchXML = "<filter type='and'><condition attribute = 'tu_customer' operator= 'eq' value = '" + payer.getValue()[0].id + "' /></filter >";
                CrmJs.Common.AddCustomLookupFilter(bank.getName(), fetchXML);
            }
        }
    },
    

    PopulateDebitDate: function (executionContext) {
        if (CrmJs.Common != "undefined") {
            CrmJs.Common.SetFormContextObject(executionContext);
            var salaryPayDay = this.XrmObject().Page.getAttribute("tu_collectionday");

            if (CrmJs.Common.IsValidAttributeControl(salaryPayDay)
                && CrmJs.Common.IsValidAttributeControl(salaryPayDay.getValue())) {
                var dateToday = new Date();
                var debitDate = this.XrmObject().Page.getAttribute("tu_debitdate");

                if (CrmJs.Common.IsValidAttributeControl(debitDate)
                    && dateToday.getDate() > salaryPayDay.getValue()
                    && !CrmJs.Common.IsValidAttributeControl(debitDate.getValue())) {
                    debitDate.setValue(new Date(dateToday.getFullYear(), (dateToday.getMonth() + 1), salaryPayDay.getValue()));
                } else if (CrmJs.Common.IsValidAttributeControl(debitDate)
                    && !CrmJs.Common.IsValidAttributeControl(debitDate.getValue())) {
                    debitDate.setValue(new Date(dateToday.getFullYear(), dateToday.getMonth(), salaryPayDay.getValue()));
                }
            }
        }
    },

    PopCollection: function (SelectedEntityTypeName, PrimaryEntityTypeName, PrimaryControl, SelectedControl) {
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidValue(SelectedControl)) {
                var parameters = {};



                parameters["tu_customer"] = PrimaryControl;
                parameters["tu_customername"] = SelectedControl.data.entity.getEntityReference().name;
                parameters["tu_customertype"] = PrimaryEntityTypeName;



                parameters["tu_payer"] = PrimaryControl;
                parameters["tu_payername"] = SelectedControl.data.entity.getEntityReference().name;
                parameters["tu_payertype"] = PrimaryEntityTypeName;



                var pageInput = {
                    pageType: "entityrecord",
                    entityName: SelectedEntityTypeName,
                    data: parameters,
                    //entityId: ,
                    //formId: entityName == "contact" ?  :,
                };



                Xrm.Navigation.navigateTo(pageInput, CrmJs.Sales.Mandate.NavigationOptions()).then(
                    function success() {
                        SelectedControl.data.refresh();
                    },
                    function error(error) {
                        //CrmJs.Sales.Mandate.FormContextObject.ui.setFormNotification(error.message, "ERROR", "CREATE_BEN_ERROR");
                    }
                );
            }
        }
    },

    NavigationOptions: function () {
        var navigationOptions = {
            target: 2,
            height: { value: 100, unit: "%" },
            width: { value: 100, unit: "%" },
            position: 1
        };



        return navigationOptions;
    }
}