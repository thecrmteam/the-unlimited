if (typeof (CrmJs) == "undefined") {
    CrmJs = { __namespace: true };
}

if (typeof (CrmJs.Sales) == "undefined") {
    CrmJs.Sales = { __namespace: true };
}

CrmJs.Sales.Contact = {
    __namespace: true,
    FormContextObject: null,
    XrmObject: function () {
        if (typeof (Xrm) == "undefined")
            return parent.Xrm;
        else
            return Xrm;
    },

    ValidateIDNumber: function () {
        var idNumber = this.XrmObject().Page.getAttribute("tu_idnopassport");
        var isForeigner = this.XrmObject().Page.getAttribute("tu_isforeigner");
        var idNumberLength = 0;
        var isNumber = false;
        var citizenShip = 0;
        var lastDigit = 0;
        var controlDigit = 0;
        var age = 0;
        if (isForeigner != null) {
            if (CrmJs.Common.IsValidAttributeControl(idNumber.getValue())) {
                if (isForeigner.getValue() == false) {
                    idNumberLength = idNumber.getValue().length;
                    isNumber = this.IsNumber(idNumber.getValue());
                    if (idNumberLength == 13 && isNumber == true) {
                        this.XrmObject().Page.getControl("tu_idnopassport").clearNotification();
                        citizenShip = parseInt(idNumber.getValue().charAt(10));
                        controlDigit = this.GetControlDigit(idNumber);
                        if (controlDigit.toString().length > 1) {
                            lastDigit = controlDigit.toString().substr(controlDigit.toString().length - 1, 1);
                        }
                        else {
                            lastDigit = controlDigit.toString();
                        }
                        if (citizenShip > 1 || lastDigit != idNumber.getValue()[idNumberLength - 1]) {
                            this.XrmObject().Page.getControl("tu_idnopassport").setNotification("INVALID_ID_NUMBER");
                        }
                        else {
                            this.XrmObject().Page.getControl("tu_idnopassport").clearNotification();
                            this.PopulateDOB(idNumber);
                            this.PopulateGender(idNumber);
                            age = this.GetAge(idNumber);
                            if (age > 65) {
                                this.XrmObject().Page.getControl("tu_idnopassport").setNotification("CUSTOMER OVER 65");
                            }
                            else {
                                this.XrmObject().Page.getControl("tu_idnopassport").clearNotification();
                            }
                        }
                    }
                    else {
                        this.XrmObject().Page.getControl("tu_idnopassport").setNotification("INVALID_ID_NUMBER");
                    }
                }
                else {
                    this.XrmObject().Page.getControl("tu_idnopassport").clearNotification();
                }
            }
        }
    },

    OnLoad: function (executionContext) {
        CrmJs.Sales.Contact.DialerParameters(executionContext);
        CrmJs.Sales.Contact.SetCountry();
    },

    SetCountry: function () {
        var resCountry = this.XrmObject().Page.getAttribute("tu_countryresidential");
        var delCountry = this.XrmObject().Page.getAttribute("tu_countrydelivery");
        var posCountry = this.XrmObject().Page.getAttribute("tu_countrypostal");
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(resCountry) && resCountry.getValue() == null) {
                resCountry.setValue(0);
            }
            if (CrmJs.Common.IsValidAttributeControl(delCountry) && delCountry.getValue() == null) {
                delCountry.setValue(0);
            }
            if (CrmJs.Common.IsValidAttributeControl(posCountry) && posCountry.getValue() == null) {
                posCountry.setValue(0);
            }
        }
    },

    OnChangeIsForeigner: function () {
        var isForeigner = this.XrmObject().Page.getAttribute("tu_isforeigner");
        var idNumber = this.XrmObject().Page.getAttribute("tu_idnopassport");
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(isForeigner) && isForeigner.getValue() == false) {
                if (CrmJs.Common.IsValidAttributeControl(idNumber.getValue())) {
                    this.ValidateIDNumber();
                }
            }
            else if (CrmJs.Common.IsValidAttributeControl(isForeigner) && isForeigner.getValue() == true) {
                if (CrmJs.Common.IsValidAttributeControl(idNumber)) {
                    this.XrmObject().Page.getControl("tu_idnopassport").clearNotification();
                }
            }
        }
    },

    PopulateDOB: function (idNumber) {
        var birthDate = this.XrmObject().Page.getAttribute("birthdate");
        if (CrmJs.Common.IsValidAttributeControl(birthDate)) {

            var idNumYear = parseInt(idNumber.getValue().substr(0, 2));
            var idNumMonth = parseInt(idNumber.getValue().substr(2, 2));
            var idNumDay = parseInt(idNumber.getValue().substr(4, 2));

            birthDate.setValue(new Date(idNumYear, idNumMonth - 1, idNumDay));
        }
    },

    GetAge: function (idNumber) {
        var idNumYear = parseInt(idNumber.getValue().substr(0, 2));
        var idNumMonth = parseInt(idNumber.getValue().substr(2, 2));
        var idNumDay = parseInt(idNumber.getValue().substr(4, 2));
        var todayDate = new Date();
        var todayYear = todayDate.getFullYear();
        var todayMonth = todayDate.getMonth();

        var birthDate = new Date(idNumYear, idNumMonth - 1, idNumDay);

        var age = todayYear - birthDate.getFullYear();
        var m = todayDate.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && todayDate.getDate() < birthDate.getDate())) {
            age--;
        }

        return age;

    },

    PopulateGender: function (idNumber) {
        var gender = this.XrmObject().Page.getAttribute("gendercode");
        if (CrmJs.Common != "undefined" && CrmJs.Common.IsValidAttributeControl(gender)) {
            var genderNumber = parseInt(idNumber.getValue().substr(6, 1));
            if (genderNumber <= 4) {
                gender.setValue(2);
            }
            else if (genderNumber > 4 && genderNumber <= 9) {
                gender.setValue(1);
            }
        }
    },

    IsNumber: function (str) {
        return !/\D/.test(str);
    },

    GetControlDigit: function (idNumber) {
        var d = -1;
        var a = 0;
        for (var i = 0; i < 6; i++) {
            a += parseInt(idNumber.getValue()[2 * i]);
        }

        var b = 0;
        for (var i = 0; i < 6; i++) {
            b = b * 10 + parseInt(idNumber.getValue()[2 * i + 1]);
        }
        b *= 2;

        var result = b.toString();
        var c = 0;
        for (var i = 0; i < result.length; i++) {
            c += parseInt(result[i]);
        }
        var lastSum = (a + c).toString();
        d = 10 - parseInt(lastSum[lastSum.length - 1]);

        return d;
    },

    PopulateDebitDate: function (executionContext) {
        if (CrmJs.Common != "undefined") {
            CrmJs.Common.SetFormContextObject(executionContext);
            var salaryPayDay = CrmJs.Common.FormContextObject.getAttribute("tu_collectionday");

            if (CrmJs.Common.IsValidAttributeControl(salaryPayDay)
                && CrmJs.Common.IsValidAttributeControl(salaryPayDay.getValue())) {
                var dateToday = new Date();
                var debitDate = CrmJs.Common.FormContextObject.getAttribute("tu_debitdate");

                if (CrmJs.Common.IsValidAttributeControl(debitDate)
                    && dateToday.getDate() > salaryPayDay.getValue()
                    && !CrmJs.Common.IsValidAttributeControl(debitDate.getValue())) {
                    debitDate.setValue(new Date(dateToday.getFullYear(), (dateToday.getMonth() + 1), salaryPayDay.getValue()));
                } else if (CrmJs.Common.IsValidAttributeControl(debitDate)
                    && !CrmJs.Common.IsValidAttributeControl(debitDate.getValue())) {
                    debitDate.setValue(new Date(dateToday.getFullYear(), dateToday.getMonth(), salaryPayDay.getValue()));
                }
            }
        }
    },

    PopulateBranchCode: function (executionContext) {
        if (CrmJs.Common != "undefined") {
            CrmJs.Common.SetFormContextObject(executionContext);
            var bank = CrmJs.Common.FormContextObject.getAttribute("tu_bankname");

            if (!CrmJs.Common.IsValidAttributeControl(bank)) return;

            if (CrmJs.Common.IsValidAttributeControl(bank)
                && CrmJs.Common.IsValidAttributeControl(bank.getValue())) {
                var branchCode = CrmJs.Common.FormContextObject.getAttribute("tu_branchcode");

                branchCode.setValue(bank.getValue().toString());
            }
        }
    },

    DialerParameters: function (executionContext) {
        if (CrmJs.Common != "undefined") {
            CrmJs.Common.SetFormContextObject(executionContext);
            CrmJs.Common.FormContextObject.ui.clearFormNotification("VOICE_LOG_ERROR");

            var description = CrmJs.Common.FormContextObject.getAttribute("description");
            if (!CrmJs.Common.IsValidAttributeControl(description)) return;

            if (CrmJs.Common.IsValidAttributeControl(description)
                && CrmJs.Common.IsValidAttributeControl(description.getValue())) {
                var dialerPar = CrmJs.Common.RemoveBrackets(description.getValue());

                if (dialerPar.length > 0) {
                    var dialerObj = dialerPar.split(',');
                    var leadId = null;
                    var merchCode = null;
                    var source = null;
                    var fileName = null;

                    for (var i = 0; i < dialerObj.length; i++) {
                        var parameter = dialerObj[i].split('=');

                        if (parameter.length > 1) {
                            switch (parameter[0]) {
                                case "LeadId":
                                    CrmJs.Common.FormContextObject.getAttribute("tu_leadid").setValue(parameter[1]);
                                    break;
                                case "MerchCode":
                                    CrmJs.Common.FormContextObject.getAttribute("tu_merchcode").setValue(parameter[1]);
                                    break;
                                case "Source":
                                    source = parameter[1];
                                    break;
                                case "Filename":
                                    CrmJs.Common.FormContextObject.getAttribute("tu_filename").setValue(parameter[1]);
                                    break;
                                case "telephoneNumber":
                                    CrmJs.Common.FormContextObject.getAttribute("mobilephone").setValue(parameter[1]);
                                    break;
                            }
                        }
                    }

                    //var entityId = CrmJs.Common.FormContextObject.data.entity.getId();					
                    //if(CrmJs.Common.IsValidValue(entityId)){
                    //		CrmJs.Sales.Contact.CreateVoiceLogNote(leadId,merchCode,source,fileName, CrmJs.Common.RemoveBrackets(entityId))
                    //	}
                }
            }
        }
    },

    CreateVoiceLogNote: function (LeadId, MerchCode, Source, Filename, EntityId) {
        var entity = {};
        entity.subject = "Sale Voice Log: " + LeadId;
        entity.notetext = Filename;
        entity.isdocument = false;
        entity["objectid_contact@odata.bind"] = "/contacts(" + EntityId + ")";
        entity.objecttypecode = 'contact';

        Xrm.WebApi.online.createRecord("annotation", entity).then(
            function success(result) {
                CrmJs.Common.FormContextObject.getAttribute("description").setValue(null);
            },
            function (error) {
                CrmJs.Common.FormContextObject.ui.setFormNotification(error.message, "ERROR", "VOICE_LOG_ERROR");
            }
        );
    },

    PopCaseForm: function (SelectedEntityTypeName, PrimaryEntityTypeName, FirstPrimaryId, PrimaryControl, SelectedControl) {
        if (PrimaryControl._entityName == "contact") {
            var parameters = {};
            parameters["customerid"] = FirstPrimaryId;
            parameters["customeridname"] = CrmJs.Common.FormContextObject.getAttribute("fullname").getValue();
            parameters["customeridtype"] = PrimaryEntityTypeName;

            var pageInput = {
                pageType: "entityrecord",
                entityName: "incident",
                data: parameters,
            };
            var navigationOptions = {
                target: 2,
                height: { value: 100, unit: "%" },
                width: { value: 100, unit: "%" },
                position: 1
            };
            Xrm.Navigation.navigateTo(pageInput, navigationOptions).then(
                function success(result) {
                    SelectedControl.refresh();
                },
                function error() {
                    // Handle errors
                }
            );
        }
    },

    CopyResidentialAddress: function () {
        var resStreet1 = this.XrmObject().Page.getAttribute("address1_line1");
        var resStreet2 = this.XrmObject().Page.getAttribute("address1_line2");
        var resStreet3 = this.XrmObject().Page.getAttribute("address1_line3");
        var resCity = this.XrmObject().Page.getAttribute("address1_city");
        var resSuburb = this.XrmObject().Page.getAttribute("address1_stateorprovince");
        var resCountry = this.XrmObject().Page.getAttribute("tu_countryresidential");
        var resPostalCode = this.XrmObject().Page.getAttribute("address1_postalcode");
        var copyResAddress = this.XrmObject().Page.getAttribute("tu_copyresidentialaddress");

        var delStreet1 = this.XrmObject().Page.getAttribute("address2_line1");
        var delStreet2 = this.XrmObject().Page.getAttribute("address2_line2");
        var delStreet3 = this.XrmObject().Page.getAttribute("address2_line3");
        var delCity = this.XrmObject().Page.getAttribute("address2_city");
        var delSuburb = this.XrmObject().Page.getAttribute("address2_stateorprovince");
        var delCountry = this.XrmObject().Page.getAttribute("tu_countrydelivery");
        var delPostalCode = this.XrmObject().Page.getAttribute("address2_postalcode");

        var posStreet1 = this.XrmObject().Page.getAttribute("address3_line1");
        var posStreet2 = this.XrmObject().Page.getAttribute("address3_line2");
        var posStreet3 = this.XrmObject().Page.getAttribute("address3_line3");
        var posCity = this.XrmObject().Page.getAttribute("address3_city");
        var posSuburb = this.XrmObject().Page.getAttribute("address3_stateorprovince");
        var posCountry = this.XrmObject().Page.getAttribute("tu_countrypostal");
        var posPostalCode = this.XrmObject().Page.getAttribute("address3_postalcode");

        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(copyResAddress)) {
                switch (copyResAddress.getValue()) {
                    case 827210000:
                        if (CrmJs.Common.IsValidAttributeControl(resStreet1) && CrmJs.Common.IsValidAttributeControl(delStreet1) && CrmJs.Common.IsValidAttributeControl(posStreet1)) {
                            delStreet1.setValue(resStreet1.getValue()); posStreet1.setValue(resStreet1.getValue());
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resStreet2) && CrmJs.Common.IsValidAttributeControl(delStreet2) && CrmJs.Common.IsValidAttributeControl(posStreet2)) {
                            delStreet2.setValue(resStreet2.getValue()); posStreet2.setValue(resStreet2.getValue());
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resStreet3) && CrmJs.Common.IsValidAttributeControl(delStreet3) && CrmJs.Common.IsValidAttributeControl(posStreet3)) {
                            delStreet3.setValue(resStreet3.getValue()); posStreet3.setValue(resStreet3.getValue());
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resCity) && CrmJs.Common.IsValidAttributeControl(delCity) && CrmJs.Common.IsValidAttributeControl(posCity)) {
                            delCity.setValue(resCity.getValue()); posCity.setValue(resCity.getValue());
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resSuburb) && CrmJs.Common.IsValidAttributeControl(delSuburb) && CrmJs.Common.IsValidAttributeControl(posSuburb)) {
                            delSuburb.setValue(resSuburb.getValue()); posSuburb.setValue(resSuburb.getValue());
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resCountry) && CrmJs.Common.IsValidAttributeControl(delCountry) && CrmJs.Common.IsValidAttributeControl(posCountry)) {
                            delCountry.setValue(resCountry.getValue()); posCountry.setValue(resCountry.getValue());
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resPostalCode) && CrmJs.Common.IsValidAttributeControl(delPostalCode) && CrmJs.Common.IsValidAttributeControl(posPostalCode)) {
                            delPostalCode.setValue(resPostalCode.getValue()); posPostalCode.setValue(resPostalCode.getValue());
                        }

                        break;

                    //Delivery
                    case 827210001:
                        if (CrmJs.Common.IsValidAttributeControl(resStreet1) && CrmJs.Common.IsValidAttributeControl(delStreet1) && CrmJs.Common.IsValidAttributeControl(posStreet1)) {
                            delStreet1.setValue(resStreet1.getValue()); posStreet1.setValue("");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resStreet2) && CrmJs.Common.IsValidAttributeControl(delStreet2) && CrmJs.Common.IsValidAttributeControl(posStreet2)) {
                            delStreet2.setValue(resStreet2.getValue()); posStreet2.setValue("");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resStreet3) && CrmJs.Common.IsValidAttributeControl(delStreet3) && CrmJs.Common.IsValidAttributeControl(posStreet3)) {
                            delStreet3.setValue(resStreet3.getValue()); posStreet3.setValue("");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resCity) && CrmJs.Common.IsValidAttributeControl(delCity) && CrmJs.Common.IsValidAttributeControl(posCity)) {
                            delCity.setValue(resCity.getValue()); posCity.setValue("");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resSuburb) && CrmJs.Common.IsValidAttributeControl(delSuburb) && CrmJs.Common.IsValidAttributeControl(posSuburb)) {
                            delSuburb.setValue(resSuburb.getValue()); posSuburb.setValue("");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resCountry) && CrmJs.Common.IsValidAttributeControl(delCountry) && CrmJs.Common.IsValidAttributeControl(posCountry)) {
                            delCountry.setValue(resCountry.getValue()); posCountry.setValue("");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resPostalCode) && CrmJs.Common.IsValidAttributeControl(delPostalCode) && CrmJs.Common.IsValidAttributeControl(posPostalCode)) {
                            delPostalCode.setValue(resPostalCode.getValue()); posPostalCode.setValue("");
                        }
                        break;

                    case 827210002:
                        if (CrmJs.Common.IsValidAttributeControl(resStreet1) && CrmJs.Common.IsValidAttributeControl(delStreet1) && CrmJs.Common.IsValidAttributeControl(posStreet1)) {
                            delStreet1.setValue(""); posStreet1.setValue(resStreet1.getValue());
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resStreet2) && CrmJs.Common.IsValidAttributeControl(delStreet2) && CrmJs.Common.IsValidAttributeControl(posStreet2)) {
                            delStreet2.setValue(""); posStreet2.setValue(resStreet2.getValue());
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resStreet3) && CrmJs.Common.IsValidAttributeControl(delStreet3) && CrmJs.Common.IsValidAttributeControl(posStreet3)) {
                            delStreet3.setValue(""); posStreet3.setValue(resStreet3.getValue());
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resCity) && CrmJs.Common.IsValidAttributeControl(delCity) && CrmJs.Common.IsValidAttributeControl(posCity)) {
                            delCity.setValue(""); posCity.setValue(resCity.getValue());
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resSuburb) && CrmJs.Common.IsValidAttributeControl(delSuburb) && CrmJs.Common.IsValidAttributeControl(posSuburb)) {
                            delSuburb.setValue(""); posSuburb.setValue(resSuburb.getValue());
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resCountry) && CrmJs.Common.IsValidAttributeControl(delCountry) && CrmJs.Common.IsValidAttributeControl(posCountry)) {
                            delCountry.setValue(""); posCountry.setValue(resCountry.getValue())
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resPostalCode) && CrmJs.Common.IsValidAttributeControl(delPostalCode) && CrmJs.Common.IsValidAttributeControl(posPostalCode)) {
                            delPostalCode.setValue(""); posPostalCode.setValue(resPostalCode.getValue());
                        }
                        break;
                    default:
                        if (CrmJs.Common.IsValidAttributeControl(resStreet1) && CrmJs.Common.IsValidAttributeControl(delStreet1) && CrmJs.Common.IsValidAttributeControl(posStreet1)) {
                            delStreet1.setValue(""); posStreet1.setValue("");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resStreet2) && CrmJs.Common.IsValidAttributeControl(delStreet2) && CrmJs.Common.IsValidAttributeControl(posStreet2)) {
                            delStreet2.setValue(""); posStreet2.setValue("");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resStreet3) && CrmJs.Common.IsValidAttributeControl(delStreet3) && CrmJs.Common.IsValidAttributeControl(posStreet3)) {
                            delStreet3.setValue(""); posStreet3.setValue("");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resCity) && CrmJs.Common.IsValidAttributeControl(delCity) && CrmJs.Common.IsValidAttributeControl(posCity)) {
                            delCity.setValue(""); posCity.setValue("");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resSuburb) && CrmJs.Common.IsValidAttributeControl(delSuburb) && CrmJs.Common.IsValidAttributeControl(posSuburb)) {
                            delSuburb.setValue(""); posSuburb.setValue("");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resCountry) && CrmJs.Common.IsValidAttributeControl(delCountry) && CrmJs.Common.IsValidAttributeControl(posCountry)) {
                            delCountry.setValue(""); posCountry.setValue("");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resPostalCode) && CrmJs.Common.IsValidAttributeControl(delPostalCode) && CrmJs.Common.IsValidAttributeControl(posPostalCode)) {
                            delPostalCode.setValue(""); posPostalCode.setValue("");
                        }

                }
            }
        }

    },

    CheckLetters: function (executionContext, attributeName) {
        if (CrmJs.Common != "undefined") {
            CrmJs.Common.SetFormContextObject(executionContext);
            if (!CrmJs.Common.IsLettersOnly(CrmJs.Common.FormContextObject.getAttribute(attributeName))) {
                CrmJs.Common.FormContextObject.getControl(attributeName).setNotification("LETTERS_ONLY");
            } else {
                CrmJs.Common.FormContextObject.getControl(attributeName).clearNotification();
            }
        }
    },
};