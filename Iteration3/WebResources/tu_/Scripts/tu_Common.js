if (typeof (CrmJs) == "undefined") {
    CrmJs = { __namespace: true };
}

CrmJs.Common = {
    __namespace: true,
    FormContextObject: null,
    ApiVersion: "/api/data/v9.1/",
    CrmUrl: null,
    GlobalContextObject: null,
    XrmObject: function () {
        if (typeof (Xrm) == "undefined")
            return parent.Xrm;
        else
            return Xrm;
    },

    /**
     * Sets the Dynamics 365 form execution context object
     * @param {context} executionContext
     */
    SetFormContextObject: function (executionContext) {
        if (executionContext != null) {
            CrmJs.Common.FormContextObject = executionContext.getFormContext();
        }
    },

    SetGlobalContextObject: function (XrmObject) {
        CrmJs.Common.GlobalContextObject = XrmObject.Utility.getGlobalContext();
        CrmJs.Common.SetCrmUrl();
    },

    /**
    * Sets the Dynamics 365 CRM url
    * */
    SetCrmUrl: function () {
        if (CrmJs.Common.GlobalContextObject != null)
            CrmJs.Common.CrmUrl = CrmJs.Common.GlobalContextObject.getClientUrl();
        else
            CrmJs.Common.CrmUrl = null;
    },

	 /**
     * Sets a value that indicates whether the tab and/or section is visible
     * @param {string} tabName Required. The name of the tab
     * @param {string} sectionName Optional. The name of the section
     * @param {boolean} visibility Required. Specify true to show the tab; false to hide the tab.
     */
    SetTabSectionVisibility: function (tabName, sectionName, visibility) {
        var tabCtrl = CrmJs.Common.FormContextObject.ui.tabs.get(tabName);
 
        if (CrmJs.Common.IsValidValue(tabCtrl)) {
            if (!CrmJs.Common.IsValidValue(sectionName))
                tabCtrl.setVisible(visibility);
            else {
                var sectionCtrl = tabCtrl.sections.get(sectionName);
                if (CrmJs.Common.IsValidValue(sectionCtrl)) {
                    try {
                        sectionCtrl.setVisible(visibility);
                        if (visibility)
                            tabCtrl.setVisible(visibility);
                    }
                    catch (e) {
                        //do nothing - just silently fail that we could not find the tab
                    }
                }
            }
        }
    },
	
	 /**
     * Checks whether the passed attribute has a value or not
     * @param {string} attributeName The attribute of the data value
     * @returns {boolean} Value indicating whether the passed attribute has a value or not
     */
    HasValue: function (attributeName) {
        return CrmJs.Common.FormContextObject.getAttribute(attributeName) != null && CrmJs.Common.FormContextObject.getAttribute(attributeName).getValue() != null ? true : false;
    },
 
    /**
     * Checks whether the passed value is valid or not
     * @param {object} value The data value
     * @returns {boolean} Value indicating whether the passed attribute has a value or not
     */
    IsValidValue: function (value) {
        if (value == null || value == "" || value == "undefined" || value == "null")
            return false;
        return true;
    },
	
	RemoveBrackets: function(guid){
		if(CrmJs.Common.IsValidValue(guid))
			return guid.replace(/[{}]/g, '');
		return null;
    },

    GetFormType: function (executionContext) {
        var formContext = executionContext.getFormContext();
        var formType = formContext.ui.getFormType();
        return formType;
    },
	
    SetControlVisibility: function (control, visibility) {
        if (CrmJs.Common.IsValidAttributeControl(control)) {
            control.setVisible(visibility);
        }
    },

    IsValidAttributeControl: function (fieldName) {
        if (fieldName == null || fieldName == "" || fieldName == "undefined" || fieldName == "null")
            return false;
        return true;
    },

    SetLookupValue: function (lookupControl, id, name, entityType) {
        if (lookupControl != null)
            lookupControl.setValue([{ id: id.replace(/[{}]/g, ''), name: name, entityType: entityType }]);
    },

    GetEnvironmentVariable: function (schemaName) {
        var defaultValue = null;

        var req = new XMLHttpRequest();
        req.open("GET", CrmJs.Common.CrmUrl + CrmJs.Common.ApiVersion + "environmentvariabledefinitions?$select=defaultvalue&$filter=schemaname eq '" + schemaName + "'", false);
        req.setRequestHeader("OData-MaxVersion", "4.0");
        req.setRequestHeader("OData-Version", "4.0");
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.setRequestHeader("Prefer", "odata.include-annotations=\"*\"");
        req.onreadystatechange = function () {
            if (this.readyState === 4) {
                req.onreadystatechange = null;
                if (this.status === 200) {
                    var results = JSON.parse(this.response);
                    for (var i = 0; i < results.value.length; i++) {
                        defaultValue = results.value[i]["defaultvalue"];
                        return defaultValue;
                    }
                } else {
                    //Xrm.Utility.alertDialog(this.statusText);
                    return defaultValue;
                }
            }
        };
        req.send();

        return defaultValue;
    },

    SetDisabled: function (bool, fieldName) {
        this.XrmObject().Page.getControl(fieldName).setDisabled(bool);
    },

    RetrieveMultiple: function (entityName, filter, columns) {
        var globalContext = Xrm.Utility.getGlobalContext();
        var colllection = "undefined";
        var req = new XMLHttpRequest();
        req.open("GET", globalContext.getClientUrl() + CrmJs.Common.ApiVersion + entityName + "?$select=" + columns + "&$filter=" + filter, false);
        req.setRequestHeader("OData-MaxVersion", "4.0");
        req.setRequestHeader("OData-Version", "4.0");
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.setRequestHeader("Prefer", "odata.include-annotations=\"*\"");
        req.onreadystatechange = function () {
            if (this.readyState === 4) {
                req.onreadystatechange = null;
                if (this.status === 200) {
                    var results = JSON.parse(this.response);
                    colllection = results;
                }
            }
        };
        req.send();
        return colllection;
    },

    AddCustomLookupFilter: function (field, fetchXml) {
        this.XrmObject().Page.getControl(field).addCustomFilter(fetchXml);
    },

    IsLettersOnly: function (attribute) {
        var letters = /^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/; 

        if (CrmJs.Common.IsValidValue(attribute.getValue())) {
            if (attribute.getValue().match(letters))
                return true;
            else
                return false;
        }
    },
}