﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TCT.D365.TheUnlimited.Plugins.Abstracts;
using TCT.D365.TheUnlimited.Plugins.Helpers;
using TCT.D365.TheUnlimited.Plugins.Implementations;

namespace TCT.D365.TheUnlimited.Plugins.Factories
{
    public class WarewolfRequestFactory : AbstractWarewolfRequest
    {
        public WarewolfRequestType WarewolfRequestType { get; set; }
        public string Request { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="warewolfRequestType"></param>
        /// <param name="request"></param>
        /// <param name="url"></param>
        public WarewolfRequestFactory(WarewolfRequestType warewolfRequestType, string request, string url) :
           base(url)
        {
            WarewolfRequestType = warewolfRequestType;
            Request = request;
            Url = url;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="warewolfRequestType"></param>
        /// <param name="request"></param>
        /// <param name="url"></param>
        /// <param name="apiKey"></param>
        public WarewolfRequestFactory(WarewolfRequestType warewolfRequestType, string request, string url, string apiKey) : 
            base(url, apiKey)
        {
            WarewolfRequestType = warewolfRequestType;
            Request = request;
            Url = url;
            ApiKey = apiKey;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="warewolfRequestType"></param>
        /// <param name="request"></param>
        /// <param name="url"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        public WarewolfRequestFactory(WarewolfRequestType warewolfRequestType, string request, string url, string userName, string password) : 
            base(url, userName, password)
        {
            WarewolfRequestType = warewolfRequestType;
            Request = request;
            UserName = userName;
            Password = password;
            Url = url;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string ExecuteWarewolfRequest()
        {
            var httpClient = new HttpClient();
            var response = string.Empty;

            try
            {
                switch (WarewolfRequestType)
                {
                    case WarewolfRequestType.ApiKey:
                        {
                            var apiKeyRequest = new ApiKeyRequest(httpClient, Url, ApiKey);
                            apiKeyRequest.Connect();

                            response = apiKeyRequest.Execute(Request).GetAwaiter().GetResult();
                            apiKeyRequest.Close();
                            break;
                        };
                    case WarewolfRequestType.BasicAuth:
                        {
                            var basicAuthRequest = new BasicAuthRequest(httpClient, Url, Password, UserName);
                            basicAuthRequest.Connect();

                            response = basicAuthRequest.Execute(Request).GetAwaiter().GetResult();
                            basicAuthRequest.Close();
                            break;
                        };
                    case WarewolfRequestType.None:
                        {
                            var noneRequest = new ViciRequest(httpClient, Url);
                            noneRequest.Connect();

                            response = noneRequest.Execute(Request).GetAwaiter().GetResult();
                            noneRequest.Close();
                            break;
                        };
                }
            }
            catch (Exception exc)
            {
                response = exc.Message;
            }

            return response;
        } 
    }
}
