﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCT.D365.TheUnlimited.Plugins.Helpers
{
    public enum WarewolfRequestType
    {
        ApiKey = 0,
        BasicAuth = 1,
        None = 2,
    }
}
