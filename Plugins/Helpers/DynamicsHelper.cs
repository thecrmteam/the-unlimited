﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using TCT.D365.TheUnlimited.Plugins.Abstract;
using TCT.D365.TheUnlimited.Plugins.Models;

namespace TCT.D365.TheUnlimited.Plugins.Helpers
{
    public class DynamicsHelper : AbstractDynamics
    {
        public DynamicsHelper(IOrganizationService organizationService, ITracingService tracingService) :
        base(organizationService, tracingService)
        {
            OrganizationService = organizationService;
            TracingService = tracingService;
        }

        internal OrganizationResponse ExecuteGlobalAction(string actionName, ParameterCollection paramCollection)
        {
            TracingService.Trace("Before ExecuteBusinessEvent Execute");
            var newActionReq = new OrganizationRequest()
            {
                RequestName = actionName,
                Parameters = paramCollection
            };

            var newActionRes = OrganizationService.Execute(newActionReq);
            TracingService.Trace("After ExecuteBusinessEvent Execute: " + newActionRes.Results["Message"]);
            return newActionRes;
        }

        #region Retrieve Methods 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        internal EntityCollection GetUserTeamsCol(Guid userId)
        {
            StringBuilder linkFetch = new StringBuilder();
            linkFetch.Append("<fetch version=\"1.0\" output-format=\"xml-platform\" mapping=\"logical\" distinct=\"true\">");
            linkFetch.Append("<entity name=\"team\">");
            linkFetch.Append("<attribute name=\"name\"/>");
            linkFetch.Append("<attribute name=\"teamid\"/>");
            linkFetch.Append("<attribute name=\"createdon\"/>");
            linkFetch.Append("<order attribute=\"createdon\" descending=\"true\"/>");
            linkFetch.Append("<link-entity name=\"teammembership\" from=\"teamid\" to=\"teamid\" visible=\"false\" intersect=\"true\">");
            linkFetch.Append("<filter type=\"and\">");
            linkFetch.Append("<condition attribute=\"systemuserid\" operator=\"eq\" value=\"" + userId + "\"/>");
            linkFetch.Append("</filter>");
            linkFetch.Append("</link-entity>");
            linkFetch.Append("</entity>");
            linkFetch.Append("</fetch>");

            TracingService.Trace("Before GetUserTeams ");
            var fetchExpression = new FetchExpression(linkFetch.ToString());
            return OrganizationService.RetrieveMultiple(fetchExpression);
        }

        internal EntityCollection GetLoggedOnUser(Guid userId)
        {
            StringBuilder linkFetch = new StringBuilder();
            linkFetch.Append("<fetch version=\"1.0\" output-format=\"xml-platform\" mapping=\"logical\" distinct=\"true\">");
            linkFetch.Append("<entity name=\"systemuser\">");
            linkFetch.Append("<attribute name=\"fullname\"/>");
            linkFetch.Append("<attribute name=\"systemuserid\"/>");
            linkFetch.Append("<attribute name=\"businessunitid\"/>");
            linkFetch.Append("<attribute name=\"tu_office\"/>");
            linkFetch.Append("<filter type=\"and\">");
            linkFetch.Append("<condition attribute=\"systemuserid\" operator=\"eq\" uitype=\"systemuser\" value=\"" + userId + "\" />");
            linkFetch.Append("</filter>");
            linkFetch.Append("</entity>");
            linkFetch.Append("</fetch>");

            TracingService.Trace("Before GetUserTeams ");
            var fetchExpression = new FetchExpression(linkFetch.ToString());
            return OrganizationService.RetrieveMultiple(fetchExpression);
        }

        internal EntityCollection GetOfficeCol(Guid teamId)
        {
            StringBuilder linkFetch = new StringBuilder();
            linkFetch.Append("<fetch version=\"1.0\" output-format=\"xml-platform\" mapping=\"logical\" distinct=\"true\">");
            linkFetch.Append("<entity name=\"team\">");
            linkFetch.Append("<attribute name=\"name\"/>");
            linkFetch.Append("<attribute name=\"teamid\"/>");
            linkFetch.Append("<attribute name=\"businessunitid\"/>");
            linkFetch.Append("<filter type=\"and\">");
            linkFetch.Append("<condition attribute=\"teamid\" operator=\"eq\" uitype=\"team\" value=\"" + teamId + "\" />");
            linkFetch.Append("</filter>");
            linkFetch.Append("</entity>");
            linkFetch.Append("</fetch>");

            TracingService.Trace("Before GetUserTeams ");
            var fetchExpression = new FetchExpression(linkFetch.ToString());
            return OrganizationService.RetrieveMultiple(fetchExpression);
        }

        internal EntityCollection GetCustomerMandates(Guid customerId)
        {
            StringBuilder linkFetch = new StringBuilder();
            linkFetch.Append("<fetch version=\"1.0\" output-format=\"xml-platform\" mapping=\"logical\" distinct=\"true\">");
            linkFetch.Append("<entity name=\"tu_collection\">");
            linkFetch.Append("<attribute name=\"tu_collectionid\"/>");
            linkFetch.Append("<attribute name=\"tu_name\"/>");
            linkFetch.Append("<attribute name=\"createdon\"/>");
            linkFetch.Append("<filter type=\"and\">");
            linkFetch.Append("<condition attribute=\"tu_customer\" operator=\"eq\" uitype=\"contact\" value=\"" + customerId + "\" />");
            linkFetch.Append("</filter>");
            linkFetch.Append("</entity>");
            linkFetch.Append("</fetch>");

            TracingService.Trace("Before GetCustomerMandates :" + customerId.ToString());
            var fetchExpression = new FetchExpression(linkFetch.ToString());
            return OrganizationService.RetrieveMultiple(fetchExpression);
        }

        internal EntityCollection GetAllTeams()
        {
            StringBuilder linkFetch = new StringBuilder();
            linkFetch.Append("<fetch version=\"1.0\" output-format=\"xml-platform\" mapping=\"logical\" distinct=\"true\">");
            linkFetch.Append("<entity name=\"team\">");
            linkFetch.Append("<attribute name=\"name\"/>");
            linkFetch.Append("<attribute name=\"teamid\"/>");
            linkFetch.Append("<attribute name=\"tu_qapercentage\"/>");
            linkFetch.Append("<attribute name=\"queueid\"/>");
            linkFetch.Append("<attribute name=\"createdon\"/>");
            linkFetch.Append("<order attribute=\"createdon\" descending=\"true\"/>");
            linkFetch.Append("<filter type=\"and\">");
            linkFetch.Append("<condition attribute=\"teamtype\" operator=\"eq\" value=\"0\"/>");
            linkFetch.Append("</filter>");
            linkFetch.Append("</entity>");
            linkFetch.Append("</fetch>");

            TracingService.Trace("Before GetAllTeams ");
            var fetchExpression = new FetchExpression(linkFetch.ToString());
            return OrganizationService.RetrieveMultiple(fetchExpression);
        }

        internal EntityCollection GetContactsByOffice(Guid teamId, DateTime createdon)
        {
            StringBuilder linkFetch = new StringBuilder();
            linkFetch.Append("<fetch version=\"1.0\" output-format=\"xml-platform\" mapping=\"logical\" distinct=\"true\">");
            linkFetch.Append("<entity name=\"contact\">");
            linkFetch.Append("<attribute name=\"fullname\"/>");
            linkFetch.Append("<attribute name=\"tu_owningoffice\"/>");
            linkFetch.Append("<attribute name=\"contactid\"/>");
            linkFetch.Append("<attribute name=\"createdon\"/>");
            linkFetch.Append("<filter type=\"and\">");
            linkFetch.Append("<condition attribute=\"createdon\" operator=\"on\" value=\"" + createdon + "\"/>");
            linkFetch.Append("<condition attribute=\"tu_owningoffice\" operator=\"eq\" value=\"" + teamId + "\"/>");
            linkFetch.Append("</filter>");
            linkFetch.Append("</entity>");
            linkFetch.Append("</fetch>");

            //TracingService.Trace("Before GetUserTeams ");
            var fetchExpression = new FetchExpression(linkFetch.ToString());
            return OrganizationService.RetrieveMultiple(fetchExpression);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="teamId"></param>
        /// <returns></returns>
        internal EntityCollection GetTeamPlansCol(Guid teamId)
        {
            StringBuilder linkFetch = new StringBuilder();
            linkFetch.Append("<fetch version=\"1.0\" output-format=\"xml-platform\" mapping=\"logical\" distinct=\"true\">");
            linkFetch.Append("<entity name=\"tu_plan\">");
            linkFetch.Append("<attribute name=\"tu_name\"/>");
            linkFetch.Append("<attribute name=\"tu_planid\"/>");
            linkFetch.Append("<attribute name=\"tu_premium\"/>");
            linkFetch.Append("<attribute name=\"tu_admincharges\"/>");
            linkFetch.Append("<attribute name=\"tu_pasplanid\"/>");
            linkFetch.Append("<attribute name=\"tu_initialpremium\"/>");
            linkFetch.Append("<link-entity name=\"tu_team_tu_plan\" from=\"tu_planid\" to=\"tu_planid\" visible=\"false\" intersect=\"true\">");
            linkFetch.Append("<filter type=\"and\">");
            linkFetch.Append("<condition attribute=\"teamid\" operator=\"eq\" value=\"" + teamId + "\"/>");
            linkFetch.Append("</filter>");
            linkFetch.Append("</link-entity>");
            linkFetch.Append("</entity>");
            linkFetch.Append("</fetch>");

            TracingService.Trace("Before GetTeamPlans");
            var fetchExpression = new FetchExpression(linkFetch.ToString());
            return OrganizationService.RetrieveMultiple(fetchExpression);
        }

        internal EntityCollection GetPlansPremiumSum(Guid teamId)
        {
            StringBuilder linkFetch = new StringBuilder();
            linkFetch.Append("<fetch version=\"1.0\" output-format=\"xml-platform\" mapping=\"logical\" distinct=\"true\" aggregate=\"true\">");
            linkFetch.Append("<entity name=\"tu_plan\">");
            linkFetch.Append("<attribute name=\"tu_premium\" alias=\"premium_sum\" aggregate=\"sum\"/>");
            linkFetch.Append("<attribute name=\"tu_initialpremium\" alias=\"initialPremium_sum\" aggregate=\"sum\"/>");
            linkFetch.Append("<link-entity name=\"tu_team_tu_plan\" from=\"tu_planid\" to=\"tu_planid\" visible=\"false\" intersect=\"true\">");
            linkFetch.Append("<filter type=\"and\">");
            linkFetch.Append("<condition attribute=\"teamid\" operator=\"eq\" value=\"" + teamId + "\"/>");
            linkFetch.Append("</filter>");
            linkFetch.Append("</link-entity>");
            linkFetch.Append("</entity>");
            linkFetch.Append("</fetch>");

            TracingService.Trace("Before GetPlansPremiumSum");
            var fetchExpression = new FetchExpression(linkFetch.ToString());
            return OrganizationService.RetrieveMultiple(fetchExpression);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        internal EntityCollection GetBenefitsCol(Guid planId)
        {
            StringBuilder linkFetch = new StringBuilder();
            linkFetch.Append("<fetch version=\"1.0\" output-format=\"xml-platform\" mapping=\"logical\" distinct=\"true\">");
            linkFetch.Append("<entity name=\"tu_benefit\">");
            linkFetch.Append("<attribute name=\"tu_id\"/>");
            linkFetch.Append("<attribute name=\"tu_benefitid\"/>");
            linkFetch.Append("<attribute name=\"tu_price\"/>");
            linkFetch.Append("<attribute name=\"tu_maxallowed\"/>");
            linkFetch.Append("<attribute name=\"tu_benefitcategory\"/>");
            linkFetch.Append("<link-entity name=\"tu_plan_tu_benefit_mapper\" from=\"tu_benefitid\" to=\"tu_benefitid\" visible=\"false\" intersect=\"true\">");
            linkFetch.Append("<filter type=\"and\">");
            linkFetch.Append("<condition attribute=\"tu_planid\" operator=\"eq\" value=\"" + planId + "\"/>");
            linkFetch.Append("</filter>");
            linkFetch.Append("</link-entity>");
            linkFetch.Append("</entity>");
            linkFetch.Append("</fetch>");

            TracingService.Trace("Before GetBenefitsCollection");
            var fetchExpression = new FetchExpression(linkFetch.ToString());
            return OrganizationService.RetrieveMultiple(fetchExpression);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        internal EntityCollection GetCustomerCol(Guid customerId)
        {
            StringBuilder linkFetch = new StringBuilder();
            linkFetch.Append("<fetch version=\"1.0\" output-format=\"xml-platform\" mapping=\"logical\" distinct=\"true\">");
            linkFetch.Append("<entity name=\"contact\">");
            linkFetch.Append("<attribute name=\"contactid\"/>");
            linkFetch.Append("<attribute name=\"mobilephone\"/>");
            linkFetch.Append("<attribute name=\"lastname\"/>");
            linkFetch.Append("<attribute name=\"tu_title\"/>");
            linkFetch.Append("<attribute name=\"tu_isforeigner\"/>");
            linkFetch.Append("<attribute name=\"tu_idnopassport\"/>");
            linkFetch.Append("<attribute name=\"tu_accounttype\"/>");
            linkFetch.Append("<attribute name=\"firstname\"/>");
            linkFetch.Append("<attribute name=\"emailaddress1\"/>");
            linkFetch.Append("<attribute name=\"birthdate\"/>"); 
            linkFetch.Append("<attribute name=\"tu_customernumber\"/>");
            linkFetch.Append("<attribute name=\"tu_fixedextractdate\"/>");
            linkFetch.Append("<attribute name=\"address1_line1\"/>");
            linkFetch.Append("<attribute name=\"address1_line2\"/>");
            linkFetch.Append("<attribute name=\"address1_line3\"/>");
            linkFetch.Append("<attribute name=\"address1_city\"/>");
            linkFetch.Append("<attribute name=\"address1_stateorprovince\"/>");
            linkFetch.Append("<attribute name=\"tu_countryresidential\"/>");
            linkFetch.Append("<attribute name=\"address1_postalcode\"/>");
            linkFetch.Append("<attribute name=\"address2_line1\"/>");
            linkFetch.Append("<attribute name=\"address2_line2\"/>");
            linkFetch.Append("<attribute name=\"address2_line3\"/>");
            linkFetch.Append("<attribute name=\"address2_city\"/>");
            linkFetch.Append("<attribute name=\"address2_stateorprovince\"/>");
            linkFetch.Append("<attribute name=\"tu_countrydelivery\"/>");
            linkFetch.Append("<attribute name=\"address2_postalcode\"/>");
            linkFetch.Append("<attribute name=\"address3_line1\"/>");
            linkFetch.Append("<attribute name=\"address3_line2\"/>");
            linkFetch.Append("<attribute name=\"address3_line3\"/>");
            linkFetch.Append("<attribute name=\"address3_city\"/>");
            linkFetch.Append("<attribute name=\"address3_stateorprovince\"/>");
            linkFetch.Append("<attribute name=\"tu_countrypostal\"/>");
            linkFetch.Append("<attribute name=\"address3_postalcode\"/>");
            linkFetch.Append("<filter type=\"and\">");
            linkFetch.Append("<condition attribute=\"contactid\" operator=\"eq\" uitype=\"contact\" value=\"" + customerId + "\" />");
            linkFetch.Append("</filter>");
            linkFetch.Append("</entity>");
            linkFetch.Append("</fetch>");
            
            TracingService.Trace("Before GetCustomerCol :" + customerId.ToString());
            var fetchExpression = new FetchExpression(linkFetch.ToString());
            return OrganizationService.RetrieveMultiple(fetchExpression);
        }

        internal EntityCollection GetMandatePlansCol(Guid planId)
        {
            StringBuilder linkFetch = new StringBuilder();
            linkFetch.Append("<fetch version=\"1.0\" output-format=\"xml-platform\" mapping=\"logical\" distinct=\"true\">");
            linkFetch.Append("<entity name=\"tu_plan\">");
            linkFetch.Append("<attribute name=\"tu_planid\"/>");
            linkFetch.Append("<attribute name=\"tu_name\"/>");
            linkFetch.Append("<attribute name=\"tu_pasplanid\"/>");
            linkFetch.Append("<filter type=\"and\">");
            linkFetch.Append("<condition attribute=\"tu_planid\" operator=\"eq\" uitype=\"tu_plan\" value=\"" + planId + "\" />");
            linkFetch.Append("</filter>");
            linkFetch.Append("</entity>");
            linkFetch.Append("</fetch>");

            TracingService.Trace("Before GetCustomerCol :" + planId.ToString());
            var fetchExpression = new FetchExpression(linkFetch.ToString());
            return OrganizationService.RetrieveMultiple(fetchExpression);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        internal EntityCollection GetActiveCustomerBenefitsCol(Guid customerId)
        {
            StringBuilder linkFetch = new StringBuilder();
            linkFetch.Append("<fetch version=\"1.0\" output-format=\"xml-platform\" mapping=\"logical\" distinct=\"true\">");
            linkFetch.Append("<entity name=\"tu_customerbenefit\">");
            linkFetch.Append("<attribute name=\"tu_benefitnumber\"/>");
            linkFetch.Append("<attribute name=\"tu_customerbenefitid\"/>");
            linkFetch.Append("<attribute name=\"tu_mandate\"/>");
            linkFetch.Append("<attribute name=\"tu_benefitcategory\"/>");
            linkFetch.Append("<filter type=\"and\">");
            linkFetch.Append("<condition attribute=\"tu_customer\" operator=\"eq\" uitype=\"contact\" value=\"" + customerId + "\" />");
            linkFetch.Append("<condition attribute=\"statecode\" operator=\"eq\" value=\"0\" />");
            linkFetch.Append("</filter>");
            linkFetch.Append("<link-entity name=\"tu_benefit\" from=\"tu_benefitid\" to=\"tu_benefit\" visible=\"false\" link-type=\"outer\" alias=\"Benefit\">");
            linkFetch.Append("<attribute name=\"tu_id\" />");
            linkFetch.Append("<attribute name=\"tu_name\" />");
            linkFetch.Append("</link-entity>");
            linkFetch.Append("</entity>");
            linkFetch.Append("</fetch>");

            TracingService.Trace("Before GetCustomerBenefitsCol :" + customerId.ToString());
            var fetchExpression = new FetchExpression(linkFetch.ToString());
            return OrganizationService.RetrieveMultiple(fetchExpression);
        }

        internal EntityCollection GetActiveMandateBenefitsCol(Guid mandateId)
        {
            StringBuilder linkFetch = new StringBuilder();
            linkFetch.Append("<fetch version=\"1.0\" output-format=\"xml-platform\" mapping=\"logical\" distinct=\"true\">");
            linkFetch.Append("<entity name=\"tu_customerbenefit\">");
            linkFetch.Append("<attribute name=\"tu_benefitnumber\"/>");
            linkFetch.Append("<attribute name=\"tu_customerbenefitid\"/>");
            linkFetch.Append("<attribute name=\"tu_mandate\"/>");
            linkFetch.Append("<attribute name=\"tu_benefitcategory\"/>");
            linkFetch.Append("<filter type=\"and\">");
            linkFetch.Append("<condition attribute=\"tu_mandate\" operator=\"eq\" uitype=\"tu_collection\" value=\"" + mandateId + "\" />");
            linkFetch.Append("<condition attribute=\"statecode\" operator=\"eq\" value=\"0\" />");
            linkFetch.Append("</filter>");
            linkFetch.Append("<link-entity name=\"tu_benefit\" from=\"tu_benefitid\" to=\"tu_benefit\" visible=\"false\" link-type=\"outer\" alias=\"Benefit\">");
            linkFetch.Append("<attribute name=\"tu_id\" />");
            linkFetch.Append("<attribute name=\"tu_name\" />");
            linkFetch.Append("</link-entity>");
            linkFetch.Append("</entity>");
            linkFetch.Append("</fetch>");

            TracingService.Trace("Before GetCustomerBenefitsCol :" + mandateId.ToString());
            var fetchExpression = new FetchExpression(linkFetch.ToString());
            return OrganizationService.RetrieveMultiple(fetchExpression);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bankingId"></param>
        /// <returns></returns>
        internal EntityCollection GetBankingDetails(Guid bankingId)
        {
            StringBuilder linkFetch = new StringBuilder();
            linkFetch.Append("<fetch version=\"1.0\" output-format=\"xml-platform\" mapping=\"logical\" distinct=\"true\">");
            linkFetch.Append("<entity name=\"tu_bankaccountdetails\">");
            linkFetch.Append("<attribute name=\"tu_bankaccountdetailsid\"/>");
            linkFetch.Append("<attribute name=\"tu_accountnumber\"/>");
            linkFetch.Append("<attribute name=\"tu_branchcode\"/>");
            linkFetch.Append("<attribute name=\"tu_bankname\"/>");
            linkFetch.Append("<attribute name=\"tu_accounttype\"/>");
            linkFetch.Append("<filter type=\"and\">");
            linkFetch.Append("<condition attribute=\"tu_bankaccountdetailsid\" operator=\"eq\" uitype=\"tu_bankaccountdetails\" value=\"" + bankingId + "\" />");
            linkFetch.Append("</filter>");
            linkFetch.Append("</entity>");
            linkFetch.Append("</fetch>");

            TracingService.Trace("Before GetBankingDetails :" + bankingId.ToString());
            var fetchExpression = new FetchExpression(linkFetch.ToString());
            return OrganizationService.RetrieveMultiple(fetchExpression);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customerBenefitId"></param>
        /// <returns></returns>
        internal EntityCollection GetSIMDetails(Guid customerBenefitId)
        {
            StringBuilder linkFetch = new StringBuilder();
            linkFetch.Append("<fetch version=\"1.0\" output-format=\"xml-platform\" mapping=\"logical\" distinct=\"true\">");
            linkFetch.Append("<entity name=\"tu_sim\">");
            linkFetch.Append("<attribute name=\"tu_iccid\"/>");
            linkFetch.Append("<attribute name=\"tu_mobilenumber\"/>");
            linkFetch.Append("<attribute name=\"tu_linktosim\"/>");
            linkFetch.Append("<attribute name=\"tu_name\"/>");
            linkFetch.Append("<filter type=\"and\">");
            linkFetch.Append("<condition attribute=\"tu_customerbenefit\" operator=\"eq\" uitype=\"tu_customerbenefit\" value=\"" + customerBenefitId + "\" />");
            linkFetch.Append("</filter>");
            linkFetch.Append("</entity>");
            linkFetch.Append("</fetch>");

            TracingService.Trace("Before GetSIMDetails :" + customerBenefitId.ToString());
            var fetchExpression = new FetchExpression(linkFetch.ToString());
            return OrganizationService.RetrieveMultiple(fetchExpression);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customerBenefitId"></param>
        /// <returns></returns>
        internal EntityCollection GetDependants(Guid customerBenefitId)
        {
            StringBuilder linkFetch = new StringBuilder();
            linkFetch.Append("<fetch version=\"1.0\" output-format=\"xml-platform\" mapping=\"logical\" distinct=\"true\">");
            linkFetch.Append("<entity name=\"contact\">");
            linkFetch.Append("<attribute name=\"contactid\"/>");
            linkFetch.Append("<attribute name=\"telephone1\"/>");
            linkFetch.Append("<attribute name=\"tu_title\"/>");
            linkFetch.Append("<attribute name=\"tu_idnopassport\"/>");
            linkFetch.Append("<attribute name=\"firstname\"/>");
            linkFetch.Append("<attribute name=\"emailaddress1\"/>");
            linkFetch.Append("<attribute name=\"birthdate\"/>");
            linkFetch.Append("<attribute name=\"tu_customerbenefit\"/>");
            linkFetch.Append("<attribute name=\"tu_type\"/>");
            linkFetch.Append("<attribute name=\"lastname\"/>");
            linkFetch.Append("<filter type=\"and\">");
            linkFetch.Append("<condition attribute=\"tu_customerbenefit\" operator=\"eq\" uitype=\"tu_customerbenefit\" value=\"" + customerBenefitId + "\" />");
            linkFetch.Append("</filter>");
            linkFetch.Append("</entity>");
            linkFetch.Append("</fetch>");

            TracingService.Trace("Before GetDependants :" + customerBenefitId.ToString());
            var fetchExpression = new FetchExpression(linkFetch.ToString());
            return OrganizationService.RetrieveMultiple(fetchExpression);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        internal EntityCollection GetCollectionDetailsCol(Guid customerId)
        {
            StringBuilder linkFetch = new StringBuilder();
            linkFetch.Append("<fetch version=\"1.0\" output-format=\"xml-platform\" mapping=\"logical\" distinct=\"true\">");
            linkFetch.Append("<entity name=\"tu_collection\">");
            linkFetch.Append("<attribute name=\"tu_name\"/>");
            linkFetch.Append("<attribute name=\"tu_premium\"/>");
            linkFetch.Append("<attribute name=\"tu_postpone1stdebit\"/>");
            linkFetch.Append("<attribute name=\"tu_payer\"/>");
            linkFetch.Append("<attribute name=\"tu_fixedextractdate\"/>");
            linkFetch.Append("<attribute name=\"tu_firstpremium\"/>");
            linkFetch.Append("<attribute name=\"tu_debitdate\"/>");
            linkFetch.Append("<attribute name=\"tu_collectionday\"/>");
            linkFetch.Append("<attribute name=\"tu_bankdetails\"/>");
            linkFetch.Append("<filter type=\"and\">");
            linkFetch.Append("<condition attribute=\"tu_customer\" operator=\"eq\" uitype=\"contact\" value=\"" + customerId + "\" />");
            linkFetch.Append("</filter>");
            //linkFetch.Append("<link-entity name =\"contact\" from=\"contactid\" to=\"tu_customer\" visible=\"false\" link-type=\"outer\" alias=\"Contact\">");
            //linkFetch.Append("<attribute name=\"tu_customernumber\" />");
            //linkFetch.Append("</link-entity>");
            linkFetch.Append("</entity>");
            linkFetch.Append("</fetch>");

            TracingService.Trace("Before GetCollectionDetailsCol :" + customerId.ToString());
            var fetchExpression = new FetchExpression(linkFetch.ToString());
            return OrganizationService.RetrieveMultiple(fetchExpression);
        }

        internal EntityCollection GetBenefitDetails(Guid benefitId)
        {
            StringBuilder linkFetch = new StringBuilder();
            linkFetch.Append("<fetch version=\"1.0\" output-format=\"xml-platform\" mapping=\"logical\" distinct=\"true\">");
            linkFetch.Append("<entity name=\"tu_benefitdetails\">");
            linkFetch.Append("<attribute name=\"tu_benefitdetailsid\"/>");
            linkFetch.Append("<attribute name=\"tu_name\"/>");
            linkFetch.Append("<attribute name=\"tu_value\"/>");
            linkFetch.Append("<filter type=\"and\">");
            linkFetch.Append("<condition attribute=\"tu_benefit\" operator=\"eq\" value=\"" + benefitId + "\"/>");
            linkFetch.Append("</filter>");
            linkFetch.Append("</entity>");
            linkFetch.Append("</fetch>");

            TracingService.Trace("GetBenefitDetails :" + benefitId.ToString());
            var fetchExpression = new FetchExpression(linkFetch.ToString());
            return OrganizationService.RetrieveMultiple(fetchExpression);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mandateId"></param>
        /// <returns></returns>
        internal EntityCollection GetMandateCol(Guid mandateId)
        {
            StringBuilder linkFetch = new StringBuilder();
            linkFetch.Append("<fetch version=\"1.0\" output-format=\"xml-platform\" mapping=\"logical\" distinct=\"true\">");
            linkFetch.Append("<entity name=\"tu_collection\">");
            linkFetch.Append("<attribute name=\"tu_collectionid\"/>");
            linkFetch.Append("<attribute name=\"tu_name\"/>");
            linkFetch.Append("<attribute name=\"tu_premium\"/>");
            linkFetch.Append("<attribute name=\"tu_postpone1stdebit\"/>");
            linkFetch.Append("<attribute name=\"tu_mobilephone\"/>");
            linkFetch.Append("<attribute name=\"tu_lastname\"/>");
            linkFetch.Append("<attribute name=\"tu_isprimarypayer\"/>");
            linkFetch.Append("<attribute name=\"tu_isforeigner\"/>");
            linkFetch.Append("<attribute name=\"tu_plan\"/>");
            linkFetch.Append("<attribute name=\"tu_idno\"/>");
            linkFetch.Append("<attribute name=\"tu_fixedextractdate\"/>");
            linkFetch.Append("<attribute name=\"tu_firstpremium\"/>");
            linkFetch.Append("<attribute name=\"tu_firstname\"/>");
            linkFetch.Append("<attribute name=\"tu_emailaddress1\"/>");
            linkFetch.Append("<attribute name=\"tu_debitdate\"/>");
            linkFetch.Append("<attribute name=\"tu_customernumber\"/>");
            linkFetch.Append("<attribute name=\"tu_customer\"/>");
            linkFetch.Append("<attribute name=\"tu_collectionday\"/>");
            linkFetch.Append("<attribute name=\"tu_branchname\"/>");
            linkFetch.Append("<attribute name=\"tu_branchcode\"/>");
            linkFetch.Append("<attribute name=\"tu_birthdate\"/>");
            linkFetch.Append("<attribute name=\"tu_bankname\"/>");
            linkFetch.Append("<attribute name=\"tu_accounttype\"/>");
            linkFetch.Append("<attribute name=\"tu_accountnumber\"/>");
            linkFetch.Append("<filter type=\"and\">");
            linkFetch.Append("<condition attribute=\"tu_collectionid\" operator=\"eq\" uitype=\"tu_collection\" value=\"" + mandateId + "\" />");
            linkFetch.Append("</filter>");
            linkFetch.Append("</entity>");
            linkFetch.Append("</fetch>");

            TracingService.Trace("Before GetMandateCol :" + mandateId.ToString());
            var fetchExpression = new FetchExpression(linkFetch.ToString());
            return OrganizationService.RetrieveMultiple(fetchExpression);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contactId"></param>
        /// <returns></returns>
        internal EntityCollection GetContactDetails(Guid contactId)
        {
            #region (* Local variables *)
            string functionName = "GetContactDetails";
            EntityCollection contactCol = new EntityCollection();
            #endregion
            try
            {
                TracingService.Trace("Inside: " + functionName);

                string fetchXML = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                      <entity name='contact'>
                                        <attribute name='fullname' />
                                        <attribute name='contactid' />
                                        <attribute name='tu_idnopassport' />
                                        <order attribute='fullname' descending='false' />
                                        <filter type='and'>
                                          <condition attribute='contactid' operator='eq' uitype='contact' value='" + contactId + @"' />
                                        </filter>
                                      </entity>
                                    </fetch>";

                TracingService.Trace("Before GetContactDetails :" + contactId.ToString());
                var fetchExpression = new FetchExpression(fetchXML);
                contactCol = OrganizationService.RetrieveMultiple(fetchExpression);

                TracingService.Trace("Exited from: " + functionName);
            }
            catch (InvalidPluginExecutionException ex)
            {
                throw new InvalidPluginExecutionException(ex.InnerException != null && ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message);
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                throw new InvalidPluginExecutionException(ex.InnerException != null && ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message);
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException(ex.InnerException != null && ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message);
            }

            return contactCol;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idNumber"></param>
        /// <returns></returns>
        internal EntityCollection GetBlacklistCollection(string idNumber)
        {
            #region (* Local variables *)
            string functionName = "GetBlacklistCollection";
            EntityCollection blacklistCol = new EntityCollection();
            #endregion
            try
            {
                TracingService.Trace("Inside: " + functionName);

                string fetchXML = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                      <entity name='tu_blacklisting'>
                                        <attribute name='tu_blacklistingid' />
                                        <attribute name='tu_name' />
                                        <order attribute='tu_name' descending='false' />
                                        <filter type='and'>
                                          <condition attribute='tu_idno' operator='eq' value='" + idNumber + @"' />
                                          <condition attribute='statecode' operator='eq' value='0' />
                                        </filter>
                                      </entity>
                                    </fetch>";

                TracingService.Trace("Before GetBlacklistCollection :" + idNumber);
                var fetchExpression = new FetchExpression(fetchXML);
                blacklistCol = OrganizationService.RetrieveMultiple(fetchExpression);

                TracingService.Trace("Exited from: " + functionName);
            }
            catch (InvalidPluginExecutionException ex)
            {
                throw new InvalidPluginExecutionException(ex.InnerException != null && ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message);
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                throw new InvalidPluginExecutionException(ex.InnerException != null && ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message);
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException(ex.InnerException != null && ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message);
            }

            return blacklistCol;
        }
        #endregion

        #region Create Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="routingKey"></param>
        /// <param name="url"></param>
        /// <param name="response"></param>
        /// <param name="businessEvent"></param>
        /// <returns></returns>
        internal Guid CreateApiLog(string eventName, string url, string businessEventRes,string businessEventReq)
        {
            var apiLog = new Entity("tu_apilog")
            {
                Attributes = new AttributeCollection()
                            {
                                new KeyValuePair<string, object>("tu_eventname", eventName),
                                new KeyValuePair<string, object>("tu_url", url),
                                new KeyValuePair<string, object>("tu_request", businessEventReq),
                                new KeyValuePair<string, object>("tu_response", businessEventRes),
                            }
            };

            return OrganizationService.Create(apiLog);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="noteText"></param>
        internal void CreateNote(string subject, string noteText)
        {
            var note = new Entity("annotation")
            {
                Attributes =
                    {
                        new KeyValuePair<string, object>("subject", subject),
                        new KeyValuePair<string, object>("notetext", noteText),
                        new KeyValuePair<string, object>("isdocument", false),
                        new KeyValuePair<string, object>("objectid", new EntityReference(PluginExecutionContext.PrimaryEntityName,PluginExecutionContext.PrimaryEntityId)),
                        new KeyValuePair<string, object>("objecttypecode",PluginExecutionContext.PrimaryEntityName)
                    },
            };

            OrganizationService.Create(note);
            TracingService.Trace("Sale Voice Log Created");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="benefit"></param>
        /// <returns></returns>
        internal Guid CreateCustomerBenefits(Entity benefit, Entity customer,Entity plan)
        {
            var customerBenefit = new Entity("tu_customerbenefit");
            customerBenefit["tu_customer"] = new EntityReference(customer.LogicalName, customer.Id);
            customerBenefit["tu_plan"] = new EntityReference(plan.LogicalName, plan.Id);
            customerBenefit["tu_benefit"] = new EntityReference(benefit.LogicalName, benefit.Id);
            customerBenefit["tu_price"] = benefit.Attributes.Contains("tu_price") ? benefit["tu_price"] : 0;
            customerBenefit["tu_benefitnumber"] = customer.Attributes.Contains("tu_customernumber") ? customer["tu_customernumber"] : string.Empty;
            customerBenefit["tu_maxallowed"] = benefit.Attributes.Contains("tu_maxallowed") ? benefit["tu_maxallowed"] : string.Empty;
            customerBenefit["tu_benefitcategory"] = benefit.Attributes.Contains("tu_benefitcategory") ? benefit["tu_benefitcategory"] : null;

            TracingService.Trace("Before CreateCustomerBenefits");
            return OrganizationService.Create(customerBenefit);
        }
        #endregion
    }
}
