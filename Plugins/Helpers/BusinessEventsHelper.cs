﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using TCT.D365.TheUnlimited.Plugins.Abstract;
using TCT.D365.TheUnlimited.Plugins.Models;

namespace TCT.D365.TheUnlimited.Plugins.Helpers
{
    public class BusinessEventsHelper : AbstractDynamics
    {
        private static readonly string ExternalSourceType = "CRM";
        private Customer Customer { get; set; }
        private DynamicsHelper DynamicsHelper { get; set; }
        public Entity D365Customer { get; set; }
        public Entity MandatePlan { get; set; }
        public Entity Mandate { get; set; }
        private EntityCollection BenefitsCollection { get; set; }
        public string EventName { get; set; }

        public BusinessEventsHelper(IOrganizationService organizationService, ITracingService tracingService, 
            Entity d365Customer, Entity mandatePlan, Entity mandate, EntityCollection benefitsCollection, string eventName) : 
            base(organizationService, tracingService)
        {
            DynamicsHelper = new DynamicsHelper(organizationService, tracingService);
            D365Customer = d365Customer;
            MandatePlan = mandatePlan;
            BenefitsCollection = benefitsCollection;
            EventName = eventName;
            Mandate = mandate;
        }

        public BusinessEvent GetBusinessEvent()
        {
            var businessEvent = new BusinessEvent
            {
                Event = EventName,
                ExternalSourceType = ExternalSourceType,
                RequestID = Guid.NewGuid(),
                ReferenceId = D365Customer.Id,

                Customer = GetCustomer(),
            };

            return businessEvent;
        }

        private Customer GetCustomer()
        {
            Customer = new Customer
            {
                DisplayNumber = D365Customer.Attributes.Contains("tu_customernumber") ? D365Customer["tu_customernumber"].ToString() : string.Empty,
                Title = D365Customer.Attributes.Contains("tu_title") ? D365Customer.FormattedValues["tu_title"].ToString() : string.Empty,
                FirstName = D365Customer.Attributes.Contains("firstname") ? D365Customer["firstname"].ToString() : string.Empty,
                LastName = D365Customer.Attributes.Contains("lastname") ? D365Customer["lastname"].ToString() : string.Empty,
                IdentityNo = D365Customer.Attributes.Contains("tu_idnopassport") ? D365Customer["tu_idnopassport"].ToString() : string.Empty,
                IdentityType = D365Customer.Attributes.Contains("tu_isforeigner") ? ((bool)D365Customer["tu_isforeigner"] == false ? "SAID" : "Passport") : string.Empty,
                MobilePhone = D365Customer.Attributes.Contains("mobilephone") ? D365Customer["mobilephone"].ToString() : string.Empty,
                EmailAddress = D365Customer.Attributes.Contains("emailaddress1") ? D365Customer["emailaddress1"].ToString() : string.Empty,
                DateofBirth = D365Customer.Attributes.Contains("birthdate") ? ((DateTime)D365Customer["birthdate"]).Date : DateTime.Today,
                PlanId = MandatePlan.Attributes.Contains("tu_pasplanid") ? (int.TryParse(((MandatePlan["tu_pasplanid"])).ToString(), out int q) ? q : 0) : 0,
                Address = new List<Address>()
                {
                },
                Mandate = new List<Mandate>()
                {
                },
            };

            if (EventName.ToLower().Equals("newsales") || EventName.ToLower().Equals("newcustomer"))
            {

                Customer.Address.Add(new Address
                {
                    Type = "Residential",
                    Street1 = D365Customer.Attributes.Contains("address1_line1") ? D365Customer["address1_line1"].ToString() : string.Empty,
                    Street2 = D365Customer.Attributes.Contains("address1_line2") ? D365Customer["address1_line2"].ToString() : string.Empty,
                    Street3 = D365Customer.Attributes.Contains("address1_line3") ? D365Customer["address1_line3"].ToString() : string.Empty,
                    City = D365Customer.Attributes.Contains("address1_city") ? D365Customer["address1_city"].ToString() : string.Empty,
                    Suburb = D365Customer.Attributes.Contains("address1_stateorprovince") ? D365Customer["address1_stateorprovince"].ToString() : string.Empty,
                    Country = D365Customer.Attributes.Contains("tu_countryresidential") ? D365Customer.FormattedValues["tu_countryresidential"].ToString() : string.Empty,
                    Code = D365Customer.Attributes.Contains("address1_postalcode") ? ((int.TryParse(D365Customer["address1_postalcode"].ToString(), out int bc)) ? bc : 0) : 0,
                });

                Customer.Address.Add(new Address
                {
                    Type = "Delivery",
                    Street1 = D365Customer.Attributes.Contains("address2_line1") ? D365Customer["address2_line1"].ToString() : string.Empty,
                    Street2 = D365Customer.Attributes.Contains("address2_line2") ? D365Customer["address2_line2"].ToString() : string.Empty,
                    Street3 = D365Customer.Attributes.Contains("address2_line3") ? D365Customer["address2_line3"].ToString() : string.Empty,
                    City = D365Customer.Attributes.Contains("address2_city") ? D365Customer["address2_city"].ToString() : string.Empty,
                    Suburb = D365Customer.Attributes.Contains("address2_stateorprovince") ? D365Customer["address2_stateorprovince"].ToString() : string.Empty,
                    Country = D365Customer.Attributes.Contains("tu_countrydelivery") ? D365Customer.FormattedValues["tu_countrydelivery"].ToString() : string.Empty,
                    Code = D365Customer.Attributes.Contains("address2_postalcode") ? ((int.TryParse(D365Customer["address2_postalcode"].ToString(), out int ba)) ? ba : 0) : 0,
                });

                Customer.Address.Add(new Address
                {
                    Type = "Postal",
                    Street1 = D365Customer.Attributes.Contains("address3_line1") ? D365Customer["address3_line1"].ToString() : string.Empty,
                    Street2 = D365Customer.Attributes.Contains("address3_line2") ? D365Customer["address3_line2"].ToString() : string.Empty,
                    Street3 = D365Customer.Attributes.Contains("address3_line3") ? D365Customer["address3_line3"].ToString() : string.Empty,
                    City = D365Customer.Attributes.Contains("address3_city") ? D365Customer["address3_city"].ToString() : string.Empty,
                    Suburb = D365Customer.Attributes.Contains("address3_stateorprovince") ? D365Customer["address3_stateorprovince"].ToString() : string.Empty,
                    Country = D365Customer.Attributes.Contains("tu_countrypostal") ? D365Customer.FormattedValues["tu_countrypostal"].ToString() : string.Empty,
                    Code = D365Customer.Attributes.Contains("address3_postalcode") ? ((int.TryParse(D365Customer["address3_postalcode"].ToString(), out int bb)) ? bb : 0) : 0,
                });
            }

            if (Mandate != null && Mandate.Id != null)
                SetMandate();

            return Customer;
        }

        private void SetMandate()
        {
            var mandate = new Mandate
            {
                MandateId = Mandate.Id,
                MandateShortId = Mandate.Attributes.Contains("tu_name") ? Mandate["tu_name"].ToString() : string.Empty,
                InstallmentAmount = Mandate.Attributes.Contains("tu_premium") ? ((Money)Mandate["tu_premium"]).Value : 0,
                FirstCollectionAmount = Mandate.Attributes.Contains("tu_firstpremium") ? ((Money)Mandate["tu_firstpremium"]).Value : 0,
                CollectionDay = Mandate.Attributes.Contains("tu_collectionday") ? ((OptionSetValue)Mandate["tu_collectionday"]).Value : 0,
                Postpone1stDebit = Mandate.Attributes.Contains("tu_postpone1stdebit") ? (bool)Mandate["tu_postpone1stdebit"] : false,
                DebitFromDate = Mandate.Attributes.Contains("tu_debitdate") ? ((DateTime)Mandate["tu_debitdate"]).Date : DateTime.Today,
                FixedExtractDate = Mandate.Attributes.Contains("tu_fixedextractdate") ? (bool)Mandate["tu_fixedextractdate"] : false,
                Payer = new Payer
                {
                    Details = new Party
                    {
                        DisplayNumber = Mandate.Attributes.Contains("tu_customernumber") ? Mandate["tu_customernumber"].ToString() : string.Empty,
                        FirstName = Mandate.Attributes.Contains("tu_firstname") ? Mandate["tu_firstname"].ToString() : string.Empty,
                        LastName = Mandate.Attributes.Contains("tu_lastname") ? Mandate["tu_lastname"].ToString() : string.Empty,
                        MobilePhone = Mandate.Attributes.Contains("tu_mobilephone") ? Mandate["tu_mobilephone"].ToString() : string.Empty,
                        IdentityNo = Mandate.Attributes.Contains("tu_idno") ? Mandate["tu_idno"].ToString() : string.Empty,
                        //Passport = Mandate.Attributes.Contains("") ? Mandate[""].ToString() : string.Empty,
                        IdentityType = Mandate.Attributes.Contains("tu_isforeigner") ? ((bool)Mandate["tu_isforeigner"] == false ? "SAID" : "Passport" ) : string.Empty,
                        EmailAddress = (Mandate.Attributes.Contains("tu_emailaddress1") && Mandate["tu_emailaddress1"] != null) ? Mandate["tu_emailaddress1"].ToString() : string.Empty,
                        DateofBirth = Mandate.Attributes.Contains("tu_birthdate") ? ((DateTime)Mandate["tu_birthdate"]).Date : DateTime.Today,

                    },
                    Banking = new Banking
                    {
                        AccountNumber = Mandate.Attributes.Contains("tu_accountnumber") ? Mandate["tu_accountnumber"].ToString() : string.Empty,
                        BankAccountType = Mandate.Attributes.Contains("tu_accounttype") ? Mandate.FormattedValues["tu_accounttype"].ToString() : string.Empty,
                        BankName = Mandate.Attributes.Contains("tu_bankname") ? Mandate.FormattedValues["tu_bankname"].ToString() : string.Empty,
                        BranchCode = Mandate.Attributes.Contains("tu_branchcode") ? ((int.TryParse(Mandate["tu_branchcode"].ToString(), out int bc)) ? bc : 0) : 0,
                    },
                },

                Benefits = new List<Benefit>() {
                },
            };

            if (EventName.ToLower().Equals("cancelmandate"))
                mandate.EffectiveFromDate = DateTime.Today.Date;

            if (BenefitsCollection != null)
                GetBenefit(mandate);

            Customer.Mandate.Add(mandate);
        }

        private void GetBenefit(Mandate mandate)
        {
            foreach(var d365Benefit in BenefitsCollection.Entities)
            {
                var benefit = new Benefit
                {
                    Name = d365Benefit.Attributes.Contains("Benefit.tu_id") ? ((AliasedValue)d365Benefit["Benefit.tu_id"]).Value.ToString() : string.Empty, 
                    Category = d365Benefit.Attributes.Contains("tu_benefitcategory") ? d365Benefit.FormattedValues["tu_benefitcategory"] : string.Empty,
                    Id = d365Benefit.Attributes.Contains("Benefit.tu_name") ? (int.TryParse((((AliasedValue)d365Benefit["Benefit.tu_name"]).Value).ToString(), out int n) ? n : 0) : 0,
                    Beneficiaries = new List<Beneficiary>() { },
                    //LinkToSim = new Sim() { },
                };

                //Add Dependents 
                var dependantsCol = DynamicsHelper.GetDependants(d365Benefit.Id);
                foreach(var dependant in dependantsCol.Entities)
                {
                    benefit.Beneficiaries.Add(new Beneficiary
                    {
                        Type = dependant.Attributes.Contains("tu_type") ? dependant.FormattedValues["tu_type"] : string.Empty,
                        FirstName = dependant.Attributes.Contains("firstname") ? dependant["firstname"].ToString() : string.Empty,
                        LastName = dependant.Attributes.Contains("lastname") ? dependant["lastname"].ToString() : string.Empty,
                        DateofBirth = dependant.Attributes.Contains("birthdate") ? ((DateTime)dependant["birthdate"]).Date : DateTime.Today,
                    });
                }

                //Add SIM
                var simCol = DynamicsHelper.GetSIMDetails(d365Benefit.Id);
                foreach (var d365Sim in simCol.Entities)
                {
                    benefit.LinkToSim = new Sim
                    {
                        ICCID = d365Sim.Attributes.Contains("tu_iccid") ? d365Sim["tu_iccid"].ToString() : string.Empty,
                        IssueNewSIM = d365Sim.Attributes.Contains("tu_linktosim") ? (bool)d365Sim["tu_linktosim"] : false,
                        MSISDN = d365Sim.Attributes.Contains("tu_name") ? d365Sim["tu_name"].ToString() : string.Empty
                    };
                }

                mandate.Benefits.Add(benefit);
            }
        }
    }
}
