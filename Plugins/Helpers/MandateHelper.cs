﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using Microsoft.Xrm.Sdk;
//using Microsoft.Xrm.Sdk.Query;
//using Newtonsoft.Json;
//using TCT.D365.TheUnlimited.Plugins.Abstract;
//using TCT.D365.TheUnlimited.Plugins.Helpers;
//using TCT.D365.TheUnlimited.Plugins.Models;

//namespace TCT.D365.TheUnlimited.Plugins.Helper
//{
//    public class MandateHelper : AbstractDynamics
//    {
//        private static readonly string ExternalSourceType = "CRM";
//        private static readonly string CancelMandateAction = "tu_CancelMandateBusinessEvents";
//        private static readonly string MandateInfomationChangeAction = "tu_MandateInformationChangeBusinessEvent";
//        private static readonly string BenefitsRemovedAction = "tu_BenefitRemovedBusinessEvent";
//        private static readonly string NewSaleAction = "tu_NewSaleBusinessEvent";
//        private static readonly string NewCustomerAction = "tu_NewCustomerBusinessEvent";
//        private DynamicsHelper DynamicsHelper { get; set; }

//        public MandateHelper(IOrganizationService organizationService, ITracingService tracingService, IPluginExecutionContext pluginExecutionContext) :
//            base(organizationService, tracingService, pluginExecutionContext)
//        {
//            DynamicsHelper = new DynamicsHelper(organizationService, tracingService);
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        public void ExecuteNewSaleBusEvent()
//        {
//            var collection = (Entity)PluginExecutionContext?.InputParameters["Target"];

//            if (collection != null
//                && collection?.LogicalName != null)
//            {
//                var customerId = ((EntityReference)collection["tu_customer"]).Id;
//                var CusBenefitsCol = DynamicsHelper.GetActiveCustomerBenefitsCol(customerId);
//                var customerCol = DynamicsHelper.GetCustomerCol(customerId);

//                TracingService.Trace("After GetCustomerBenefitsCol :" + CusBenefitsCol.Entities.Count());
//                TracingService.Trace("After GetCustomerCol :" + customerCol.Entities.Count());

//                if (CusBenefitsCol?.Entities?.Count > 0
//                && customerCol?.Entities?.Count > 0)
//                {
//                    TracingService.Trace("Before Create newSaleBusEvent");
//                    var newSaleBusEvent = new NewSaleBusinessEvent()
//                    {
//                        Mandate = SetMandateInformation(customerCol, collection),
//                        BenefitHolderDetails = SetPartyDetails(customerCol),
//                        Benefits = new List<Benefit>()
//                            {
//                            },
//                    };

//                    TracingService.Trace("Before Create newSaleBusEvent customerBenefit");
//                    foreach (var customerBenefit in CusBenefitsCol.Entities)
//                    {
//                        newSaleBusEvent.Benefits.Add(new Benefit()
//                        {
//                            Id = (int.TryParse((((AliasedValue)customerBenefit["Benefit.tu_name"]).Value).ToString(), out int n) ? n : 0),
//                            Name = ((AliasedValue)customerBenefit["Benefit.tu_id"]).Value.ToString(),
//                        });
//                    }

//                    TracingService.Trace("Before newSaleBusEventActionReq execute");
//                    var parameters = new ParameterCollection {
//                                new KeyValuePair<string, object>("Payload",JsonConvert.SerializeObject(newSaleBusEvent)),
//                            };

//                    var actionResponse = DynamicsHelper.ExecuteGlobalAction(NewSaleAction, parameters);
//                    TracingService.Trace("After newSaleBusEventActionReq execute: " + actionResponse.Results["Message"]);
//                }
//            }
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        public void ExeceuteNewCustomerBusEvent()
//        {
//            var collection = (Entity)PluginExecutionContext.InputParameters["Target"];
//            if (collection != null && collection.LogicalName != null)
//            {
//                var customerID = ((EntityReference)collection.Attributes["tu_customer"]).Id;
//                var customerBenefits = DynamicsHelper.GetActiveCustomerBenefitsCol(customerID);
//                var customers = DynamicsHelper.GetCustomerCol(customerID);

//                if (customerBenefits.Entities.Count > 0 && customers.Entities.Count > 0)
//                {
//                    var newCustomerBusEvent = new NewCustomerBusinessEvent()
//                    {
//                        ExternalSourceType = ExternalSourceType,
//                        RequestID = Guid.NewGuid(),
//                        ReferenceId = customers.Entities.Count > 0 ? customers.Entities[0].Id.ToString() : string.Empty,
//                        DebitFromDate = collection.Attributes.Contains("tu_debitdate") ? (DateTime)collection["tu_debitdate"] : DateTime.Today,
//                        //MandateId = collection.Id,
//                        Customer = new Party()
//                        {
//                            DisplayNumber = customers.Entities[0].Attributes.Contains("tu_customernumber")
//                            ? customers.Entities[0].Attributes["tu_customernumber"].ToString() : string.Empty,
//                            FirstName = customers.Entities[0].Attributes.Contains("firstname")
//                            ? customers.Entities[0].Attributes["firstname"].ToString() : string.Empty,
//                            Surname = customers.Entities[0].Attributes.Contains("lastname")
//                            ? customers.Entities[0].Attributes["lastname"].ToString() : string.Empty,
//                            IdentityNo = customers.Entities[0].Attributes.Contains("tu_idnopassport")
//                            ? customers.Entities[0].Attributes["tu_idnopassport"].ToString() : string.Empty,
//                            MobilePhone = customers.Entities[0].Attributes.Contains("mobilephone") ?
//                            customers.Entities[0].Attributes["mobilephone"].ToString() : string.Empty,
//                            EmailAddress = customers.Entities[0].Contains("emailaddress1") ?
//                            customers.Entities[0].Attributes["emailaddress1"].ToString() : string.Empty,
//                            DateofBirth = customers.Entities[0].Contains("birthdate") ?
//                            (DateTime)customers.Entities[0].Attributes["birthdate"] : DateTime.Today
//                        },
//                        Benefits = new List<Benefit>()
//                        {
//                        },
//                    };

//                    foreach (var custBenefit in customerBenefits.Entities)
//                    {
//                        newCustomerBusEvent.Benefits.Add(new Benefit()
//                        {
//                            Id = (int.TryParse((((AliasedValue)custBenefit["Benefit.tu_name"]).Value).ToString(), out int n) ? n : 0),
//                            Name = ((AliasedValue)custBenefit["Benefit.tu_id"]).Value.ToString(),
//                        });
//                    }

//                    var parameters = new ParameterCollection {
//                                new KeyValuePair<string, object>("Payload",JsonConvert.SerializeObject(newCustomerBusEvent)),
//                            };

//                    var actionResponse = DynamicsHelper.ExecuteGlobalAction(NewCustomerAction, parameters);
//                    TracingService.Trace("After newSaleBusEventActionReq execute: " + actionResponse.Results["Message"]);
//                }
//            }
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        public void ExecuteCancelMandateBusEvent()
//        {
//            var postMandateImage = PluginExecutionContext.PostEntityImages["PostMandateImage"];
//            var customer = postMandateImage.Attributes.Contains("tu_customer") ? (EntityReference)postMandateImage["tu_customer"] : null;

//            if (customer != null && customer?.Id != null)
//            {
//                var customersCol = DynamicsHelper.GetCustomerCol(customer.Id);
//                var customerBenefitsCol = DynamicsHelper.GetActiveCustomerBenefitsCol(customer.Id);

//                TracingService.Trace("After GetCustomerBenefitsCol :" + customerBenefitsCol.Entities.Count());
//                TracingService.Trace("After GetCustomerCol :" + customersCol.Entities.Count());

//                if (customerBenefitsCol.Entities.Count > 0 && customersCol.Entities.Count > 0)
//                {
//                    var cancelMandateBusEvent = new CancelMandateBusinessEvent
//                    {
//                        ExternalSourceType = ExternalSourceType,
//                        RequestID = Guid.NewGuid(),
//                        MandateId = PluginExecutionContext.PrimaryEntityId,
//                        EffectiveFromDate = DateTime.Today,
//                        ReferenceId = customersCol.Entities.Count > 0 ? customersCol.Entities[0].Id.ToString() : string.Empty,

//                        Benefits = new List<Benefit>()
//                            {
//                            },
//                    };

//                    foreach (var customerBenefit in customerBenefitsCol.Entities)
//                    {
//                        cancelMandateBusEvent.Benefits.Add(new Benefit()
//                        {
//                            Id = customerBenefit.Attributes.Contains("Benefit.tu_name") ? (int.TryParse((((AliasedValue)customerBenefit["Benefit.tu_name"]).Value).ToString(), out int n) ? n : 0) : 0,
//                            Name = customerBenefit.Attributes.Contains("Benefit.tu_id") ? ((AliasedValue)customerBenefit["Benefit.tu_id"]).Value.ToString() : string.Empty,
//                        });
//                    }

//                    var parameters = new ParameterCollection {
//                                new KeyValuePair<string, object>("Payload",JsonConvert.SerializeObject(cancelMandateBusEvent)),
//                            };

//                    DynamicsHelper.ExecuteGlobalAction(CancelMandateAction, parameters);
//                }
//            }
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        public void ExecuteMandateInfoChangeBusEvent()
//        {
//            var postMandateImage = PluginExecutionContext?.PostEntityImages["PostMandateImage"];
//            var customer = postMandateImage.Attributes.Contains("tu_customer") ? (EntityReference)postMandateImage["tu_customer"]: null;

//            if (customer != null && customer?.Id != null)
//            {
//                var customersCol = DynamicsHelper.GetCustomerCol(customer.Id);
//                TracingService.Trace("After GetCustomerCol :" + customersCol?.Entities?.Count());

//                if (customersCol?.Entities?.Count > 0)
//                {
//                    var mandateInfoChangeBusEvent = new MandateInformationChangeBusinessEvent()
//                    {
//                        Mandate = SetMandateInformation(customersCol,postMandateImage),
//                    };

//                    var parameters = new ParameterCollection {
//                                new KeyValuePair<string, object>("Payload",JsonConvert.SerializeObject(mandateInfoChangeBusEvent)),
//                            };

//                  DynamicsHelper.ExecuteGlobalAction(MandateInfomationChangeAction, parameters);
//                }
//            }
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        public void ExecuteBenefitRemovedBusEvent()
//        {
//            var postMandateImage = PluginExecutionContext?.PostEntityImages["PostMandateImage"];
//            var customer = postMandateImage.Attributes.Contains("tu_customer") ? (EntityReference)postMandateImage["tu_customer"] : null;

//            if (customer != null && customer?.Id != null)
//            {
//                var customersCol = DynamicsHelper.GetCustomerCol(customer.Id);
//                var customerBenefitsCol = DynamicsHelper.GetActiveCustomerBenefitsCol(customer.Id);
//                TracingService.Trace("After GetCustomerCol :" + customersCol?.Entities?.Count());

//                if (customersCol?.Entities?.Count > 0 && customerBenefitsCol?.Entities?.Count > 0)
//                {
//                    var benefitsRemovedBusEvent = new BenefitRemovedBusinessEvent()
//                    {
//                        Mandate = SetMandateInformation(customersCol, postMandateImage),
//                        BenefitHolderDetails = SetPartyDetails(customersCol),
//                        Benefits = new List<Benefit>()
//                            {
//                            },
//                    };

//                    foreach (var customerBenefit in customerBenefitsCol.Entities)
//                    {
//                        benefitsRemovedBusEvent.Benefits.Add(new Benefit()
//                        {
//                            Id = customerBenefit.Attributes.Contains("Benefit.tu_name") ? (int.TryParse((((AliasedValue)customerBenefit["Benefit.tu_name"]).Value).ToString(), out int n) ? n : 0) : 0,
//                            Name = customerBenefit.Attributes.Contains("Benefit.tu_id") ? ((AliasedValue)customerBenefit["Benefit.tu_id"]).Value.ToString() : string.Empty,
//                        });
//                    }

//                    var parameters = new ParameterCollection {
//                                new KeyValuePair<string, object>("Payload",JsonConvert.SerializeObject(benefitsRemovedBusEvent)),
//                            };

//                    DynamicsHelper.ExecuteGlobalAction(BenefitsRemovedAction, parameters);
//                }
//            }
//        }

//        #region Private Methods
//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="customersCol"></param>
//        /// <param name="mandateInfo"></param>
//        /// <returns></returns>
//        private Mandate SetMandateInformation(EntityCollection customersCol, Entity mandateInfo)
//        {
//            var mandateInfomation = new Mandate()
//            {
//                ExternalSourceType = ExternalSourceType,
//                RequestID = Guid.NewGuid(),
//                ReferenceId = customersCol.Entities.Count > 0 ? customersCol.Entities[0].Id.ToString() : string.Empty,
//                MandateId = PluginExecutionContext.PrimaryEntityId,
//                InstallmentAmount = mandateInfo.Attributes.Contains("tu_premium") ? ((Money)mandateInfo["tu_premium"]).Value : 0,
//                FirstCollectionAmount = mandateInfo.Attributes.Contains("tu_firstpremium") ? ((Money)mandateInfo["tu_firstpremium"]).Value : 0,
//                DebitFromDate = mandateInfo.Attributes.Contains("tu_debitdate") ? (DateTime)mandateInfo["tu_debitdate"] : DateTime.Today,
//                FixedExtractDate = mandateInfo.Attributes.Contains("tu_fixedextractdate") ? (bool)mandateInfo["tu_fixedextractdate"] : false,
//                CollectionDay = mandateInfo.Attributes.Contains("tu_collectionday") ? ((OptionSetValue)mandateInfo["tu_collectionday"]).Value : 0,
//                Postpone1stDebit = mandateInfo.Attributes.Contains("tu_postpone1stdebit") ? (bool)mandateInfo["tu_postpone1stdebit"] : false,
//                Payer = new Payer()
//                {
//                    Banking = mandateInfo.Attributes.Contains("tu_bankdetails") ? 
//                    SetBankingDetails(DynamicsHelper.GetBankingDetails(((EntityReference)mandateInfo["tu_bankdetails"]).Id)) : null,
//                    Details = mandateInfo.Attributes.Contains("tu_payer") ?
//                    SetPartyDetails(DynamicsHelper.GetCustomerCol(((EntityReference)mandateInfo["tu_payer"]).Id)) : null,
//                },
//            };

//            return mandateInfomation;
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="customersCol"></param>
//        /// <returns></returns>
//        private Party SetPartyDetails(EntityCollection customersCol)
//        {
//            var benefitHolderDetails = new Party()
//            {
//                DisplayNumber = customersCol.Entities[0].Attributes.Contains("tu_customernumber") ? customersCol.Entities[0]["tu_customernumber"].ToString() : string.Empty,
//                FirstName = customersCol.Entities[0].Attributes.Contains("firstname") ? customersCol.Entities[0]["firstname"].ToString() : string.Empty,
//                LastName = customersCol.Entities[0].Attributes.Contains("lastname") ? customersCol.Entities[0]["lastname"].ToString() : string.Empty,
//                IdentityNo = customersCol.Entities[0].Attributes.Contains("tu_idnopassport") ? customersCol.Entities[0]["tu_idnopassport"].ToString() : string.Empty,
//                IdentityType = customersCol.Entities[0].Attributes.Contains("tu_isforeigner") ? ((bool)customersCol.Entities[0]["tu_isforeigner"] == false ? "SAID" : "Foreigner") : string.Empty,
//                MobilePhone = customersCol.Entities[0].Attributes.Contains("mobilephone") ? customersCol.Entities[0]["mobilephone"].ToString() : string.Empty,
//                EmailAddress = customersCol.Entities[0].Attributes.Contains("emailaddress1") ? customersCol.Entities[0]["emailaddress1"].ToString() : string.Empty,
//                DateofBirth = customersCol.Entities[0].Attributes.Contains("birthdate") ? (DateTime)customersCol.Entities[0]["birthdate"] : DateTime.Today,
//            };

//            return benefitHolderDetails;
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="bankingCol"></param>
//        /// <returns></returns>
//        private Banking SetBankingDetails(EntityCollection bankingCol)
//        {
//            var banking = new Banking()
//            {
//                AccountNumber = bankingCol[0].Attributes.Contains("tu_accountnumber") ? bankingCol[0]["tu_accountnumber"].ToString() : string.Empty,
//                BankAccountType = bankingCol[0].Attributes.Contains("tu_accounttype") ? bankingCol[0].FormattedValues["tu_accounttype"] : string.Empty,
//                BranchCode = bankingCol[0].Attributes.Contains("tu_branchcode") ? ((int.TryParse(bankingCol[0]["tu_branchcode"].ToString(), out int bc)) ? bc : 0) : 0,
//                BankName = bankingCol[0].Attributes.Contains("tu_bankname") ? bankingCol[0].FormattedValues["tu_bankname"] : string.Empty,
//            };

//            return banking;
//        }
//        #endregion
//    }
//}
