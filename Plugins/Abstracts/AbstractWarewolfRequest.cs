﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TCT.D365.TheUnlimited.Plugins.Helpers;

namespace TCT.D365.TheUnlimited.Plugins.Abstracts
{
    public abstract class AbstractWarewolfRequest
    {
        public string Url { get; set; }
        public string ApiKey { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        public AbstractWarewolfRequest(string url)
        {
            Url = url;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="apiKey"></param>
        public AbstractWarewolfRequest(string url, string apiKey)
        {
            Url = url;
            ApiKey = apiKey;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        public AbstractWarewolfRequest(string url, string userName, string password)
        {
            Url = url;
            UserName = userName;
            Password = password;
        }
    }
}
