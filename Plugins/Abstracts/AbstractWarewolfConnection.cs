﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TCT.D365.TheUnlimited.Plugins.Interfaces;

namespace TCT.D365.TheUnlimited.Plugins.Abstracts
{
    public abstract class AbstractWarewolfConnection : AbstractWarewolfRequest, IAbstractRequestConnection 
    {
        public HttpClient HttpClient { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpClient"></param>
        /// <param name="url"></param>
        public AbstractWarewolfConnection(HttpClient httpClient, string url) :
           base(url)
        {
            HttpClient = httpClient;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpClient"></param>
        /// <param name="url"></param>
        /// <param name="apiKey"></param>
        public AbstractWarewolfConnection(HttpClient httpClient,string url, string apiKey) : 
            base(url, apiKey)
        {
            HttpClient = httpClient;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpClient"></param>
        /// <param name="url"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        public AbstractWarewolfConnection(HttpClient httpClient, string url, string userName, string password) : 
            base(url, userName, password)
        {
            HttpClient = httpClient;
        }

        public abstract void Connect();
        public abstract void Close();
    }
}
