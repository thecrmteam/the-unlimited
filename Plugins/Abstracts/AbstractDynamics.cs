﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCT.D365.TheUnlimited.Plugins.Abstract
{
    public abstract class AbstractDynamics
    {
        public IOrganizationService OrganizationService { get; set; }
        public ITracingService TracingService { get; set; }
        public IPluginExecutionContext PluginExecutionContext { get; set; }

        public AbstractDynamics(IOrganizationService organizationService, ITracingService tracingService, IPluginExecutionContext pluginExecutionContext)
        {
            OrganizationService = organizationService;
            TracingService = tracingService;
            PluginExecutionContext = pluginExecutionContext;
        }

        public AbstractDynamics(IOrganizationService organizationService, ITracingService tracingService)
        {
            OrganizationService = organizationService;
            TracingService = tracingService;
        }

        public AbstractDynamics(IOrganizationService organizationService)
        {
            OrganizationService = organizationService;
        }
    }
}
