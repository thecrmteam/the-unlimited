﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCT.D365.TheUnlimited.Plugins.Interfaces
{
    public interface IWarewolfRequest
    {
        Task<string> Execute(string requestBody);
    }
}
