﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TCT.D365.TheUnlimited.Plugins.Interfaces
{
    public interface IAbstractRequestConnection
    {
        void Connect();
        void Close();
    }
}
