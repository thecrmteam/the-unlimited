﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using TCT.D365.TheUnlimited.Plugins.Helpers;

namespace TCT.D365.TheUnlimited.Plugins.Plugins.Contact
{
    public class RestrictBlackListedContactActivation : PluginBase
    {
        public RestrictBlackListedContactActivation() : base(typeof(RestrictBlackListedContactActivation))
        {
        }

        /// <summary>
        /// Main entry point for he business logic that the plug-in is to execute.
        /// </summary>
        /// <param name="localContext">The <see cref="LocalPluginContext"/> which contains the
        /// <see cref="IPluginExecutionContext"/>,
        /// <see cref="IOrganizationService"/>
        /// and <see cref="ITracingService"/>
        /// </param>
        /// <remarks>
        /// For improved performance, Microsoft Dynamics 365 caches plug-in instances.
        /// The plug-in's Execute method should be written to be stateless as the constructor
        /// is not called for every invocation of the plug-in. Also, multiple system threads
        /// could execute the plug-in at the same time. All per invocation state information
        /// is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected override void ExecuteCrmPlugin(LocalPluginContext localContext)
        {
            #region (* Local variables *)
            string functionName = "ExecuteCrmPlugin";
            Entity contact = new Entity();
            #endregion
            try
            {
                //Validate localcontext and its atrributes
                if (localContext != null && localContext?.OrganizationService != null
                   && localContext?.PluginExecutionContext != null && localContext?.PluginExecutionContext?.InputParameters != null
                   && localContext?.TracingService != null)
                {
                    localContext.TracingService.Trace("Inside: " + functionName);

                    //Read target
                    contact = localContext.PluginExecutionContext.InputParameters.Contains("Target") && localContext.PluginExecutionContext.InputParameters["Target"] is Entity ? localContext?.PluginExecutionContext?.InputParameters["Target"] as Entity : null;

                    //Validate target entity 
                    if (contact != null)
                    {
                        localContext.TracingService.Trace("After validating target entity");

                        //Create Helper object
                        var dynamicsHelper = new DynamicsHelper(localContext?.OrganizationService, localContext?.TracingService);

                        if (dynamicsHelper != null)
                        {
                            //Function to process Contact
                            ProcessContact(contact, localContext, dynamicsHelper);
                        }
                    }//End If
                    else
                    {
                        throw new InvalidPluginExecutionException("RestrictBlackListedContactActivation: Target entity not found.");
                    }
                }//End If
                else
                {
                    throw new InvalidPluginExecutionException("RestrictBlackListedContactActivation: Null localContext.");
                }

                localContext.TracingService.Trace("Exited from: " + functionName);

            }
            catch (InvalidPluginExecutionException ex)
            {
                throw new InvalidPluginExecutionException(ex.InnerException != null && ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message);
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                throw new InvalidPluginExecutionException(ex.InnerException != null && ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message);
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException(ex.InnerException != null && ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message);
            }
        }

        /// <summary>
        /// Function to start process for Contact
        /// </summary>
        /// <param name="contact">Entity object</param>
        /// <param name="localContext">LocalPluginContext object</param>
        private void ProcessContact(Entity contact, LocalPluginContext localContext, DynamicsHelper dynamicsHelper)
        {
            #region (* Local variables *)
            string functionName = "ProcessContact";
            int status = -1;
            EntityCollection contactCol = new EntityCollection();
            EntityCollection blacklistCol = new EntityCollection();
            string idNumber = string.Empty;
            #endregion
            try
            {
                localContext.TracingService.Trace("Inside: " + functionName);

                //Read Contact Status
                status = contact.GetAttributeValue<OptionSetValue>("statecode") != null ? contact.GetAttributeValue<OptionSetValue>("statecode").Value : -1;

                localContext.TracingService.Trace("Contact Status: " + status);

                //If status is Active check for Blacklist
                if (status == 0 /*Active*/)
                {
                    localContext.TracingService.Trace("After checking active status.");

                    contactCol = dynamicsHelper.GetContactDetails(contact.Id);

                    //Validate collection count
                    if (contactCol.Entities.Count > 0)
                    {
                        localContext.TracingService.Trace("After checking contact collection count.");

                        //ID Number
                        idNumber = string.IsNullOrEmpty(contactCol.Entities[0].GetAttributeValue<string>("tu_idnopassport")) ? string.Empty : contactCol.Entities[0].GetAttributeValue<string>("tu_idnopassport");

                        localContext.TracingService.Trace("ID Number: " + idNumber);

                        //If ID number is valid
                        //Then check for blacklist
                        if (!string.IsNullOrEmpty(idNumber))
                        {
                            //Get Blacklist collection for contact
                            blacklistCol = dynamicsHelper.GetBlacklistCollection(idNumber);

                            //If blacklist record found for contact
                            //Then show error
                            //Else do nothing
                            if (blacklistCol.Entities.Count > 0)
                            {
                                localContext.TracingService.Trace("After checking blacklist collection count.");

                                throw new InvalidPluginExecutionException("Unfavorable information exists.");

                            }//End If
                        }

                    }//End If

                }//End If


                localContext.TracingService.Trace("Exited from: " + functionName);
            }
            catch (InvalidPluginExecutionException ex)
            {
                throw new InvalidPluginExecutionException(ex.InnerException != null && ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message);
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                throw new InvalidPluginExecutionException(ex.InnerException != null && ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message);
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException(ex.InnerException != null && ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message);
            }
        }
    }
}
