// <copyright file="PostSystemuserCreate.cs" company="">
// Copyright (c) 2020 All Rights Reserved
// </copyright>
// <author></author>
// <date>6/12/2020 2:19:11 PM</date>
// <summary>Implements the PostSystemuserCreate Plugin.</summary>
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1
// </auto-generated>

using System;
using System.Collections.Generic;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using TCT.D365.TheUnlimited.Plugins.Helpers;

namespace TCT.D365.TheUnlimited.Plugins
{

    /// <summary>
    /// PostSystemuserUpdate Plugin.
    /// </summary>    
    public class PostSystemuserUpdate : PluginBase
    {
        public string VICIUserAction = "tu_ViciUsers";
        public int VICIUserLevel = 1;

        /// <summary>
        /// Initializes a new instance of the <see cref="PostSystemuserUpdate"/> class.
        /// </summary>
        /// <param name="unsecure">Contains public (unsecured) configuration information.</param>
        /// <param name="secure">Contains non-public (secured) configuration information. 
        /// When using Microsoft Dynamics 365 for Outlook with Offline Access, 
        /// the secure string is not passed to a plug-in that executes while the client is offline.</param>
        public PostSystemuserUpdate(string unsecure, string secure)
            : base(typeof(PostSystemuserUpdate))
        {

            // TODO: Implement your custom configuration handling.
        }

        /// <summary>
        /// Main entry point for he business logic that the plug-in is to execute.
        /// </summary>
        /// <param name="localContext">The <see cref="LocalPluginContext"/> which contains the
        /// <see cref="IPluginExecutionContext"/>,
        /// <see cref="IOrganizationService"/>
        /// and <see cref="ITracingService"/>
        /// </param>
        /// <remarks>
        /// For improved performance, Microsoft Dynamics 365 caches plug-in instances.
        /// The plug-in's Execute method should be written to be stateless as the constructor
        /// is not called for every invocation of the plug-in. Also, multiple system threads
        /// could execute the plug-in at the same time. All per invocation state information
        /// is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected override void ExecuteCrmPlugin(LocalPluginContext localContext)
        {
            if (localContext != null && localContext?.OrganizationService != null
              && localContext?.PluginExecutionContext != null && localContext?.PluginExecutionContext?.InputParameters != null
              && localContext.PluginExecutionContext.InputParameters.Contains("Target") 
              && localContext.PluginExecutionContext.PostEntityImages != null 
              && localContext.PluginExecutionContext.PostEntityImages.Contains("PostSytemUserImage"))
            {
                try
                {
                    var tracingService = localContext?.TracingService;
                    var organizationService = localContext?.OrganizationService;
                    var dynamicsHelper = new DynamicsHelper(organizationService, tracingService);

                    var postImage = localContext?.PluginExecutionContext?.PostEntityImages["PostSytemUserImage"];
                    var systemUser = (Entity)localContext?.PluginExecutionContext?.InputParameters["Target"];

                    tracingService.Trace("Before tu_isviciuser check... " + postImage.Attributes.Contains("tu_office"));

                    if (postImage.Attributes.Contains("tu_isviciuser") && postImage.Attributes.Contains("tu_office") && postImage.Attributes.Contains("tu_viciuseractive"))
                    {
                        var office = organizationService.Retrieve(((EntityReference)postImage["tu_office"]).LogicalName,
                            ((EntityReference)postImage["tu_office"]).Id, new ColumnSet("tu_officecode"));
                        var isViciUser = (bool)postImage["tu_isviciuser"];
                        var isViciUserActive = (bool)postImage["tu_viciuseractive"];

                        tracingService.Trace("After Office assign.... " + isViciUser);
                        if (!string.IsNullOrEmpty(office["tu_officecode"].ToString()))
                        {
                            var parameters = new ParameterCollection {
                                new KeyValuePair<string, object>("MerchCode", postImage["tu_merchcode"].ToString()),                            
                                new KeyValuePair<string, object>("Level", VICIUserLevel),
                                new KeyValuePair<string, object>("FullName",postImage["fullname"].ToString()),
                                new KeyValuePair<string, object>("OfficeCode", office["tu_officecode"].ToString()),
                            };

                            tracingService.Trace("Check if Create/Upate Vici user... " + isViciUserActive);

                            if (isViciUserActive) {
                                var active = (isViciUser) ? "Y" : "N";
                                
                                parameters.Add(new KeyValuePair<string, object>("Action", "useredit"));
                                parameters.Add(new KeyValuePair<string, object>("Active",active));
                            }
                            else
                            {
                                parameters.Add(new KeyValuePair<string, object>("Action", "useradd"));
                                systemUser["tu_viciuseractive"] = true;
                            } 

                            tracingService.Trace("Before executing Action... " + office["tu_officecode"].ToString());
                            var actionResponse = new DynamicsHelper(organizationService, tracingService).ExecuteGlobalAction(VICIUserAction, parameters);
                            tracingService.Trace("After excecuting action");

                            systemUser["tu_vicilog"] = actionResponse.Results["Message"].ToString();

                            tracingService.Trace("Before updating the user record");
                            organizationService.Update(systemUser);
                        }
                    }
                }
                catch (Exception Exc)
                {
                    throw new InvalidPluginExecutionException(Exc.Message.ToString());
                }
            }
            else
            {
                throw new InvalidPluginExecutionException("Null localContext");
            }
        }
    }
}
