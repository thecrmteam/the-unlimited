// <copyright file="PostOperationcontactCreate.cs" company="">
// Copyright (c) 2020 All Rights Reserved
// </copyright>
// <author></author>
// <date>3/23/2020 12:07:16 PM</date>
// <summary>Implements the PostOperationcontactCreate Plugin.</summary>
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1
// </auto-generated>

using System;
using System.ServiceModel;
using System.Text;
using System.Xml;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using TCT.D365.TheUnlimited.Plugins.Factories;
using TCT.D365.TheUnlimited.Plugins.Helpers;
using TCT.D365.TheUnlimited.Plugins.Models;

namespace TCT.D365.TheUnlimited.Plugins
{

    /// <summary>
    /// PostOperationcontactCreate Plugin.
    /// </summary>    
    public class PostContactCreate: PluginBase
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="PostContactCreate"/> class.
        /// </summary>
        /// <param name="unsecure">Contains public (unsecured) configuration information.</param>
        /// <param name="secure">Contains non-public (secured) configuration information. 
        /// When using Microsoft Dynamics 365 for Outlook with Offline Access, 
        /// the secure string is not passed to a plug-in that executes while the client is offline.</param>
        public PostContactCreate(string unsecure, string secure)
            : base(typeof(PostContactCreate))
        {
    
        }


        /// <summary>
        /// Main entry point for he business logic that the plug-in is to execute.
        /// </summary>
        /// <param name="localContext">The <see cref="LocalPluginContext"/> which contains the
        /// <see cref="IPluginExecutionContext"/>,
        /// <see cref="IOrganizationService"/>
        /// and <see cref="ITracingService"/>
        /// </param>
        /// <remarks>
        /// For improved performance, Microsoft Dynamics 365 caches plug-in instances.
        /// The plug-in's Execute method should be written to be stateless as the constructor
        /// is not called for every invocation of the plug-in. Also, multiple system threads
        /// could execute the plug-in at the same time. All per invocation state information
        /// is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected override void ExecuteCrmPlugin(LocalPluginContext localContext)
        {
            if (localContext != null && localContext?.OrganizationService != null
                && localContext?.PluginExecutionContext != null && localContext?.PluginExecutionContext?.InputParameters != null
                && localContext.PluginExecutionContext.InputParameters.Contains("Target"))
            {
                try
                {
                    var tracingService = localContext?.TracingService;
                    var organizationService = localContext?.OrganizationService;
                    var dynamicsHelper = new DynamicsHelper(organizationService, tracingService);
                    var test = "";
                    var test2 = "test2";

                    var customer = (Entity)localContext?.PluginExecutionContext?.InputParameters["Target"];
                    localContext.PluginExecutionContext.SharedVariables.Add("PostContactCreate", true);

                    if (customer.Attributes.Contains("tu_customerbenefit") && !string.IsNullOrEmpty(customer["tu_customerbenefit"].ToString())) return;

                    //var teams = dynamicsHelper.GetUserTeamsCol(localContext.PluginExecutionContext.UserId);
                    var user = dynamicsHelper.GetLoggedOnUser(localContext.PluginExecutionContext.UserId);
                    if (user.Entities[0].Attributes.Contains("tu_office") && user.Entities[0]["tu_office"] != null)
                    {
                        var team = dynamicsHelper.GetOfficeCol(((EntityReference)user.Entities[0]["tu_office"]).Id);

                        tracingService.Trace("After GetOffice ");

                        if (team?.Entities?.Count < 0) return;
                        var plans = dynamicsHelper.GetTeamPlansCol(team[0].Id);
                        var aggregatedPlans = dynamicsHelper.GetPlansPremiumSum(team[0].Id);

                        if (aggregatedPlans.Entities.Count > 0)
                        {
                            customer["tu_premium"] = aggregatedPlans.Entities[0].Attributes.Contains("premium_sum") ? 
                                ((Money)((AliasedValue)aggregatedPlans.Entities[0]["premium_sum"]).Value).Value : 0;

                            customer["tu_firstpremium"] = aggregatedPlans.Entities[0].Attributes.Contains("initialPremium_sum") ?
                                ((Money)((AliasedValue)aggregatedPlans.Entities[0]["initialPremium_sum"]).Value).Value : 0;
                            organizationService.Update(customer);
                        }

                        tracingService.Trace("After GetTeamPlans ");
                        tracingService.Trace("Get Releated Benefits for each plan");
                        foreach (var plan in plans.Entities)
                        {
                            var Benefits = dynamicsHelper.GetBenefitsCol(plan.Id);
                            tracingService.Trace("After GetBenefitsCollection");

                            foreach (var benefit in Benefits.Entities)
                            {
                                try
                                {
                                    var customerBenefitId = dynamicsHelper.CreateCustomerBenefits(benefit, customer,plan);
                                }

                                catch (Exception ex)
                                {

                                }
                            }
                        }
                    }
                }
                catch (Exception Exc)
                {
                    throw new InvalidPluginExecutionException(Exc.Message.ToString());
                }
            }
            else
            {
                throw new InvalidPluginExecutionException("Null localContext");
            }
        }
    }
}
