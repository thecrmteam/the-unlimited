﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TCT.D365.TheUnlimited.Plugins.Abstracts;
using TCT.D365.TheUnlimited.Plugins.Interfaces;

namespace TCT.D365.TheUnlimited.Plugins.Implementations
{
    public class BasicAuthRequest : AbstractWarewolfConnection, IWarewolfRequest
    {
        public BasicAuthRequest(HttpClient httpClient, string url, string password, string username) 
            : base(httpClient, url, password, username)
        {
        }

        public override void Close()
        {
            HttpClient.Dispose();
        }

        public override void Connect()
        {
            var authToken = Encoding.ASCII.GetBytes($"{UserName}:{Password}");
            HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic",
                Convert.ToBase64String(authToken));
        }

        public async Task<string> Execute(string requestBody)
        {
            var response = await HttpClient.PostAsync(Url, new StringContent(requestBody, Encoding.UTF8, "application/json"));
            return response.Content.ReadAsStringAsync().Result;
        }
    }
}
