﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TCT.D365.TheUnlimited.Plugins.Abstracts;
using TCT.D365.TheUnlimited.Plugins.Interfaces;

namespace TCT.D365.TheUnlimited.Plugins.Implementations
{
    public class ViciRequest : AbstractWarewolfConnection, IWarewolfRequest
    {
        public ViciRequest(HttpClient httpClient, string url) : base(httpClient, url)
        {
        }

        public override void Close()
        {
            HttpClient.Dispose();
        }

        public override void Connect()
        {
            HttpClient.Timeout = TimeSpan.FromSeconds(60);
        }

        public async Task<string> Execute(string requestBody)
        {
            var response = await HttpClient.GetAsync(Url + requestBody);
            return response.Content.ReadAsStringAsync().Result;
        }
    }
}
