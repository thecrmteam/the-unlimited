﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TCT.D365.TheUnlimited.Plugins.Abstracts;
using TCT.D365.TheUnlimited.Plugins.Interfaces;

namespace TCT.D365.TheUnlimited.Plugins.Implementations
{

    public class ApiKeyRequest : AbstractWarewolfConnection, IWarewolfRequest
    {
        public ApiKeyRequest(HttpClient httpClient, string url, string apiKey) : 
            base(httpClient, url, apiKey)
        {
        }

        public override void Close()
        {
            HttpClient.Dispose();
        }

        public override void Connect()
        {
            HttpClient.DefaultRequestHeaders.Add("x-api-key", ApiKey);
        }

        public async Task<string> Execute(string requestBody)
        {
            var response = await HttpClient.PostAsync(Url, new StringContent(requestBody, Encoding.UTF8, "application/json"));
            return response.Content.ReadAsStringAsync().Result;
        }
    }
}
