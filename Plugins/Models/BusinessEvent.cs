﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCT.D365.TheUnlimited.Plugins.Models
{
    public class BaseBussinessEvent
    {
        public BusinessEvent BusinessEvent { get; set; }
    }

    public class BusinessEvent
    {
        public string Event { get; set; }
        public string ExternalSourceType { get; set; }
        public Guid RequestID { get; set; }
        public Guid ReferenceId { get; set; }
        public Customer Customer { get; set; }
    }

    public class Customer
    {
        public string DisplayNumber { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobilePhone { get; set; }
        public string EmailAddress { get; set; }
        public string IdentityNo { get; set; }
        public string IdentityType { get; set; }
        public DateTime DateofBirth { get; set; }
        public int PlanId { get; set; }
        public List<Address> Address { get; set; }
        public List<Mandate> Mandate { get; set; }
    }

    public class Address
    {
        public string Type { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string Street3 { get; set; }
        public string City { get; set; }
        public string Suburb { get; set; }
        public string Country { get; set; }
        public int Code { get; set; }

    }

    public class Mandate
    {
        public Guid MandateId { get; set; }
        public string MandateShortId { get; set; }
        public decimal InstallmentAmount { get; set; }
        public decimal FirstCollectionAmount { get; set; }
        public int CollectionDay { get; set; }
        public DateTime DebitFromDate { get; set; }
        public bool FixedExtractDate { get; set; }
        public bool Postpone1stDebit { get; set; }
        public DateTime EffectiveFromDate { get; set; }
        public Payer Payer { get; set; }
        public List<Benefit> Benefits { get; set; }
    }

    public class Payer
    {
        public Party Details { get; set; }
        public Banking Banking { get; set; }
    }

    public class Banking
    {
        public string AccountNumber { get; set; }
        public int BranchCode { get; set; }
        public string BankAccountType { get; set; }
        public string BankName { get; set; }
    }

    public class Party
    {
        public string DisplayNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobilePhone { get; set; }
        public string IdentityNo { get; set; }
        public string IdentityType { get; set; }
        public string EmailAddress { get; set; }
        public DateTime DateofBirth { get; set; }
    }

    public class Benefit
    {
        public string Category { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
        public List<Beneficiary> Beneficiaries { get; set; }
        public Sim LinkToSim { get; set; }
    }

    public class Beneficiary
    {
        public string Type { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateofBirth { get; set; }
    }

    public class Sim
    {
        public bool IssueNewSIM { get; set; }
        public string MSISDN { get; set; }
        public string ICCID { get; set; }
    }
}
