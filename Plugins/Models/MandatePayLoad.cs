﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCT.D365.TheUnlimited.Plugins.Models
{
    public class MandatePayLoad
    {
        public Guid MandateId { get; set; }
        public string MandateShortId { get; set; }
        public decimal InstallmentAmount { get; set; }
        public decimal FirstCollectionAmount { get; set; }
        public int CollectionDay { get; set; }
        public DateTime DebitFromDate { get; set; }
        public bool FixedExtractDate { get; set; }
        public bool Postpone1stDebit { get; set; }
        public Payer Payer { get; set; }
        public List<Benefit> Benefits { get; set; }

    }
}
