﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCT.D365.TheUnlimited.Plugins.Models
{
    public class LinkToSim
    {
        public bool IssueNewSIM { get; set; }
        public string MSISDN { get; set; }
        public string ICCID { get; set; }
    }
}
