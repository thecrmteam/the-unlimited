﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCT.D365.TheUnlimited.Plugins.Models
{
    public class NewSaleBusinessEvent
    {
        public MandateInformation BusinessEvent { get; set; }
    }

    public class NewCustomerBusinessEvent
    {
        public string ExternalSourceType { get; set; }
        public DateTime DebitFromDate { get; set; }
        public Guid RequestID { get; set; }
        public string ReferenceId { get; set; }
        public Guid MandateId { get; set; }
        public List<Benefit> Benefits { get; set; }
        public Party Customer { get; set; }
    }

    public class CancelMandateBusinessEvent
    {
        public string ExternalSourceType { get; set; }
        public Guid RequestID { get; set; }
        public Guid MandateId { get; set; }
        public DateTime EffectiveFromDate { get; set; }
        public string ReferenceId { get; set; }
        public List<Benefit> Benefits { get; set; }
    }

    public class MandateInformationChangeBusinessEvent
    {
        public MandateInformation Mandate { get; set; }
    }

    public class BenefitRemovedBusinessEvent
    {
        public MandateInformation Mandate { get; set; }
        public Party BenefitHolderDetails { get; set; }
        public List<Benefit> Benefits { get; set; }
    }

    public class MandateInformation
    {
        public string Event { get; set; }
        public string ExternalSourceType { get; set; }
        public Guid RequestID { get; set; }
        public string ReferenceId { get; set; }
        public Party Customer { get; set; }
    }
}
