﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCT.D365.TheUnlimited.Plugins.Models
{
    public class PayerDetails
    {
        public string DisplayNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobilePhone { get; set; }
        public string IdentityNo { get; set; }
        public string IdentityType { get; set; }
        public string EmailAddress { get; set; }
        public DateTime DateofBirth { get; set; }
    }
}
