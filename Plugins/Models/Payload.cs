﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCT.D365.TheUnlimited.Plugins.Models
{
    public class Payload
    {
        public Properties properties { get; set; }
        public string routing_key { get; set; }
        public string payload { get; set; }
        public string payload_encoding { get; set; }
    }

    public class Properties
    {

    }
}
