﻿if (typeof (CrmJs) == "undefined") {
    CrmJs = { __namespace: true };
}

if (typeof (CrmJs.Legal) == "undefined") {
    CrmJs.Legal = { __namespace: true };
}
CrmJs.Legal.Blacklisting = {
    __namespace: true,
    FormContextObject: null,
    XrmObject: function () {
        if (typeof (Xrm) == "undefined")
            return parent.Xrm;
        else
            return Xrm;
    },

    OnChangeIdNo: function () {
        var idNo = this.XrmObject().Page.getAttribute("tu_idno");
        var isValidIdNumber = false;
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(idNo)) {
                isValidIdNumber = CrmJs.Common.ValidateIdNo(idNo);
                if (isValidIdNumber) {
                    this.XrmObject().Page.getControl("tu_idno").clearNotification();
                }
                else {
                    this.XrmObject().Page.getControl("tu_idno").setNotification("INVALID_ID_NUMBER");
                }
            }
        }
    },

    OnChangeAccountNumber: function (executionContext) {
        var accountNumber = this.XrmObject().Page.getAttribute("tu_accountnumber");
        var isValidAccNumber = false;
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(accountNumber)) {
                isValidAccNumber = CrmJs.Common.ValidateAccNumber(executionContext, accountNumber);
                if (isValidAccNumber)
                    this.XrmObject().Page.getControl("tu_accountnumber").clearNotification();
                else
                    this.XrmObject().Page.getControl("tu_accountnumber").setNotification("INVALID_ID_NUMBER");
                
            }
        }
    }
}