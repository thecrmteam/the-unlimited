﻿if (typeof (CrmJs) == "undefined") {
    CrmJs = { __namespace: true };
}
if (typeof (CrmJs.Sales) == "undefined") {
    CrmJs.Sales = { __namespace: true };
}

CrmJs.Sales.SalesCapture = {
    __namespace: true,
    XrmObject: null,
    FormContextObject: null,
    ApiVersion: "/api/data/v9.1/",

    GetXrmObjectObject: function () {
        if (parent != null && parent != undefined && parent.getCurrentXrm() != undefined) {
            CrmJs.Sales.SalesCapture.XrmObject = parent.getCurrentXrm();
        }
        else {
            CrmJs.Sales.SalesCapture.XrmObject = Xrm;
        }
    },

    OnLoad: function (executionContext) {
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidValue(executionContext)) {
                CrmJs.Common.SetFormContextObject(executionContext);
            }
        }


    },

    ValidateBankingDetails: function () {
        CrmJs.Sales.SalesCapture.GetXrmObjectObject();
        //CrmJs.Sales.SalesCapture.XrmObject.Page.getControl("tu_accountnumber").clearNotification();
        //CrmJs.Sales.SalesCapture.XrmObject.Page.getControl("tu_branchcode").clearNotification();
        CrmJs.Sales.SalesCapture.XrmObject.Page.ui.clearFormNotification("COL_VALUE_ERROR");
        CrmJs.Sales.SalesCapture.XrmObject.Page.ui.clearFormNotification("BANK_VALUE_ERROR");
        CrmJs.Sales.SalesCapture.XrmObject.Page.ui.clearFormNotification("COLCREATE_VALUE_ERROR");

        CrmJs.Sales.SalesCapture.XrmObject.Utility.showProgressIndicator("Processing");
        CrmJs.Sales.SalesCapture.CdvCheck();

    },

    ValidateAccNumber: function (executionContext) {
        if (CrmJs.Common != "undefined") {
            CrmJs.Common.SetFormContextObject(executionContext);
            var accNumber = CrmJs.Common.FormContextObject.getAttribute("tu_accountnumber");
            CrmJs.Common.FormContextObject.getControl("tu_accountnumber").clearNotification();

            if (!CrmJs.Common.IsValidAttributeControl(accNumber)) return;

            if (CrmJs.Common.IsValidValue(accNumber.getValue())) {
                if (isNaN(accNumber.getValue().trim()) || accNumber.getValue().trim().length == 0
                    || accNumber.getValue().trim().length < 6 || accNumber.getValue().trim().length > 11) {
                    CrmJs.Common.FormContextObject.getControl("tu_accountnumber").setNotification("INVALID_ACCOUNT_NUMBER");
                } else {
                    CrmJs.Common.FormContextObject.getControl("tu_accountnumber").clearNotification();
                }
            } else {
                CrmJs.Common.FormContextObject.getControl("tu_accountnumber").setNotification("INVALID_ACCOUNT_NUMBER");
            }
        }
    },

    CdvCheck: function () {
        var parameters = {};
        var bankValidation = CrmJs.Sales.SalesCapture.XrmObject.Page.getAttribute("tu_bankvalidation");
        parameters.AccountNumber = CrmJs.Sales.SalesCapture.XrmObject.Page.getAttribute("tu_accountnumber").getValue();
        parameters.AccountType = CrmJs.Sales.SalesCapture.XrmObject.Page.getAttribute("tu_accounttype").getText();
        parameters.BranchCode = CrmJs.Sales.SalesCapture.XrmObject.Page.getAttribute("tu_branchcode").getValue();

        var req = new XMLHttpRequest();
        req.open("POST", CrmJs.Sales.SalesCapture.XrmObject.Page.context.getClientUrl() +
            CrmJs.Sales.SalesCapture.ApiVersion + "/tu_CDVVerification", true);
        req.setRequestHeader("OData-MaxVersion", "4.0");
        req.setRequestHeader("OData-Version", "4.0");
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.onreadystatechange = function () {
            if (this.readyState === 4) {
                req.onreadystatechange = null;
                if (this.status === 200) {
                    var results = JSON.parse(this.response);

                    if (results.Success) {
                        var resMessage = JSON.parse(results.Message);

                        if (resMessage.length > 0 && resMessage[0].valid) {
                            CrmJs.Sales.SalesCapture.XrmObject.Page.ui.setFormNotification("BANKING_DETAILS_VALIDATED", "INFO", "BANK_VALUE_ERROR");
                            //CrmJs.Sales.SalesCapture.CreateCollection();
                            CrmJs.Sales.SalesCapture.XrmObject.Utility.closeProgressIndicator();
                            if (CrmJs.Sales.SalesCapture.IsValidAttributeControl(bankValidation)) {
                                bankValidation.setValue(827210000);
                            }

                        } else if (CrmJs.Sales.SalesCapture.IsValidValue(resMessage.status)
                            && resMessage.status === 400
                            && resMessage.errors.length > 0) {

                            if (CrmJs.Sales.SalesCapture.IsValidValue(resMessage.errors[0].errors[0].BranchCode[0])) {
                                CrmJs.Sales.SalesCapture.XrmObject.Page.ui.setFormNotification("BANKING_DETAILS_INVALID : "
                                    + resMessage.errors[0].errors[0].BranchCode[0], "ERROR", "BANK_VALUE_ERROR");
                                if (CrmJs.Sales.SalesCapture.IsValidAttributeControl(bankValidation))
                                    bankValidation.setValue(827210001);
                                CrmJs.Sales.SalesCapture.XrmObject.Utility.closeProgressIndicator();
                            }
                        } else if (resMessage.length > 0) {
                            CrmJs.Sales.SalesCapture.XrmObject.Page.ui.setFormNotification("BANKING_DETAILS_INVALID : "
                                + resMessage[0].title, "ERROR", "BANK_VALUE_ERROR");
                            if (CrmJs.Sales.SalesCapture.IsValidAttributeControl(bankValidation))
                                bankValidation.setValue(827210001);
                            CrmJs.Sales.SalesCapture.XrmObject.Utility.closeProgressIndicator();
                        }
                    } else {
                        CrmJs.Sales.SalesCapture.XrmObject.Page.ui.setFormNotification("BANKING_DETAILS_INVALID : "
                            + results.Message, "ERROR", "BANK_VALUE_ERROR");
                        if (CrmJs.Sales.SalesCapture.IsValidAttributeControl(bankValidation))
                            bankValidation.setValue(827210001);
                        CrmJs.Sales.SalesCapture.XrmObject.Utility.closeProgressIndicator();
                    }
                } else {
                    CrmJs.Sales.SalesCapture.XrmObject.Page.ui.setFormNotification("BANKING_DETAILS_INVALID : "
                        + this.statusText, "ERROR", "BANK_VALUE_ERROR");
                    if (CrmJs.Sales.SalesCapture.IsValidAttributeControl(bankValidation))
                        bankValidation.setValue(827210001);
                    CrmJs.Sales.SalesCapture.XrmObject.Utility.closeProgressIndicator();
                }
            }
        };
        req.send(JSON.stringify(parameters));


    },

    ValidateBranchCode: function () {
        var branchCode = CrmJs.Sales.SalesCapture.XrmObject.Page.getAttribute("tu_branchcode").getValue();
        if (CrmJs.Sales.SalesCapture.HasValue("tu_branchcode")) {
            if (isNaN(branchCode.trim()) || branchCode.trim().length == 0 || branchCode.trim().length > 6) {
                CrmJs.Sales.SalesCapture.XrmObject.Page.getControl("tu_branchcode").setNotification("INVALID_BRANCH_CODE");
                return false;
            }
        } else {
            CrmJs.Sales.SalesCapture.XrmObject.Page.getControl("tu_branchcode").setNotification("INVALID_BRANCH_CODE");
            return false;
        }

        return true;
    },

    CreateCollection: function () {
        var parameters = {};
        var entity = {};
        entity.id = CrmJs.Sales.SalesCapture.XrmObject.Page.data.entity.getId(); //"109e5c93-bc6d-ea11-a811-000d3a3a7902";
        entity.entityType = CrmJs.Sales.SalesCapture.XrmObject.Page.data.entity.getEntityName();//"contact";
        parameters.entity = entity;

        var tu_CollectRequest = {
            entity: parameters.entity,

            getMetadata: function () {
                return {
                    boundParameter: "entity",
                    parameterTypes: {
                        "entity": {
                            "typeName": "mscrm.contact",
                            "structuralProperty": 5
                        }
                    },
                    operationType: 0,
                    operationName: "tu_Collect"
                };
            }
        };

        CrmJs.Sales.SalesCapture.XrmObject.WebApi.online.execute(tu_CollectRequest).then(
            function success(result) {
                if (result.ok) {
                    return true;
                }
            },
            function (error) {
                CrmJs.Sales.SalesCapture.XrmObject.Page.ui.setFormNotification(error.message, "ERROR", "COLCREATE_VALUE_ERROR");
                CrmJs.Sales.SalesCapture.XrmObject.Utility.closeProgressIndicator();
            }
        );
    },

    PopBankingDetails: function (SelectedEntityTypeName, PrimaryEntityTypeName, PrimaryControl, SelectedControl) {
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidValue(SelectedControl)) {
                var parameters = {};

                parameters["tu_customer"] = PrimaryControl;
                parameters["tu_customername"] = SelectedControl.data.entity.getEntityReference().name;
                parameters["tu_customertype"] = PrimaryEntityTypeName;

                var pageInput = {
                    pageType: "entityrecord",
                    entityName: SelectedEntityTypeName,
                    data: parameters,
                    //entityId: CrmJs.Sales.BenefitCapture.FormContextObject.data.entity.getId(),
                    //formId: entityName == "contact" ? CrmJs.Sales.BenefitCapture.ContactBenFormId : CrmJs.Sales.BenefitCapture.VehicleBenFormId,
                };

                Xrm.Navigation.navigateTo(pageInput, CrmJs.Sales.SalesCapture.NavigationOptions()).then(
                    function success() {
                        SelectedControl.data.refresh();
                        //CrmJs.Sales.BenefitCapture.XrmObject.Utility.closeProgressIndicator();
                    },
                    function error(error) {
                        CrmJs.Sales.SalesCapture.FormContextObject.ui.setFormNotification(error.message, "ERROR", "CREATE_BEN_ERROR");
                        //CrmJs.Sales.BenefitCapture.XrmObject.Utility.closeProgressIndicator();
                    }
                );
            }
        }
    },

    NavigationOptions: function () {
        var navigationOptions = {
            target: 2,
            height: { value: 100, unit: "%" },
            width: { value: 100, unit: "%" },
            position: 1
        };

        return navigationOptions;
    },

    PopulateBranchCode: function (executionContext) {
        if (CrmJs.Common != "undefined") {
            CrmJs.Common.SetFormContextObject(executionContext);
            var bank = CrmJs.Common.FormContextObject.getAttribute("tu_bankname");

            if (!CrmJs.Common.IsValidAttributeControl(bank)) return;

            if (CrmJs.Common.IsValidAttributeControl(bank)
                && CrmJs.Common.IsValidAttributeControl(bank.getValue())) {
                var branchCode = CrmJs.Common.FormContextObject.getAttribute("tu_branchcode");
                if (bank.getValue().toString() == "51001" )
                    branchCode.setValue("0" + bank.getValue().toString());
                else
                    branchCode.setValue(bank.getValue().toString());
            }
        }
    },

    OnSave: function (executionContext) {
        CrmJs.Sales.SalesCapture.GetXrmObjectObject();
        var saveEvent = executionContext.getEventArgs();
        var bankValidation = CrmJs.Sales.SalesCapture.XrmObject.Page.getAttribute("tu_bankvalidation");
        var bankName = CrmJs.Sales.SalesCapture.XrmObject.Page.getAttribute("tu_bankname");
        var accountNumber = CrmJs.Sales.SalesCapture.XrmObject.Page.getAttribute("tu_accountnumber");
        if (CrmJs.Sales.SalesCapture.IsValidAttributeControl(bankValidation)) {
            if (bankValidation.getValue() == null) {
                CrmJs.Sales.SalesCapture.XrmObject.Page.ui.setFormNotification("PLEASE VALIDATE BANKING DETAILS BEFORE SAVING : ",
                    "ERROR", "NO_VALIDATION_ERROR");
                saveEvent.preventDefault();
            }
            else if (bankValidation.getValue() == 827210001) {
                CrmJs.Sales.SalesCapture.XrmObject.Page.ui.clearFormNotification("NO_VALIDATION_ERROR");
                saveEvent.preventDefault();
            }
            else {
                if (CrmJs.Common != "undefined") {
                    if (CrmJs.Common.IsValidAttributeControl(bankName)
                        && CrmJs.Common.IsValidAttributeControl(accountNumber)) {
                        var columns = "tu_accountnumber,tu_bankname";
                        var filter = "tu_accountnumber eq '" + accountNumber.getValue() +
                            "' and tu_bankname eq " + bankName.getValue();
                        var bankAccount = CrmJs.Common.RetrieveMultiple("tu_bankaccountdetailses", filter, columns);
                        if (bankAccount != "undefinded") {
                            if (bankAccount.value.length > 0) {
                                saveEvent.preventDefault();
                                CrmJs.Sales.SalesCapture.XrmObject.Page.ui.setFormNotification("BANKING ACCOUNT ALREADY EXISTS : ",
                                    "ERROR", "BANK_ACCOUNT_DUPLICATION_ERROR");
                                CrmJs.Sales.SalesCapture.XrmObject.Page.ui.clearFormNotification("NO_VALIDATION_ERROR");
                                CrmJs.Sales.SalesCapture.XrmObject.Page.ui.clearFormNotification("BANK_VALUE_ERROR");
                                CrmJs.Sales.SalesCapture.XrmObject.Page.ui.clearFormNotification("BANK_VALUE_ERROR");
                            }
                            else {
                                CrmJs.Sales.SalesCapture.XrmObject.Page.ui.clearFormNotification("NO_VALIDATION_ERROR");
                                CrmJs.Sales.SalesCapture.XrmObject.Page.ui.clearFormNotification("BANK_ACCOUNT_DUPLICATION_ERROR");
                            }
                        }
                    }
                }
            }

        }
    },

    HasValue: function (attributeName) {
        return CrmJs.Sales.SalesCapture.XrmObject.Page.getAttribute(attributeName) != null
            && CrmJs.Sales.SalesCapture.XrmObject.Page.getAttribute(attributeName).getValue() != null ? true : false;
    },

    IsValidValue: function (value) {
        if (value == null || value == "" || value == "undefined" || value == "null")
            return false;
        return true;
    },

    IsValidAttributeControl: function (fieldName) {
        if (fieldName == null || fieldName == "" || fieldName == "undefined" || fieldName == "null")
            return false;
        return true;
    },

    GetFormType: function () {
        var formType = CrmJs.Sales.SalesCapture.XrmObject.Page.ui.getFormType();
        return formType;
    },

    RemoveBrackets: function (guid) {
        if (CrmJs.Sales.SalesCapture.IsValidValue(guid))
            return guid.replace(/[{}]/g, '');
        return null;
    }
}
