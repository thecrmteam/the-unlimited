﻿if (typeof (CrmJs) == "undefined") {
    CrmJs = { __namespace: true };
}
if (typeof (CrmJs.Sales) == "undefined") {
    CrmJs.Sales = { __namespace: true };
}

CrmJs.Sales.VahicleCapture = {
    __namespace: true,
    FormContextObject: null,
    XrmObject: function () {
        if (typeof (Xrm) == "undefined")
            return parent.Xrm;
        else
            return Xrm;
    },

    OnLoad: function (executionContext) {
        if (CrmJs.Common.IsValidAttributeControl(executionContext) &&
			CrmJs.Common.IsValidAttributeControl(executionContext.getFormContext())) {
            CrmJs.Sales.VahicleCapture.FormContextObject = executionContext.getFormContext();

            var customerId = localStorage.getItem("CustomerRefId");
            var customerName = localStorage.getItem("CustomerRefName");
            var customerType = localStorage.getItem("CustomerRefType");

            var custBenefitId = localStorage.getItem("CustBenefitRefId");
            var custBenefitName = localStorage.getItem("CustBenefitRefName");
            var custBenefitType = localStorage.getItem("CustBenefitRefType");

            if (CrmJs.Common.IsValidAttributeControl(customerId)
                && CrmJs.Common.IsValidAttributeControl(custBenefitId)) {

                CrmJs.Common.SetLookupValue(CrmJs.Sales.VahicleCapture.FormContextObject.getAttribute("tu_customer"),
                    customerId, customerName, customerType);
                CrmJs.Common.SetLookupValue(CrmJs.Sales.VahicleCapture.FormContextObject.getAttribute("tu_customerbenefit"),
                    custBenefitId, custBenefitName, custBenefitType);

            }
        }
    },

    ValidateServiceDate: function () {
        var lastServiceDate = this.XrmObject().Page.getAttribute("tu_lastservicedate");
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(lastServiceDate) && CrmJs.Common.IsValidAttributeControl(lastServiceDate.getValue())) {
                var date = new Date();
                if (lastServiceDate.getValue() > date) {
                    this.XrmObject().Page.getControl("tu_lastservicedate").setNotification("LAST SERVICE DATE CANNOT BE IN FUTURE");
                }
                else {
                    this.XrmObject().Page.getControl("tu_lastservicedate").clearNotification();
                }
            }
        }
    }
}