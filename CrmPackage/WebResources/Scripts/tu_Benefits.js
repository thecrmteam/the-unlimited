﻿if (typeof (CrmJs) == "undefined") {
    CrmJs = { __namespace: true };
}
if (typeof (CrmJs.Sales) == "undefined") {
    CrmJs.Sales = { __namespace: true };
}

CrmJs.Sales.BenefitCapture = {
    __namespace: true,
    FormContextObject: null,
	XrmObject: null,
	SelectedEntityTypeName: null,
	PrimaryEntityTypeName: null,
	FirstPrimaryItemId: null,
	PrimaryControl: null,
    SelectedControl: null,
    ContactBenFormId: null,
    VehicleBenFormId: null,

    GetXrmObject: function () {
        if (parent.Xrm != null && parent.Xrm != undefined)
            CrmJs.Sales.BenefitCapture.XrmObject = parent.Xrm;
        else
            CrmJs.Sales.BenefitCapture.XrmObject = Xrm;
    },
	
    CaptureBenefitDetails: function (executionContext) {
        if (CrmJs.Common.IsValidAttributeControl(executionContext) &&
            CrmJs.Common.IsValidAttributeControl(executionContext.getFormContext())) {
            CrmJs.Sales.BenefitCapture.PopCustomerBenefit(executionContext.getFormContext().entityReference)
        }
    },

    OnLoad: function (executionContext) {
        CrmJs.Sales.BenefitCapture.GetXrmObject();
        CrmJs.Sales.BenefitCapture.XrmObject.Page.ui.clearFormNotification("SIM_INFO");
        CrmJs.Sales.BenefitCapture.XrmObject.Page.ui.clearFormNotification("VEHICLE_INFO");
        CrmJs.Sales.BenefitCapture.XrmObject.Page.ui.clearFormNotification("DEPENDENT_INFO");

        if (CrmJs.Common.IsValidAttributeControl(executionContext) &&
            CrmJs.Common.IsValidAttributeControl(executionContext.getFormContext())) {
            CrmJs.Sales.BenefitCapture.FormContextObject = executionContext.getFormContext();

            var customer = CrmJs.Sales.BenefitCapture.FormContextObject.getAttribute("tu_customer");
            var custBenefit = CrmJs.Sales.BenefitCapture.FormContextObject.data.entity;

            if (CrmJs.Common.IsValidAttributeControl(customer.getValue())
                && CrmJs.Common.IsValidAttributeControl(custBenefit.getEntityReference())) {
                localStorage.setItem("CustomerRefId", customer.getValue()[0].id);
                localStorage.setItem("CustomerRefName", customer.getValue()[0].name);
                localStorage.setItem("CustomerRefType", customer.getValue()[0].entityType);

                localStorage.setItem("CustBenefitRefId", custBenefit.getEntityReference().id);
                localStorage.setItem("CustBenefitRefName", custBenefit.getEntityReference().name);
                localStorage.setItem("CustBenefitRefType", custBenefit.getEntityReference().entityType);
            }

            var category = CrmJs.Sales.BenefitCapture.FormContextObject.getAttribute("tu_benefitcategory");
            var vehicleGrid = CrmJs.Sales.BenefitCapture.FormContextObject.getControl("Vehicle");
            var BeneficiaryGrid = CrmJs.Sales.BenefitCapture.FormContextObject.getControl("Beneficiary");
            var simGrid = CrmJs.Sales.BenefitCapture.FormContextObject.getControl("SIM");

            if (CrmJs.Common.IsValidAttributeControl(category)
                && CrmJs.Common.IsValidAttributeControl(vehicleGrid)
                && CrmJs.Common.IsValidAttributeControl(BeneficiaryGrid)
                && CrmJs.Common.IsValidAttributeControl(simGrid)) {
					switch(category.getValue()) {
						case 1:{
							CrmJs.Common.SetControlVisibility(BeneficiaryGrid, true);
                            CrmJs.Common.SetControlVisibility(vehicleGrid, false);
                            CrmJs.Common.SetControlVisibility(simGrid, false);
							break;
                        }
                        case 2: {
                            CrmJs.Common.SetControlVisibility(BeneficiaryGrid, true);
                            CrmJs.Common.SetControlVisibility(vehicleGrid, false);
                            CrmJs.Common.SetControlVisibility(simGrid, false);
                            break;
                        }
						case 6:{
							CrmJs.Common.SetControlVisibility(BeneficiaryGrid, false);
                            CrmJs.Common.SetControlVisibility(vehicleGrid, false);
                            crmj.Common.SetControlVisibility(simGrid, true);
							break;
						}
						case 3:{
							CrmJs.Common.SetControlVisibility(BeneficiaryGrid, false);
                            CrmJs.Common.SetControlVisibility(vehicleGrid, true);
                            CrmJs.Common.SetControlVisibility(simGrid, false);
							break;
						}
						default:{
							CrmJs.Common.SetControlVisibility(BeneficiaryGrid, false);
                            CrmJs.Common.SetControlVisibility(vehicleGrid, false);
                            CrmJs.Common.SetControlVisibility(simGrid, false);
						}
					}
            }
        }
    },
	
    PopCustomerBenefit: function (recordReference) {
        Xrm.Navigation.navigateTo({
            pageType: "entityrecord",
            entityName: recordReference.entityType,
            entityId: recordReference.id
        }, {
            target: 2,
                height: { value: 100, unit: "%" },
                width: { value: 100, unit: "%" },
                position: 1
        });
    },

    PopDependentCapture: function (type) {
        var customerType = 0;
        CrmJs.Sales.BenefitCapture.GetXrmObject();
        var entityId = CrmJs.Sales.BenefitCapture.XrmObject.Page.data.entity.getId();
        var customerBenefit = CrmJs.Sales.BenefitCapture.XrmObject.Page.getAttribute("tu_benefit");
        var contactGrid = CrmJs.Sales.BenefitCapture.XrmObject.Page.ui.controls.get("Beneficiary");
        var benefitcategory = CrmJs.Sales.BenefitCapture.XrmObject.Page.getAttribute("tu_benefitcategory");
        var benefitName = "";

        if (CrmJs.Sales.BenefitCapture.IsValidAttributeControl(customerBenefit) && customerBenefit.getValue() != null) {
            benefitName = customerBenefit.getValue()[0].name;
        }

        if (CrmJs.Sales.BenefitCapture.IsValidAttributeControl(benefitcategory)) {
            if (benefitcategory.getValue() == 1 || benefitcategory.getValue() == 2) {

                CrmJs.Sales.BenefitCapture.XrmObject.Page.ui.clearFormNotification("SIM_INFO");
                CrmJs.Sales.BenefitCapture.XrmObject.Page.ui.clearFormNotification("VEHICLE_INFO");
                CrmJs.Sales.BenefitCapture.XrmObject.Page.ui.clearFormNotification("DEPENDENT_INFO");
                switch (type) {
                    case "Spouse":
                        customerType = 827210002;
                        break;
                    case "Adult":
                        customerType = 827210001;
                        break;
                    case "Child":
                        customerType = 827210003;
                        break;

                }

                var entityName = "contact";
                var pageInput = {
                    pageType: "entityrecord",
                    entityName: entityName,
                    formId: "89C60568-738D-4CF4-9DEF-983D48956EE3",
                    data: {
                        tu_type: customerType,
                        tu_customerbenefit: entityId,
                        tu_customerbenefitname: benefitName,
                        tu_customerbenefittype: "tu_customerbenefit"
                    }
                };

                CrmJs.Sales.BenefitCapture.XrmObject.Navigation.navigateTo(pageInput, CrmJs.Sales.BenefitCapture.NavigationOptions()).then(
                    function success() {
                        if (CrmJs.Sales.BenefitCapture.IsValidAttributeControl(contactGrid)) {
                            contactGrid.refresh();
                        }
                    },
                    function error(error) {

                    }
                );

            }
            else {
                CrmJs.Sales.BenefitCapture.XrmObject.Page.ui.clearFormNotification("SIM_INFO");
                CrmJs.Sales.BenefitCapture.XrmObject.Page.ui.clearFormNotification("VEHICLE_INFO");
                CrmJs.Sales.BenefitCapture.XrmObject.Page.ui.setFormNotification("CANNOT CAPTURE DEPENDENT INFO AGAINST THIS TYPE OF BENEFIT", "INFO", "DEPENDENT_INFO");
            }
        }
    },

    PopVehicleCapture: function () {
        CrmJs.Sales.BenefitCapture.GetXrmObject();
        var entityId = CrmJs.Sales.BenefitCapture.XrmObject.Page.data.entity.getId();
        var customerBenefit = CrmJs.Sales.BenefitCapture.XrmObject.Page.getAttribute("tu_benefit");
        var vehicleGrid = CrmJs.Sales.BenefitCapture.XrmObject.Page.ui.controls.get("Vehicle");
        var benefitcategory = CrmJs.Sales.BenefitCapture.XrmObject.Page.getAttribute("tu_benefitcategory");
        var benefitName = "";
        if (CrmJs.Sales.BenefitCapture.IsValidAttributeControl(customerBenefit) && customerBenefit.getValue() != null) {
            benefitName = customerBenefit.getValue()[0].name;
        }

        if (CrmJs.Sales.BenefitCapture.IsValidAttributeControl(benefitcategory)) {
            if (benefitcategory.getValue() != 3) {
                CrmJs.Sales.BenefitCapture.XrmObject.Page.ui.clearFormNotification("SIM_INFO");
                CrmJs.Sales.BenefitCapture.XrmObject.Page.ui.clearFormNotification("DEPENDENT_INFO");
                CrmJs.Sales.BenefitCapture.XrmObject.Page.ui.setFormNotification("CANNOT CAPTURE VEHICLE INFO AGAINST THIS TYPE OF BENEFIT", "INFO", "VEHICLE_INFO");
            }
            else {
                CrmJs.Sales.BenefitCapture.XrmObject.Page.ui.clearFormNotification("SIM_INFO");
                CrmJs.Sales.BenefitCapture.XrmObject.Page.ui.clearFormNotification("VEHICLE_INFO");
                CrmJs.Sales.BenefitCapture.XrmObject.Page.ui.clearFormNotification("DEPENDENT_INFO");
                var entityName = "tu_vehicle";
                var pageInput = {
                    pageType: "entityrecord",
                    entityName: entityName,
                    formId: "E229C032-9E2F-4872-96C7-9B3CFF145B90",
                    data: {
                        tu_customerbenefit: entityId,
                        tu_customerbenefitname: benefitName,
                        tu_customerbenefittype: "tu_customerbenefit"
                    }
                };

                CrmJs.Sales.BenefitCapture.XrmObject.Navigation.navigateTo(pageInput, CrmJs.Sales.BenefitCapture.NavigationOptions()).then(
                    function success() {
                        if (CrmJs.Sales.BenefitCapture.IsValidAttributeControl(vehicleGrid)) {
                            vehicleGrid.refresh();
                        }
                    },
                    function error(error) {

                    }
                );
            }

        }
    },

    PopSIMCapture: function () {
        CrmJs.Sales.BenefitCapture.GetXrmObject();
        var entityId = CrmJs.Sales.BenefitCapture.XrmObject.Page.data.entity.getId();
        var customerBenefit = CrmJs.Sales.BenefitCapture.XrmObject.Page.getAttribute("tu_benefit");
        var benefitcategory = CrmJs.Sales.BenefitCapture.XrmObject.Page.getAttribute("tu_benefitcategory");
        var simGrid = CrmJs.Sales.BenefitCapture.XrmObject.Page.ui.controls.get("SIM");
        var benefitName = "";
        if (CrmJs.Sales.BenefitCapture.IsValidAttributeControl(customerBenefit) && customerBenefit.getValue() != null)
            benefitName = customerBenefit.getValue()[0].name;

        if (CrmJs.Sales.BenefitCapture.IsValidAttributeControl(benefitcategory)) {
            if (benefitcategory.getValue() != 6) {
                CrmJs.Sales.BenefitCapture.XrmObject.Page.ui.clearFormNotification("VEHICLE_INFO");
                CrmJs.Sales.BenefitCapture.XrmObject.Page.ui.clearFormNotification("DEPENDENT_INFO");
                CrmJs.Sales.BenefitCapture.XrmObject.Page.ui.setFormNotification("CANNOT CAPTURE SIM INFO AGAINST THIS TYPE OF BENEFIT", "INFO", "SIM_INFO");
            }
            else {
                CrmJs.Sales.BenefitCapture.XrmObject.Page.ui.clearFormNotification("SIM_INFO");
                CrmJs.Sales.BenefitCapture.XrmObject.Page.ui.clearFormNotification("VEHICLE_INFO");
                CrmJs.Sales.BenefitCapture.XrmObject.Page.ui.clearFormNotification("DEPENDENT_INFO");
                var entityName = "tu_sim";
                var pageInput = {
                    pageType: "entityrecord",
                    entityName: entityName,
                    formId: "4CE5647E-D644-42C9-AC2A-378F9A93D029",
                    data: {
                        tu_customerbenefit: entityId,
                        tu_customerbenefitname: benefitName,
                        tu_customerbenefittype: "tu_customerbenefit"
                    }
                };

                CrmJs.Sales.BenefitCapture.XrmObject.Navigation.navigateTo(pageInput, CrmJs.Sales.BenefitCapture.NavigationOptions()).then(
                    function success() {
                        if (CrmJs.Sales.BenefitCapture.IsValidAttributeControl(simGrid)) {
                            simGrid.refresh();
                        }
                    },
                    function error(error) {

                    }
                );
            }
        }
    },

    PopBeneficiaryCapture: function (selectedEntityTypeName, primaryEntityTypeName, firstPrimaryItemId, primaryControl, selectedControl) {
        if (CrmJs.Common.IsValidAttributeControl(selectedEntityTypeName) &&
            CrmJs.Common.IsValidAttributeControl(primaryControl)) {

            CrmJs.Sales.BenefitCapture.GetXrmObject();
            CrmJs.Common.SetGlobalContextObject(CrmJs.Sales.BenefitCapture.XrmObject);
			CrmJs.Sales.BenefitCapture.XrmObject.Utility.showProgressIndicator("Processing");
			
			CrmJs.Sales.BenefitCapture.FormContextObject.ui.clearFormNotification("MAX_VALUE_ERROR");
			CrmJs.Sales.BenefitCapture.FormContextObject.ui.clearFormNotification("CREATE_BEN_ERROR");
			CrmJs.Sales.BenefitCapture.FormContextObject.ui.clearFormNotification("CREATE_BEN_ERROR");

            CrmJs.Sales.BenefitCapture.SetBeneficiaryFormId();
			CrmJs.Sales.BenefitCapture.SelectedEntityTypeName = selectedEntityTypeName;
			CrmJs.Sales.BenefitCapture.PrimaryEntityTypeName = primaryEntityTypeName;
			CrmJs.Sales.BenefitCapture.FirstPrimaryItemId = firstPrimaryItemId;
			CrmJs.Sales.BenefitCapture.PrimaryControl = primaryControl;
			CrmJs.Sales.BenefitCapture.SelectedControl = selectedControl;

			var maxAllowed = CrmJs.Common.IsValidAttributeControl(primaryControl.getAttribute("tu_maxallowed")) 
				&& CrmJs.Common.IsValidAttributeControl(primaryControl.getAttribute("tu_maxallowed").getValue())
				? primaryControl.getAttribute("tu_maxallowed").getValue() : 100;
				
			var qeury = "?$select="+selectedEntityTypeName+"id&$filter=_"+primaryEntityTypeName+"_value eq '"+CrmJs.Common.RemoveBrackets(firstPrimaryItemId)+"'"
            CrmJs.Sales.BenefitCapture.CheckMaxAllowed(qeury,maxAllowed);

        }
    },

    PopBeneficiary: function (executionContext) {
        if (CrmJs.Common.IsValidValue(CrmJs.Sales.BenefitCapture.VehicleBenFormId)
            || CrmJs.Common.IsValidValue(CrmJs.Sales.BenefitCapture.ContactBenFormId)) {

            CrmJs.Sales.BenefitCapture.FormContextObject = executionContext.getFormContext();
            var entityName = CrmJs.Sales.BenefitCapture.FormContextObject.data.entity.getEntityName();

            var pageInput = {
                pageType: "entityrecord",
                entityName: entityName,
                entityId: CrmJs.Sales.BenefitCapture.FormContextObject.data.entity.getId(),
                formId: entityName == "contact" ? CrmJs.Sales.BenefitCapture.ContactBenFormId : CrmJs.Sales.BenefitCapture.VehicleBenFormId,
            };

            Xrm.Navigation.navigateTo(pageInput, CrmJs.Sales.BenefitCapture.NavigationOptions()).then(
                function success() {
                    CrmJs.Sales.BenefitCapture.SelectedControl.refresh();
                    CrmJs.Sales.BenefitCapture.XrmObject.Utility.closeProgressIndicator();
                },
                function error(error) {
                    CrmJs.Sales.BenefitCapture.FormContextObject.ui.setFormNotification(error.message, "ERROR", "CREATE_BEN_ERROR");
                    CrmJs.Sales.BenefitCapture.XrmObject.Utility.closeProgressIndicator();
                }
            );
        }
    },

    NavigationOptions: function () {
        var navigationOptions = {
            target: 2,
            height: { value: 100, unit: "%" },
            width: { value: 100, unit: "%" },
            position: 1
        };

        return navigationOptions;
    },

    SetBeneficiaryFormId: function () {
        if (CrmJs.Common != "undefined") {
            CrmJs.Sales.BenefitCapture.ContactBenFormId = CrmJs.Common.GetEnvironmentVariable("tu_ContactBenFormId");
            CrmJs.Sales.BenefitCapture.VehicleBenFormId = CrmJs.Common.GetEnvironmentVariable("tu_VehicleBenFormId");
        }
    },

    IsValidAttributeControl: function (fieldName) {
        if (fieldName == null || fieldName == "" || fieldName == "undefined" || fieldName == "null")
            return false;
        return true;
    },

    SetLookupValue: function (lookupControl, id, name, entityType) {
        if (lookupControl != null)
            lookupControl.setValue([{ id: id.replace(/[{}]/g, ''), name: name, entityType: entityType }]);
    },

    CreateBeneficairy: function () {
        if (CrmJs.Common.IsValidValue(CrmJs.Sales.BenefitCapture.VehicleBenFormId)
            || CrmJs.Common.IsValidValue(CrmJs.Sales.BenefitCapture.ContactBenFormId)) {
            var parameters = {};
            
            parameters["tu_customerbenefit"] = CrmJs.Sales.BenefitCapture.FormContextObject.data.entity.getEntityReference().id;
            parameters["tu_customerbenefitname"] = CrmJs.Sales.BenefitCapture.FormContextObject.data.entity.getEntityReference().name;
            parameters["tu_customerbenefittype"] = CrmJs.Sales.BenefitCapture.FormContextObject.data.entity.getEntityReference().entityType;

            var pageInput = {
                pageType: "entityrecord",
                entityName: CrmJs.Sales.BenefitCapture.SelectedEntityTypeName,
                data: parameters,
                formId: CrmJs.Sales.BenefitCapture.SelectedEntityTypeName == "contact" ? CrmJs.Sales.BenefitCapture.ContactBenFormId : CrmJs.Sales.BenefitCapture.VehicleBenFormId,
            };

            Xrm.Navigation.navigateTo(pageInput, CrmJs.Sales.BenefitCapture.NavigationOptions()).then(
                function success() {
                    CrmJs.Sales.BenefitCapture.SelectedControl.refresh();
                    CrmJs.Common.SetControlVisibility(CrmJs.Sales.BenefitCapture.SelectedControl, true);
                    CrmJs.Sales.BenefitCapture.XrmObject.Utility.closeProgressIndicator();
                },
                function error(error) {
                    CrmJs.Sales.BenefitCapture.FormContextObject.ui.setFormNotification(error.message, "ERROR", "CREATE_BEN_ERROR");
                    CrmJs.Sales.BenefitCapture.XrmObject.Utility.closeProgressIndicator();
                }
            );

            //var entity = {};
            //entity["tu_customerbenefit@odata.bind"] = "/tu_customerbenefits("+CrmJs.Common.RemoveBrackets(CrmJs.Sales.BenefitCapture.FirstPrimaryItemId)+")";

            //Xrm.WebApi.online.createRecord(CrmJs.Sales.BenefitCapture.SelectedEntityTypeName, entity).then(
            //	function success(result) {
            //		CrmJs.Sales.BenefitCapture.SelectedControl.refresh();
            //		CrmJs.Common.SetControlVisibility(CrmJs.Sales.BenefitCapture.SelectedControl, true);
            //		CrmJs.Sales.BenefitCapture.XrmObject.Utility.closeProgressIndicator();
            //	},
            //	function(error) {
            //		CrmJs.Sales.BenefitCapture.FormContextObject.ui.setFormNotification(error.message,"ERROR","CREATE_BEN_ERROR");
            //		CrmJs.Sales.BenefitCapture.XrmObject.Utility.closeProgressIndicator();
            //	});
        }
	},
	
	CheckMaxAllowed: function(query, maxAllowed){
		Xrm.WebApi.online.retrieveMultipleRecords(CrmJs.Sales.BenefitCapture.SelectedEntityTypeName, query).then(
			function success(results) {
				if(CrmJs.Common.IsValidAttributeControl(results.entities)
					&& CrmJs.Common.IsValidAttributeControl(results.entities.length)){
						if(results.entities.length >= maxAllowed){
							CrmJs.Sales.BenefitCapture.FormContextObject.ui.setFormNotification("MAX_NO_EXCEEDED","ERROR","MAX_VALUE_ERROR");
							CrmJs.Sales.BenefitCapture.XrmObject.Utility.closeProgressIndicator();
						}else{
							CrmJs.Sales.BenefitCapture.CreateBeneficairy();
						}
					}else{
						CrmJs.Sales.BenefitCapture.CreateBeneficairy();
					}
			},
			function(error) {
				CrmJs.Sales.BenefitCapture.FormContextObject.ui.setFormNotification(error.message,"ERROR","CHECK_MAX_ERROR");
				CrmJs.Sales.BenefitCapture.XrmObject.Utility.closeProgressIndicator();
			}
		);
    },
}