﻿if (typeof (CrmJs) == "undefined") {
    CrmJs = { __namespace: true };
}

CrmJs.Common = {
    __namespace: true,
    FormContextObject: null,
    XrmObject: function () {
        if (typeof (Xrm) == "undefined")
            return parent.Xrm;
        else
            return Xrm;
    },
    ApiVersion: "/api/data/v9.1/",
    CrmUrl: null,
    GlobalContextObject: null,

    /**
     * Sets the Dynamics 365 form execution context object
     * @param {context} executionContext
     */
    SetFormContextObject: function (executionContext) {
        if (executionContext != null) {
            CrmJs.Common.FormContextObject = executionContext.getFormContext();
        }
    },

    SetGlobalContextObject: function (XrmObject) {
        CrmJs.Common.GlobalContextObject = XrmObject.Utility.getGlobalContext();
        CrmJs.Common.SetCrmUrl();
    },

    /**
    * Sets the Dynamics 365 CRM url
    * */
    SetCrmUrl: function () {
        if (CrmJs.Common.GlobalContextObject != null)
            CrmJs.Common.CrmUrl = CrmJs.Common.GlobalContextObject.getClientUrl();
        else
            CrmJs.Common.CrmUrl = null;
    },

    /**
    * Sets a value that indicates whether the tab and/or section is visible
    * @param {string} tabName Required. The name of the tab
    * @param {string} sectionName Optional. The name of the section
    * @param {boolean} visibility Required. Specify true to show the tab; false to hide the tab.
    */
    SetTabSectionVisibility: function (tabName, sectionName, visibility) {
        var tabCtrl = CrmJs.Common.FormContextObject.ui.tabs.get(tabName);

        if (CrmJs.Common.IsValidValue(tabCtrl)) {
            if (!CrmJs.Common.IsValidValue(sectionName))
                tabCtrl.setVisible(visibility);
            else {
                var sectionCtrl = tabCtrl.sections.get(sectionName);
                if (CrmJs.Common.IsValidValue(sectionCtrl)) {
                    try {
                        sectionCtrl.setVisible(visibility);
                        if (visibility)
                            tabCtrl.setVisible(visibility);
                    }
                    catch (e) {
                        //do nothing - just silently fail that we could not find the tab
                    }
                }
            }
        }
    },

    CheckForIdBlacklisting: function (id) {
        var isBlacklisted = false;
        var idNo = this.XrmObject().Page.getAttribute(id.getName());
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(idNo) &&
                CrmJs.Common.IsValidValue(idNo.getValue())) {
                var filter = "tu_idno eq '" + idNo.getValue() + "' and statecode eq 0";
                var blacklistings = CrmJs.Common.RetrieveMultiple("tu_blacklistings", filter, "tu_idno");
                if (blacklistings.value.length > 0) {
                    isBlacklisted = true;
                }
                else {
                    isBlacklisted = false;
                }
            }
        }
        return isBlacklisted;
    },

    CheckForAccountBlacklisting: function (account) {
        var isBlacklisted = false;
        var accountNumber = this.XrmObject().Page.getAttribute(account.getName());
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(accountNumber) &&
                CrmJs.Common.IsValidValue(accountNumber.getValue())) {
                var filter = "tu_accountnumber eq '" + accountNumber.getValue() + "' and statecode eq 0";
                var blacklistings = CrmJs.Common.RetrieveMultiple("tu_blacklistings", filter, "tu_accountnumber");
                if (blacklistings.value.length > 0) {
                    isBlacklisted = true;
                }
                else {
                    isBlacklisted = false;
                }
            }
        }
        return isBlacklisted;
    },

    SetRequired: function (executionContext, arg, requirementLevel) {
        executionContext.getFormContext().getAttribute(arg).setRequiredLevel(requirementLevel);
    },

    ValidateMobilePhone: function (fieldName) {
        var mobilePhone = this.XrmObject().Page.getAttribute(fieldName);
        if (this.IsValidAttributeControl(mobilePhone) && mobilePhone.getValue() != null) {
            var regex = /^(\+?27|0)[6-8][0-9]{8}$/;
            if (regex.test(mobilePhone.getValue())) {
                this.XrmObject().Page.getControl(fieldName).clearNotification();
            }
            else {
                this.XrmObject().Page.getControl(fieldName).setNotification("INVALID_MOBILE_PHONE");
            }

        }

    },

    IsNumber: function (str) {
        return !/\D/.test(str);
    },

    SetSectionVisibility: function (executionContext, tabName, sectionName, visibility) {
        var tabCtrl = executionContext.getFormContext().ui.tabs.get(tabName);

        if (CrmJs.Common.IsValidValue(tabCtrl)) {
            if (!CrmJs.Common.IsValidValue(sectionName))
                tabCtrl.setVisible(visibility);
            else {
                var sectionCtrl = tabCtrl.sections.get(sectionName);
                if (CrmJs.Common.IsValidValue(sectionCtrl)) {
                    try {
                        sectionCtrl.setVisible(visibility);
                        if (visibility)
                            tabCtrl.setVisible(visibility);
                    }
                    catch (e) {
                        //do nothing - just silently fail that we could not find the tab
                    }
                }
            }
        }
    },

    SetDisabled: function (bool, ctrlName) {
        CrmJs.Common.XrmObject().Page.getControl(ctrlName).setDisabled(bool);
    },

    /**
    * Checks whether the passed attribute has a value or not
    * @param {string} attributeName The attribute of the data value
    * @returns {boolean} Value indicating whether the passed attribute has a value or not
    */
    HasValue: function (attributeName) {
        return CrmJs.Common.FormContextObject.getAttribute(attributeName) != null && CrmJs.Common.FormContextObject.getAttribute(attributeName).getValue() != null ? true : false;
    },

    /**
     * Checks whether the passed value is valid or not
     * @param {object} value The data value
     * @returns {boolean} Value indicating whether the passed attribute has a value or not
     */
    IsValidValue: function (value) {
        if (value == null || value == "" || value == "undefined" || value == "null")
            return false;
        return true;
    },

    RemoveBrackets: function (guid) {
        if (CrmJs.Common.IsValidValue(guid))
            return guid.replace(/[{}]/g, '');
        return null;
    },

    GetFormType: function (executionContext) {
        var formContext = executionContext.getFormContext();
        var formType = formContext.ui.getFormType();
        return formType;
    },

    SetControlVisibility: function (control, visibility) {
        if (CrmJs.Common.IsValidAttributeControl(control)) {
            control.setVisible(visibility);
        }
    },

    ValidateIdNo: function (idNo) {
        var isValid = true;
        var isNumber = false;
        var idNumberLength = 0;
        var citizenShip = 0;
        var controlDigit = 0;
        var lastDigit = 0;
        if (this.IsValidAttributeControl(idNo)) {
            if (this.IsValidValue(idNo.getValue())) {
                idNumberLength = idNo.getValue().length;
                isNumber = this.IsNumber(idNo.getValue());
                if (idNumberLength == 13 && isNumber == true) {
                    this.XrmObject().Page.getControl("tu_idno").clearNotification();
                    citizenShip = parseInt(idNo.getValue().charAt(10));
                    controlDigit = this.GetControlDigit(idNo);
                    if (controlDigit.toString().length > 1) {
                        lastDigit = controlDigit.toString().substr(controlDigit.toString().length - 1, 1);
                    }
                    else {
                        lastDigit = controlDigit.toString();
                    }
                    if (citizenShip > 1 || lastDigit != idNo.getValue()[idNumberLength - 1]) {
                        this.XrmObject().Page.getControl("tu_idno").setNotification("INVALID_ID_NUMBER");
                        isValid = false;
                    }
                }
                else {
                    this.XrmObject().Page.getControl("tu_idno").setNotification("INVALID_ID_NUMBER");
                    isValid = false;
                }
            }
        }
        return isValid;
    },

    GetControlDigit: function (idNumber) {
        var d = -1;
        var a = 0;
        for (var i = 0; i < 6; i++) {
            a += parseInt(idNumber.getValue()[2 * i]);
        }

        var b = 0;
        for (var i = 0; i < 6; i++) {
            b = b * 10 + parseInt(idNumber.getValue()[2 * i + 1]);
        }
        b *= 2;

        var result = b.toString();
        var c = 0;
        for (var i = 0; i < result.length; i++) {
            c += parseInt(result[i]);
        }
        var lastSum = (a + c).toString();
        d = 10 - parseInt(lastSum[lastSum.length - 1]);

        return d;
    },

    IsValidAttributeControl: function (fieldName) {
        if (fieldName == null || fieldName == "" || fieldName == "undefined" || fieldName == "null")
            return false;
        return true;
    },

    PopulateDOB: function (idNumber, birthdate) {
        if (CrmJs.Common.IsValidAttributeControl(birthdate)) {
            var idNumYear = parseInt(idNumber.getValue().substr(0, 2));
            var idNumMonth = parseInt(idNumber.getValue().substr(2, 2));
            var idNumDay = parseInt(idNumber.getValue().substr(4, 2));
            birthdate.setValue(new Date(idNumYear, idNumMonth - 1, idNumDay));
        }
    },

    SetLookupValue: function (lookupControl, id, name, entityType) {
        if (lookupControl != null)
            lookupControl.setValue([{ id: id.replace(/[{}]/g, ''), name: name, entityType: entityType }]);
    },

    NavigationOptions: function () {
        var navigationOptions = {
            target: 2,
            height: { value: 100, unit: "%" },
            width: { value: 100, unit: "%" },
            position: 1
        };
        return navigationOptions;
    },

    GetEnvironmentVariable: function (schemaName) {
        var defaultValue = null;

        var req = new XMLHttpRequest();
        req.open("GET", CrmJs.Common.CrmUrl + CrmJs.Common.ApiVersion + "environmentvariabledefinitions?$select=defaultvalue&$filter=schemaname eq '" + schemaName + "'", false);
        req.setRequestHeader("OData-MaxVersion", "4.0");
        req.setRequestHeader("OData-Version", "4.0");
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.setRequestHeader("Prefer", "odata.include-annotations=\"*\"");
        req.onreadystatechange = function () {
            if (this.readyState === 4) {
                req.onreadystatechange = null;
                if (this.status === 200) {
                    var results = JSON.parse(this.response);
                    for (var i = 0; i < results.value.length; i++) {
                        defaultValue = results.value[i]["defaultvalue"];
                        return defaultValue;
                    }
                } else {
                    //Xrm.Utility.alertDialog(this.statusText);
                    return defaultValue;
                }
            }
        };
        req.send();

        return defaultValue;
    },

    ValidateAccNumber: function (executionContext, accNumber) {
        var isValid = true;
        this.SetFormContextObject(executionContext);
        if (!this.IsValidAttributeControl(accNumber)) return;
        if (this.IsValidValue(accNumber.getValue())) {
            if (isNaN(accNumber.getValue().trim()) || accNumber.getValue().trim().length == 0
                || accNumber.getValue().trim().length < 6 || accNumber.getValue().trim().length > 11) {
                isValid = false;
            }
        } else {
            isValid = false;
        }
        return isValid;
    },

    RetrieveMultiple: function (entityName, filter, columns) {
        var globalContext = Xrm.Utility.getGlobalContext();
        var colllection = "undefined";
        var req = new XMLHttpRequest();
        req.open("GET", globalContext.getClientUrl() + CrmJs.Common.ApiVersion + entityName
            + "?$select=" + columns + "&$filter=" + filter, false);
        req.setRequestHeader("OData-MaxVersion", "4.0");
        req.setRequestHeader("OData-Version", "4.0");
        req.setRequestHeader("Accept", "application/json");
        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.setRequestHeader("Prefer", "odata.include-annotations=\"*\"");
        req.onreadystatechange = function () {
            if (this.readyState === 4) {
                req.onreadystatechange = null;
                if (this.status === 200) {
                    var results = JSON.parse(this.response);
                    colllection = results;
                }
            }
        };
        req.send();
        return colllection;
    },

    IsLettersOnly: function (attribute) {
        var letters = /^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/;

        if (CrmJs.Common.IsValidValue(attribute.getValue())) {
            if (attribute.getValue().match(letters))
                return true;
            else
                return false;
        }
    },
    CheckLetters: function (executionContext, attributeName) {
        if (CrmJs.Common != "undefined") {
            CrmJs.Common.SetFormContextObject(executionContext);
            if (!CrmJs.Common.IsLettersOnly(CrmJs.Common.FormContextObject.getAttribute(attributeName))) {
                CrmJs.Common.FormContextObject.getControl(attributeName).setNotification("LETTERS_ONLY");
            } else {
                CrmJs.Common.FormContextObject.getControl(attributeName).clearNotification();
            }
        }
    },
}