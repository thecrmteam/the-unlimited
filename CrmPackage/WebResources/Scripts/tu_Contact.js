﻿if (typeof (CrmJs) == "undefined") {
    CrmJs = { __namespace: true };
}

if (typeof (CrmJs.Sales) == "undefined") {
    CrmJs.Sales = { __namespace: true };
}

CrmJs.Sales.Contact = {
    __namespace: true,
    FormContextObject: null,
    XrmObject: function () {
        if (typeof (Xrm) == "undefined")
            return parent.Xrm;
        else
            return Xrm;
    },
    apiVersion: "/api/data/v9.1",

    ValidateIDNumber: function () {
        var idNumber = this.XrmObject().Page.getAttribute("tu_idnopassport");
        var isForeigner = this.XrmObject().Page.getAttribute("tu_isforeigner");
        var customerAge = this.XrmObject().Page.getAttribute("tu_age");
        var idNumberLength = 0;
        var isNumber = false;
        var citizenShip = 0;
        var lastDigit = 0;
        var controlDigit = 0;
        if (isForeigner != null) {
            if (CrmJs.Common.IsValidAttributeControl(idNumber.getValue())) {
                if (isForeigner.getValue() == false) {
                    idNumberLength = idNumber.getValue().length;
                    isNumber = this.IsNumber(idNumber.getValue());
                    if (idNumberLength == 13 && isNumber == true) {
                        this.XrmObject().Page.getControl("tu_idnopassport").clearNotification();
                        citizenShip = parseInt(idNumber.getValue().charAt(10));
                        controlDigit = this.GetControlDigit(idNumber);
                        if (controlDigit.toString().length > 1) {
                            lastDigit = controlDigit.toString().substr(controlDigit.toString().length - 1, 1);
                        }
                        else {
                            lastDigit = controlDigit.toString();
                        }
                        if (citizenShip > 1 || lastDigit != idNumber.getValue()[idNumberLength - 1]) {
                            this.XrmObject().Page.getControl("tu_idnopassport").setNotification("INVALID_ID_NUMBER");
                        }
                        else {
                            this.XrmObject().Page.getControl("tu_idnopassport").clearNotification();
                            this.CheckCustomerExists(idNumber);
                            this.PopulateDOB(idNumber);
                            this.PopulateGender(idNumber);
                            this.PopulateAge();
                        }
                    }
                    else {
                        this.XrmObject().Page.getControl("tu_idnopassport").setNotification("INVALID_ID_NUMBER");
                    }
                }
                else {
                    this.XrmObject().Page.getControl("tu_idnopassport").clearNotification();
                }
            }
        }
    },

    OnLoad: function (executionContext) {
        CrmJs.Sales.Contact.DialerParameters(executionContext);
        var formContext = executionContext.getFormContext()
        var isForeigner = this.XrmObject().Page.getAttribute("tu_isforeigner");
        var countryOfBirth = this.XrmObject().Page.getAttribute("tu_countryofbirth");
        var type = this.XrmObject().Page.getAttribute("tu_type");
        var idNo = this.XrmObject().Page.getAttribute("tu_idnopassport");
        var mobilePhone = this.XrmObject().Page.getAttribute("mobilephone");
        var street1 = this.XrmObject().Page.getAttribute("address1_line1");
        var city = this.XrmObject().Page.getAttribute("address1_city");
        var postalCode = this.XrmObject().Page.getAttribute("address1_postalcode");
        var age = this.XrmObject().Page.getAttribute("tu_age");
        var dateOfBirth = this.XrmObject().Page.getAttribute("birthdate");
        var gender = this.XrmObject().Page.getAttribute("gendercode");
        var validateButton = this.XrmObject().Page.ui.controls.get("WebResource_validatebanking");
        if (CrmJs.Common != "undefined") {
            var formType = CrmJs.Common.GetFormType(executionContext);
            if (CrmJs.Common.IsValidAttributeControl(type)) {
                if (type.getValue() == null) {
                    type.setValue(827210004);
                }
                if (formType == 1) {
                    CrmJs.Sales.Contact.SetCountry();
                }
                else if (type.getValue() == 827210001 || type.getValue() == 827210002 ||
                    type.getValue() == 827210003) {
                    CrmJs.Common.SetTabSectionVisibility("Personal_tab", "Personal_tab_section_beneficiary", true);
                    CrmJs.Common.SetTabSectionVisibility("Personal_tab", "PERSONAL_INFORMATION", false);
                    CrmJs.Common.SetTabSectionVisibility("Employment_tab", null, false);
                    CrmJs.Common.SetTabSectionVisibility("Address_tab", null, false);
                    CrmJs.Common.SetTabSectionVisibility("Banking_tab", null, false);
                    CrmJs.Common.SetTabSectionVisibility("Benefits_tab", null, false);
                    CrmJs.Common.SetTabSectionVisibility("Mandates_tab", null, false);
                    CrmJs.Common.SetTabSectionVisibility("Collections_tab", null, false);
                    CrmJs.Common.SetTabSectionVisibility("tab_8", null, false);
                    if (CrmJs.Common.IsValidAttributeControl(city)) {
                        formContext.getAttribute(city.getName()).setRequiredLevel("none");
                    }
                    if (CrmJs.Common.IsValidAttributeControl(postalCode)) {
                        formContext.getAttribute(postalCode.getName()).setRequiredLevel("none");
                    }
                    if (CrmJs.Common.IsValidAttributeControl(dateOfBirth)) {
                        CrmJs.Common.SetDisabled(false, dateOfBirth.getName());
                    }
                    if (CrmJs.Common.IsValidAttributeControl(gender)) {
                        CrmJs.Common.SetDisabled(false, gender.getName());
                    }
                    if (CrmJs.Common.IsValidAttributeControl(age)) {
                        this.XrmObject().Page.getControl(age.getName()).setVisible(false);
                    }

                }
                this.HideSections(type, executionContext);
            }
            if (CrmJs.Common.IsValidAttributeControl(isForeigner) &&
                CrmJs.Common.IsValidAttributeControl(countryOfBirth)) {
                if (isForeigner.getValue() == true) {
                    this.XrmObject().Page.getControl(countryOfBirth.getName()).setVisible(true);
                    countryOfBirth.setRequiredLevel("required");
                }
                else {
                    this.XrmObject().Page.getControl(countryOfBirth.getName()).setVisible(false);
                    countryOfBirth.setRequiredLevel("none");
                }
            }

        }
    },

    OnChangeMobilePhone: function () {
        var mobilePhone = this.XrmObject().Page.getAttribute("mobilephone");
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(mobilePhone)) {
                CrmJs.Common.ValidateMobilePhone(mobilePhone.getName());
            }
        }
    },

    CheckCustomerExists: function (idNumber) {
        var type = this.XrmObject().Page.getAttribute("tu_type");
        var columns = "tu_idnopassport";
        var filter = "tu_idnopassport eq '" + idNumber.getValue() + "' and statecode eq 0";
        var pageInput = {
            pageType: "entityrecord",
            entityName: "contact",
            formId: "89c60568-738d-4cf4-9def-983d48956ee3",
        };
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(type) && type.getValue() == 827210000) {
                var customer = CrmJs.Common.RetrieveMultiple("contacts", filter, columns);
                if (customer.value.length > 0) {
                    pageInput.entityId = customer.value[0].contactid;
                    this.XrmObject().Navigation.navigateTo(pageInput, CrmJs.Common.NavigationOptions()).then(
                        function success() {
                        },
                        function erro() {

                        });
                }
            }
        }
    },

    OnSave: function (executionContext) {
        this.ValidateIDNumber();
        var idNo = this.XrmObject().Page.getAttribute("tu_idnopassport");
        var saveEvent = executionContext.getEventArgs();
        var formType = CrmJs.Common.GetFormType(executionContext);
        var entityType = this.XrmObject().Page.data.entity.getEntityName();
        var preventSave = false;
        var type = this.XrmObject().Page.getAttribute("tu_type");
        var age = this.XrmObject().Page.getAttribute("tu_age");
        if (formType == 1) {
            if (CrmJs.Common != "undefined") {
                if (CrmJs.Common.IsValidAttributeControl(idNo) &&
                    CrmJs.Common.IsValidValue(idNo)) {
                    var idNumberBlacklisted  = CrmJs.Common.CheckForIdBlacklisting(idNo);
                    if (idNumberBlacklisted) {
                        this.XrmObject().Page.ui.setFormNotification("ERROR.UNFAVORABLE INFORMATION EXISTS",
                            "ERROR", "IDNUMBER_BLACKLISTING");
                        saveEvent.preventDefault();
                    }
                    else {
                        this.XrmObject().Page.ui.clearFormNotification("IDNUMBER_BLACKLISTING");
                    }
                }
                if (CrmJs.Common.IsValidAttributeControl(type)) {
                    if (type.getValue() == 827210004) {
                        if (CrmJs.Common.IsValidAttributeControl(age)) {
                            if (CrmJs.Common.IsValidValue(age.getValue())) {
                                var data = {
                                    "Age": age.getValue()
                                };
                                var req = new XMLHttpRequest();
                                req.open("POST", this.XrmObject().Page.context.getClientUrl() +
                                    this.apiVersion + "/tu_GetQualifyingBenefits", false);
                                req.setRequestHeader("OData-MaxVersion", "4.0");
                                req.setRequestHeader("OData-Version", "4.0");
                                req.setRequestHeader("Accept", "application/json");
                                req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                                req.onreadystatechange = function () {
                                    if (this.readyState === 4) {
                                        req.onreadystatechange = null;
                                        if (this.status === 200) {
                                            var results = JSON.parse(this.response);
                                            if (results.BenefitsToAdd.length > 0) {
                                                CrmJs.Sales.Contact.XrmObject().Page.ui.clearFormNotification("AGE_ERROR");
                                            }
                                            else {
                                                preventSave = true;   
                                                CrmJs.Sales.Contact.XrmObject().Page.ui.setFormNotification(results.Message,
                                                    "ERROR", "AGE_ERROR");
                                            }
                                        }
                                        else {
                                            var error = JSON.parse(this.response).error; 
                                        }
                                    }
                                };
                                req.send(JSON.stringify(data));
                                if (preventSave) {
                                    saveEvent.preventDefault();
                                }
                            }
                        }
                    }
                }
            }
            
        }
        else if (CrmJs.Common.IsValidAttributeControl(type)) {
            if (type.getValue() == 827210000) {
                var payerId = this.XrmObject().Page.data.entity.getId();
                var columns = "tu_bankname, _tu_customer_value,tu_accountnumber";
                var filter = "_tu_customer_value eq '" + payerId + "' and statecode eq 0";
                var bankingDetails = CrmJs.Common.RetrieveMultiple("tu_bankaccountdetailses", filter, columns);
                if (bankingDetails != "undefined") {
                    if (bankingDetails.value.length == 0) {
                        var bankingData = {
                            "tu_accounttype": this.XrmObject().Page.getAttribute("tu_accounttype").getValue(),
                            "tu_accountnumber": this.XrmObject().Page.getAttribute("tu_accountnumber").getValue(),
                            "tu_bankname": this.XrmObject().Page.getAttribute("tu_bankname").getValue(),
                            "tu_branchcode": this.XrmObject().Page.getAttribute("tu_branchcode").getValue(),
                            "tu_customer@odata.bind": "/contacts(" + this.RemoveBrackets(payerId) + ")"
                        };

                        this.XrmObject().WebApi.createRecord("tu_bankaccountdetails", bankingData).then(
                            function success(result) {

                            },
                            function (error) {

                            }
                        );

                    }
                    else {

                    }
                }
            }
        }
    },

    HideSections: function (type, executionContect) {
        var dateOfBirth = this.XrmObject().Page.getAttribute("birthdate");
        var gender = this.XrmObject().Page.getAttribute("gendercode");
        var idNumber = this.XrmObject().Page.getAttribute("tu_idnopassport");
        var tabName = "Personal_tab"; var sectionName = "Payer";
        if (type.getValue() == 827210001 || type.getValue() == 827210002 || type.getValue() == 827210003) {
            if (CrmJs.Common.IsValidAttributeControl(dateOfBirth)) {
                //dateOfBirth.setRequiredLevel("none");
                CrmJs.Common.SetDisabled(false, dateOfBirth.getName());
                if (CrmJs.Common.IsValidAttributeControl(idNumber))
                    idNumber.setRequiredLevel("none");
                if (CrmJs.Common.IsValidAttributeControl(gender))
                    CrmJs.Common.SetDisabled(false, gender.getName());
                CrmJs.Common.SetFormContextObject(executionContect);
                //CrmJs.Common.SetTabSectionVisibility(tabName, sectionName, false);
            }
        }
    },

    SetCountry: function () {
        var resCountry = this.XrmObject().Page.getAttribute("tu_countryresidential");
        var delCountry = this.XrmObject().Page.getAttribute("tu_countrydelivery");
        var posCountry = this.XrmObject().Page.getAttribute("tu_countrypostal");
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(resCountry) && resCountry.getValue() == null) {
                resCountry.setValue(0);
            }
            if (CrmJs.Common.IsValidAttributeControl(delCountry) && delCountry.getValue() == null) {
                delCountry.setValue(0);
            }
            if (CrmJs.Common.IsValidAttributeControl(posCountry) && posCountry.getValue() == null) {
                posCountry.setValue(0);
            }
        }
    },

    OnChangeIsForeigner: function () {
        var isForeigner = this.XrmObject().Page.getAttribute("tu_isforeigner");
        var countryOfBirth = this.XrmObject().Page.getAttribute("tu_countryofbirth");
        var idNumber = this.XrmObject().Page.getAttribute("tu_idnopassport");
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(isForeigner) && isForeigner.getValue() == false) {
                if (CrmJs.Common.IsValidAttributeControl(idNumber.getValue())) {
                    this.ValidateIDNumber();
                }
                if (CrmJs.Common.IsValidAttributeControl(countryOfBirth)) {
                    this.XrmObject().Page.getControl(countryOfBirth.getName()).setVisible(false);
                    countryOfBirth.setRequiredLevel("none");
                }
            }
            else if (CrmJs.Common.IsValidAttributeControl(isForeigner) && isForeigner.getValue() == true) {
                if (CrmJs.Common.IsValidAttributeControl(idNumber)) {
                    this.XrmObject().Page.getControl("tu_idnopassport").clearNotification();
                }
                if (CrmJs.Common.IsValidAttributeControl(countryOfBirth)) {
                    this.XrmObject().Page.getControl(countryOfBirth.getName()).setVisible(true);
                    countryOfBirth.setRequiredLevel("required");
                }
            }
        }
    },

    PopulateDOB: function (idNumber) {
        var birthDate = this.XrmObject().Page.getAttribute("birthdate");
        if (CrmJs.Common.IsValidAttributeControl(birthDate)) {

            var idNumYear = parseInt(idNumber.getValue().substr(0, 2));
            var idNumMonth = parseInt(idNumber.getValue().substr(2, 2));
            var idNumDay = parseInt(idNumber.getValue().substr(4, 2));
            birthDate.setValue(new Date(idNumYear, idNumMonth - 1, idNumDay));
        }
    },

    PopulateAge: function () {
        var today = new Date();
        var age = 0;
        var birthdate = this.XrmObject().Page.getAttribute("birthdate");
        var customerAge = this.XrmObject().Page.getAttribute("tu_age");
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(birthdate) && CrmJs.Common.IsValidAttributeControl(customerAge)) {
                age = today.getFullYear() - birthdate.getValue().getFullYear();
                var m = today.getMonth() - birthdate.getValue().getMonth();
                if (m < 0 || (m === 0 && today.getDate() < birthdate.getValue().getDate())) {
                    age--;
                }
                customerAge.setValue(age);
            }

        }

    },

    PopulateGender: function (idNumber) {
        var gender = this.XrmObject().Page.getAttribute("gendercode");
        if (CrmJs.Common != "undefined" && CrmJs.Common.IsValidAttributeControl(gender)) {
            var genderNumber = parseInt(idNumber.getValue().substr(6, 1));
            if (genderNumber <= 4) {
                gender.setValue(2);
            }
            else if (genderNumber > 4 && genderNumber <= 9) {
                gender.setValue(1);
            }
        }
    },

    IsNumber: function (str) {
        return !/\D/.test(str);
    },

    GetControlDigit: function (idNumber) {
        var d = -1;
        var a = 0;
        for (var i = 0; i < 6; i++) {
            a += parseInt(idNumber.getValue()[2 * i]);
        }

        var b = 0;
        for (var i = 0; i < 6; i++) {
            b = b * 10 + parseInt(idNumber.getValue()[2 * i + 1]);
        }
        b *= 2;

        var result = b.toString();
        var c = 0;
        for (var i = 0; i < result.length; i++) {
            c += parseInt(result[i]);
        }
        var lastSum = (a + c).toString();
        d = 10 - parseInt(lastSum[lastSum.length - 1]);

        return d;
    },

    PopulateDebitDate: function (executionContext) {
        if (CrmJs.Common != "undefined") {
            CrmJs.Common.SetFormContextObject(executionContext);
            var salaryPayDay = CrmJs.Common.FormContextObject.getAttribute("tu_collectionday");

            if (CrmJs.Common.IsValidAttributeControl(salaryPayDay)
                && CrmJs.Common.IsValidAttributeControl(salaryPayDay.getValue())) {
                var dateToday = new Date();
                var debitDate = CrmJs.Common.FormContextObject.getAttribute("tu_debitdate");

                if (CrmJs.Common.IsValidAttributeControl(debitDate)
                    && dateToday.getDate() > salaryPayDay.getValue()
                    && !CrmJs.Common.IsValidAttributeControl(debitDate.getValue())) {
                    debitDate.setValue(new Date(dateToday.getFullYear(), (dateToday.getMonth() + 1), salaryPayDay.getValue()));
                } else if (CrmJs.Common.IsValidAttributeControl(debitDate)
                    && !CrmJs.Common.IsValidAttributeControl(debitDate.getValue())) {
                    debitDate.setValue(new Date(dateToday.getFullYear(), dateToday.getMonth(), salaryPayDay.getValue()));
                }
            }
        }
    },

    PopulateBranchCode: function (executionContext) {
        if (CrmJs.Common != "undefined") {
            CrmJs.Common.SetFormContextObject(executionContext);
            var bank = CrmJs.Common.FormContextObject.getAttribute("tu_bankname");

            if (!CrmJs.Common.IsValidAttributeControl(bank)) return;

            if (CrmJs.Common.IsValidAttributeControl(bank)
                && CrmJs.Common.IsValidAttributeControl(bank.getValue())) {
                var branchCode = CrmJs.Common.FormContextObject.getAttribute("tu_branchcode");

                branchCode.setValue(bank.getValue().toString());
            }
        }
    },

    DialerParameters: function (executionContext) {
        if (CrmJs.Common != "undefined") {
            CrmJs.Common.SetFormContextObject(executionContext);
            CrmJs.Common.FormContextObject.ui.clearFormNotification("VOICE_LOG_ERROR");

            var description = CrmJs.Common.FormContextObject.getAttribute("description");
            if (!CrmJs.Common.IsValidAttributeControl(description)) return;

            if (CrmJs.Common.IsValidAttributeControl(description)
                && CrmJs.Common.IsValidAttributeControl(description.getValue())) {
                var dialerPar = CrmJs.Common.RemoveBrackets(description.getValue());

                if (dialerPar.length > 0) {
                    var dialerObj = dialerPar.split(',');
                    var leadId = null;
                    var merchCode = null;
                    var source = null;
                    var fileName = null;

                    for (var i = 0; i < dialerObj.length; i++) {
                        var parameter = dialerObj[i].split('=');

                        if (parameter.length > 1) {
                            switch (parameter[0]) {
                                case "LeadId":
                                    CrmJs.Common.FormContextObject.getAttribute("tu_leadid").setValue(parameter[1]);
                                    break;
                                case "MerchCode":
                                    CrmJs.Common.FormContextObject.getAttribute("tu_merchcode").setValue(parameter[1]);
                                    break;
                                case "Source":
                                    source = parameter[1];
                                    break;
                                case "Filename":
                                    CrmJs.Common.FormContextObject.getAttribute("tu_filename").setValue(parameter[1]);
                                    break;
                                case "telephoneNumber":
                                    CrmJs.Common.FormContextObject.getAttribute("mobilephone").setValue(parameter[1]);
                                    break;
                            }
                        }
                    }

                    //var entityId = CrmJs.Common.FormContextObject.data.entity.getId();					
                    //if(CrmJs.Common.IsValidValue(entityId)){
                    //		CrmJs.Sales.Contact.CreateVoiceLogNote(leadId,merchCode,source,fileName, CrmJs.Common.RemoveBrackets(entityId))
                    //	}
                }
            }
        }
    },

    CreateVoiceLogNote: function (LeadId, MerchCode, Source, Filename, EntityId) {
        var entity = {};
        entity.subject = "Sale Voice Log: " + LeadId;
        entity.notetext = Filename;
        entity.isdocument = false;
        entity["objectid_contact@odata.bind"] = "/contacts(" + EntityId + ")";
        entity.objecttypecode = 'contact';

        Xrm.WebApi.online.createRecord("annotation", entity).then(
            function success(result) {
                CrmJs.Common.FormContextObject.getAttribute("description").setValue(null);
            },
            function (error) {
                CrmJs.Common.FormContextObject.ui.setFormNotification(error.message, "ERROR", "VOICE_LOG_ERROR");
            }
        );
    },

    PopCaseForm: function (SelectedEntityTypeName, PrimaryEntityTypeName, FirstPrimaryId, PrimaryControl, SelectedControl) {
        var parameters = {};
        parameters["customerid"] = FirstPrimaryId;
        parameters["customeridname"] = CrmJs.Common.FormContextObject.getAttribute("fullname").getValue();
        parameters["customeridtype"] = PrimaryEntityTypeName;
        var idNo = CrmJs.Sales.Contact.XrmObject().Page.getAttribute("tu_idnopassport");

        var pageInput = {
            pageType: "entityrecord",
            entityName: "incident",
            data: parameters,
        };
        var navigationOptions = {
            target: 2,
            height: { value: 100, unit: "%" },
            width: { value: 100, unit: "%" },
            position: 1
        };

        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(idNo)) {
                var idNumberBlacklisted = CrmJs.Common.CheckForIdBlacklisting(idNo);
                if (idNumberBlacklisted) {
                    CrmJs.Sales.Contact.XrmObject().Page.ui.setFormNotification("ERROR.UNFAVORABLE INFORMATION EXISTS", 
                        "ERROR","IDNUMBER_BLACKLISTING");
                }
                else {
                    CrmJs.Sales.Contact.XrmObject().Page.ui.clearFormNotification("IDNUMBER_BLACKLISTING");
                    Xrm.Navigation.navigateTo(pageInput, navigationOptions).then(
                        function success(result) {
                            SelectedControl.refresh();
                        },
                        function error() {
                            // Handle errors
                        }
                    );
                }
            }           
        }
    },

    CheckForIdBlacklisting: function (SelectedControl, SelectedControlSelectedItemReferences,
    SelectedEntityTypeName) {
        var idNo = CrmJs.Sales.Contact.XrmObject().Page.getAttribute("tu_idnopassport");
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(idNo)) {
                var idNumberBlacklisted = CrmJs.Common.CheckForIdBlacklisting(idNo);
                if (idNumberBlacklisted) {
                    CrmJs.Sales.Contact.XrmObject().Page.ui.setFormNotification("ERROR.UNFAVORABLE INFORMATION EXISTS",
                        "ERROR", "IDNUMBER_BLACKLISTING");
                }
                else {
                    CrmJs.Sales.Contact.XrmObject().Page.ui.clearFormNotification("IDNUMBER_BLACKLISTING");
                    XrmCore.Commands.Activate.activateRecords(SelectedControl, SelectedControlSelectedItemReferences,
                        SelectedEntityTypeName);
                }
            }
        }
    },


    CopyResidentialAddress: function () {
        var resStreet1 = this.XrmObject().Page.getAttribute("address1_line1");
        var resStreet2 = this.XrmObject().Page.getAttribute("address1_line2");
        var resStreet3 = this.XrmObject().Page.getAttribute("address1_line3");
        var resCity = this.XrmObject().Page.getAttribute("address1_city");
        var resSuburb = this.XrmObject().Page.getAttribute("address1_stateorprovince");
        var resCountry = this.XrmObject().Page.getAttribute("tu_countryresidential");
        var resPostalCode = this.XrmObject().Page.getAttribute("address1_postalcode");
        var copyResAddress = this.XrmObject().Page.getAttribute("tu_copyresidentialaddress");

        var delStreet1 = this.XrmObject().Page.getAttribute("address2_line1");
        var delStreet2 = this.XrmObject().Page.getAttribute("address2_line2");
        var delStreet3 = this.XrmObject().Page.getAttribute("address2_line3");
        var delCity = this.XrmObject().Page.getAttribute("address2_city");
        var delSuburb = this.XrmObject().Page.getAttribute("address2_stateorprovince");
        var delCountry = this.XrmObject().Page.getAttribute("tu_countrydelivery");
        var delPostalCode = this.XrmObject().Page.getAttribute("address2_postalcode");

        var posStreet1 = this.XrmObject().Page.getAttribute("address3_line1");
        var posStreet2 = this.XrmObject().Page.getAttribute("address3_line2");
        var posStreet3 = this.XrmObject().Page.getAttribute("address3_line3");
        var posCity = this.XrmObject().Page.getAttribute("address3_city");
        var posSuburb = this.XrmObject().Page.getAttribute("address3_stateorprovince");
        var posCountry = this.XrmObject().Page.getAttribute("tu_countrypostal");
        var posPostalCode = this.XrmObject().Page.getAttribute("address3_postalcode");

        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(copyResAddress)) {
                switch (copyResAddress.getValue()) {
                    case 827210000:
                        if (CrmJs.Common.IsValidAttributeControl(resStreet1) && CrmJs.Common.IsValidAttributeControl(delStreet1) && CrmJs.Common.IsValidAttributeControl(posStreet1)) {
                            delStreet1.setValue(resStreet1.getValue()); posStreet1.setValue(resStreet1.getValue());
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resStreet2) && CrmJs.Common.IsValidAttributeControl(delStreet2) && CrmJs.Common.IsValidAttributeControl(posStreet2)) {
                            delStreet2.setValue(resStreet2.getValue()); posStreet2.setValue(resStreet2.getValue());
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resStreet3) && CrmJs.Common.IsValidAttributeControl(delStreet3) && CrmJs.Common.IsValidAttributeControl(posStreet3)) {
                            delStreet3.setValue(resStreet3.getValue()); posStreet3.setValue(resStreet3.getValue());
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resCity) && CrmJs.Common.IsValidAttributeControl(delCity) && CrmJs.Common.IsValidAttributeControl(posCity)) {
                            delCity.setValue(resCity.getValue()); posCity.setValue(resCity.getValue());
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resSuburb) && CrmJs.Common.IsValidAttributeControl(delSuburb) && CrmJs.Common.IsValidAttributeControl(posSuburb)) {
                            delSuburb.setValue(resSuburb.getValue()); posSuburb.setValue(resSuburb.getValue());
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resCountry) && CrmJs.Common.IsValidAttributeControl(delCountry) && CrmJs.Common.IsValidAttributeControl(posCountry)) {
                            delCountry.setValue(resCountry.getValue()); posCountry.setValue(resCountry.getValue());
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resPostalCode) && CrmJs.Common.IsValidAttributeControl(delPostalCode) && CrmJs.Common.IsValidAttributeControl(posPostalCode)) {
                            delPostalCode.setValue(resPostalCode.getValue()); posPostalCode.setValue(resPostalCode.getValue());
                        }

                        break;

                    //Delivery
                    case 827210001:
                        if (CrmJs.Common.IsValidAttributeControl(resStreet1) && CrmJs.Common.IsValidAttributeControl(delStreet1) && CrmJs.Common.IsValidAttributeControl(posStreet1)) {
                            delStreet1.setValue(resStreet1.getValue()); posStreet1.setValue("");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resStreet2) && CrmJs.Common.IsValidAttributeControl(delStreet2) && CrmJs.Common.IsValidAttributeControl(posStreet2)) {
                            delStreet2.setValue(resStreet2.getValue()); posStreet2.setValue("");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resStreet3) && CrmJs.Common.IsValidAttributeControl(delStreet3) && CrmJs.Common.IsValidAttributeControl(posStreet3)) {
                            delStreet3.setValue(resStreet3.getValue()); posStreet3.setValue("");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resCity) && CrmJs.Common.IsValidAttributeControl(delCity) && CrmJs.Common.IsValidAttributeControl(posCity)) {
                            delCity.setValue(resCity.getValue()); posCity.setValue("");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resSuburb) && CrmJs.Common.IsValidAttributeControl(delSuburb) && CrmJs.Common.IsValidAttributeControl(posSuburb)) {
                            delSuburb.setValue(resSuburb.getValue()); posSuburb.setValue("");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resCountry) && CrmJs.Common.IsValidAttributeControl(delCountry) && CrmJs.Common.IsValidAttributeControl(posCountry)) {
                            delCountry.setValue(resCountry.getValue()); posCountry.setValue("");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resPostalCode) && CrmJs.Common.IsValidAttributeControl(delPostalCode) && CrmJs.Common.IsValidAttributeControl(posPostalCode)) {
                            delPostalCode.setValue(resPostalCode.getValue()); posPostalCode.setValue("");
                        }
                        break;

                    case 827210002:
                        if (CrmJs.Common.IsValidAttributeControl(resStreet1) && CrmJs.Common.IsValidAttributeControl(delStreet1) && CrmJs.Common.IsValidAttributeControl(posStreet1)) {
                            delStreet1.setValue(""); posStreet1.setValue(resStreet1.getValue());
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resStreet2) && CrmJs.Common.IsValidAttributeControl(delStreet2) && CrmJs.Common.IsValidAttributeControl(posStreet2)) {
                            delStreet2.setValue(""); posStreet2.setValue(resStreet2.getValue());
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resStreet3) && CrmJs.Common.IsValidAttributeControl(delStreet3) && CrmJs.Common.IsValidAttributeControl(posStreet3)) {
                            delStreet3.setValue(""); posStreet3.setValue(resStreet3.getValue());
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resCity) && CrmJs.Common.IsValidAttributeControl(delCity) && CrmJs.Common.IsValidAttributeControl(posCity)) {
                            delCity.setValue(""); posCity.setValue(resCity.getValue());
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resSuburb) && CrmJs.Common.IsValidAttributeControl(delSuburb) && CrmJs.Common.IsValidAttributeControl(posSuburb)) {
                            delSuburb.setValue(""); posSuburb.setValue(resSuburb.getValue());
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resCountry) && CrmJs.Common.IsValidAttributeControl(delCountry) && CrmJs.Common.IsValidAttributeControl(posCountry)) {
                            delCountry.setValue(""); posCountry.setValue(resCountry.getValue())
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resPostalCode) && CrmJs.Common.IsValidAttributeControl(delPostalCode) && CrmJs.Common.IsValidAttributeControl(posPostalCode)) {
                            delPostalCode.setValue(""); posPostalCode.setValue(resPostalCode.getValue());
                        }
                        break;
                    default:
                        if (CrmJs.Common.IsValidAttributeControl(resStreet1) && CrmJs.Common.IsValidAttributeControl(delStreet1) && CrmJs.Common.IsValidAttributeControl(posStreet1)) {
                            delStreet1.setValue(""); posStreet1.setValue("");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resStreet2) && CrmJs.Common.IsValidAttributeControl(delStreet2) && CrmJs.Common.IsValidAttributeControl(posStreet2)) {
                            delStreet2.setValue(""); posStreet2.setValue("");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resStreet3) && CrmJs.Common.IsValidAttributeControl(delStreet3) && CrmJs.Common.IsValidAttributeControl(posStreet3)) {
                            delStreet3.setValue(""); posStreet3.setValue("");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resCity) && CrmJs.Common.IsValidAttributeControl(delCity) && CrmJs.Common.IsValidAttributeControl(posCity)) {
                            delCity.setValue(""); posCity.setValue("");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resSuburb) && CrmJs.Common.IsValidAttributeControl(delSuburb) && CrmJs.Common.IsValidAttributeControl(posSuburb)) {
                            delSuburb.setValue(""); posSuburb.setValue("");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resCountry) && CrmJs.Common.IsValidAttributeControl(delCountry) && CrmJs.Common.IsValidAttributeControl(posCountry)) {
                            delCountry.setValue(""); posCountry.setValue("");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(resPostalCode) && CrmJs.Common.IsValidAttributeControl(delPostalCode) && CrmJs.Common.IsValidAttributeControl(posPostalCode)) {
                            delPostalCode.setValue(""); posPostalCode.setValue("");
                        }

                }
            }
        }

    },
    RemoveBrackets: function (guid) {
        return guid.replace(/[{}]/g, '');
    },

    CheckLetters: function (executionContext, attributeName) {
        if (CrmJs.Common != "undefined") {
            CrmJs.Common.SetFormContextObject(executionContext);
            if (!CrmJs.Common.IsLettersOnly(CrmJs.Common.FormContextObject.getAttribute(attributeName))) {
                CrmJs.Common.FormContextObject.getControl(attributeName).setNotification("LETTERS_ONLY");
            } else {
                CrmJs.Common.FormContextObject.getControl(attributeName).clearNotification();
            }
        }
    },
};