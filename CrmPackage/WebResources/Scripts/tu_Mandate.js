﻿if (typeof (CrmJs) == "undefined") {
    CrmJs = { __namespace: true };
}

if (typeof (CrmJs.Sales) == "undefined") {
    CrmJs.Sales = { __namespace: true };
}
CrmJs.Sales.Mandate = {
    __namespace: true,
    FormContextObject: null,
    XrmObject: function () {
        if (typeof (Xrm) == "undefined")
            return parent.Xrm;
        else
            return Xrm;
    },
    OnLoad: function (executionContext) {


        var alertStrings = {
            confirmButtonLabel: "OK", text: "Balances for:\n Total Purchased Airtime\n Total Free Airtime\n Total Data\n"+
            "Total SMS\n Total WhatsApp Data"
            , title: "Balance Enquiry"
        };
        var alertOptions = { height: 200, width: 260 };
        this.XrmObject().Navigation.openAlertDialog(alertStrings, alertOptions).then(
            function success(result) {
                console.log("Alert dialog closed");
            },
            function (error) {
                console.log(error.message);
            }
        );



        var customer = this.XrmObject().Page.getAttribute("tu_customer");
        var payer = this.XrmObject().Page.getAttribute("tu_payer");
        var bank = this.XrmObject().Page.getAttribute("tu_bankdetails");
        var isPrimaryPayer = this.XrmObject().Page.getAttribute("tu_isprimarypayer");
        var isForeigner = this.XrmObject().Page.getAttribute("tu_isforeigner");
        var idNo = this.XrmObject().Page.getAttribute("tu_idno");
        var customerNumber = this.XrmObject().Page.getAttribute("tu_customernumber");
        var firstName = this.XrmObject().Page.getAttribute("tu_firstname");
        var lastName = this.XrmObject().Page.getAttribute("tu_lastname");
        var emailAddress = this.XrmObject().Page.getAttribute("tu_emailaddress1");
        var firstPremium = this.XrmObject().Page.getAttribute("tu_firstpremium");
        var premium = this.XrmObject().Page.getAttribute("tu_premium");
        var mobilePhone = this.XrmObject().Page.getAttribute("tu_mobilephone");
        var dateofBirth = this.XrmObject().Page.getAttribute("tu_birthdate");
        var type = this.XrmObject().Page.getAttribute("tu_type");
        var formType = CrmJs.Common.GetFormType(executionContext);
        if (formType == 1) {
            this.SetInitialDebitDate();
            if (CrmJs.Common.IsValidAttributeControl(isPrimaryPayer)) {

                if (CrmJs.Common.IsValidAttributeControl(customer)) {
                    if (customer.getValue() != null) {
                        var custId = customer.getValue()[0].id;
                        var customerData = this.XrmObject().WebApi.retrieveRecord("contact", custId,
                            "?$select=tu_firstpremium,tu_premium,tu_customernumber,mobilephone,birthdate,tu_isforeigner,tu_idnopassport,firstname,lastname,emailaddress1").
                            then(
                                function success(result) {

                                    if (CrmJs.Common.IsValidAttributeControl(firstPremium))
                                        firstPremium.setValue(result.tu_firstpremium);

                                    if (CrmJs.Common.IsValidAttributeControl(premium))
                                        premium.setValue(result.tu_premium);

                                    if (CrmJs.Common.IsValidAttributeControl(customerNumber))
                                        customerNumber.setValue(result.tu_customernumber);

                                    if (isPrimaryPayer.getValue() == true) {
                                        if (CrmJs.Common.IsValidAttributeControl(isForeigner)) {
                                            isForeigner.setValue(result.tu_isforeigner);
                                            CrmJs.Common.SetDisabled(true, isForeigner.getName());
                                        }
                                        if (CrmJs.Common.IsValidAttributeControl(idNo)) {
                                            idNo.setValue(result.tu_idnopassport);
                                            CrmJs.Common.SetDisabled(true, idNo.getName());
                                        }
                                        if (CrmJs.Common.IsValidAttributeControl(firstName)) {
                                            firstName.setValue(result.firstname);
                                            CrmJs.Common.SetDisabled(true, firstName.getName());
                                        }
                                        if (CrmJs.Common.IsValidAttributeControl(lastName)) {
                                            lastName.setValue(result.lastname);
                                            CrmJs.Common.SetDisabled(true, lastName.getName());
                                        }
                                        if (CrmJs.Common.IsValidAttributeControl(emailAddress)) {
                                            emailAddress.setValue(result.emailaddress1);
                                            CrmJs.Common.SetDisabled(true, emailAddress.getName())
                                        }
                                        if (CrmJs.Common.IsValidAttributeControl(mobilePhone)) {
                                            mobilePhone.setValue(result.mobilephone);
                                            CrmJs.Common.SetDisabled(true, mobilePhone.getName());
                                        }

                                    }

                                    var columns = "_tu_customer_value, tu_accountnumber,statecode,tu_accounttype,tu_bankname,tu_branchcode,tu_isprimarypayer,statecode";
                                    var filter = "_tu_customer_value eq '" + custId + "' and statecode eq 0";
                                    var bankingDetails = CrmJs.Common.RetrieveMultiple("tu_bankaccountdetailses", filter, columns);

                                    if (bankingDetails.value.length > 0) {
                                        if (bankingDetails.value.length == 1) {
                                            isPrimaryPayer.setValue(bankingDetails.value[0].tu_isprimarypayer);
                                            if (CrmJs.Common.IsValidAttributeControl(bank))
                                                CrmJs.Common.SetLookupValue(bank, bankingDetails.value[0].tu_bankaccountdetailsid,
                                                    bankingDetails.value[0].tu_accountnumber, "tu_bankaccountdetails");
                                            CrmJs.Sales.Mandate.SetBankingFieldValues
                                                (bankingDetails.value[0].tu_bankaccountdetailsid);

                                            if (bankingDetails.value[0].tu_isprimarypayer == false) {
                                                if (CrmJs.Common.IsValidAttributeControl(payer))
                                                    payer.setValue(null);
                                                CrmJs.Sales.Mandate.SetBankingFields(executionContext, true, "none");
                                                CrmJs.Sales.Mandate.SetCustomerFields(executionContext, false, "required");
                                                //CrmJs.Common.SetDisabled(true, bank.getName());   
                                            }
                                            else {
                                                CrmJs.Sales.Mandate.SetBankingFields(executionContext, true, "none");
                                                CrmJs.Sales.Mandate.SetCustomerFields(executionContext, true, "none");
                                                CrmJs.Sales.Mandate.SetPayerFieldValues(custId);
                                                if (CrmJs.Common.IsValidAttributeControl(isForeigner)) {
                                                    isForeigner.setValue(result.tu_isforeigner);
                                                }
                                                if (CrmJs.Common.IsValidAttributeControl(mobilePhone)) {
                                                    mobilePhone.setValue(result.mobilephone);
                                                }
                                                if (CrmJs.Common.IsValidAttributeControl(dateofBirth)) {
                                                    dateofBirth.setValue(new Date(result.birthdate));
                                                }
                                            }

                                        }
                                        else {
                                            if (CrmJs.Common.IsValidAttributeControl(bank))
                                                CrmJs.Common.SetDisabled(false, bank.getName());
                                        }

                                    }
                                    else {
                                        if (CrmJs.Common.IsValidAttributeControl(bank))
                                            CrmJs.Common.SetDisabled(false, bank.getName());
                                    }
                                },
                                function (error) {

                                }
                            );
                    }
                }
            }
        }
        else if (formType == 2) {
            this.SetMandateStatus();
            if (CrmJs.Common.IsValidAttributeControl(isPrimaryPayer)) {
                if (isPrimaryPayer.getValue() == false) {
                    CrmJs.Sales.Mandate.SetBankingFields(executionContext, false, "required");
                    CrmJs.Sales.Mandate.SetCustomerFields(executionContext, false, "required");
                }
                else {
                    CrmJs.Sales.Mandate.SetCustomerFields(executionContext, true, "none");
                    CrmJs.Sales.Mandate.SetBankingFields(executionContext, true, "none");
                }
            }
        }
    },

    SetMandateStatus: function () {
        var customer = this.XrmObject().Page.getAttribute("tu_customer");
        if (CrmJs.Common != "undefined") {
            var mandateId = CrmJs.Common.RemoveBrackets(this.XrmObject().Page.data.entity.getId());
            var referenceId = "";
            var statusReason = this.XrmObject().Page.getAttribute("statuscode");
            if (CrmJs.Common.IsValidAttributeControl(customer)) {
                if (customer.getValue() != null) {
                    referenceId = CrmJs.Common.RemoveBrackets(customer.getValue()[0].id);

                    var req = new XMLHttpRequest()
                    req.open("GET", "https://prod-148.westeurope.logic.azure.com/workflows/9d641ca56d1348a2bd8f2f54df023643/triggers/manual/paths/invoke/" + mandateId + "/" + referenceId + "?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=Ntc5EUgjNvOGm-o2ODdL5Fgbvhr0IflsHkSo5Z8mrPU", false);
                    req.setRequestHeader("OData-MaxVersion", "4.0");
                    req.setRequestHeader("OData-Version", "4.0");
                    req.setRequestHeader("Accept", "application/json");
                    req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                    req.setRequestHeader("Prefer", "odata.include-annotations=\"*\"");
                    req.onreadystatechange = function () {
                        if (this.readyState === 4) {
                            req.onreadystatechange = null;
                            if (this.status === 200) {

                                var results = JSON.parse(this.response);
                                switch (results.MandateInformation.MandateStatus) {
                                    case "Awaiting for First Payment":
                                        statusReason.setValue(827210000);
                                        break;
                                    case "Fail":
                                        statusReason.setValue(827210004);
                                        break;
                                    case "Active":
                                        statusReason.setValue(1);
                                        break;
                                    case "Suspended":
                                        statusReason.setValue(827210005);
                                        break;
                                    case "Cancelled":
                                        statusReason.setValue(827210006);
                                        break;

                                    default:
                                        statusReason.setValue(827210003);
                                }
                                
                            }
                            else {
                                    
                            }
                        }
                    };
                    req.send();

                }
            }
        } 
    },

    OnChangeFirstName: function (executionContext) {
        var firstName = this.XrmObject().Page.getAttribute("tu_firstname");
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(firstName))
                CrmJs.Common.CheckLetters(executionContext, firstName.getName());

        }
    },

    OnChangeLastName: function (executionContext) {
        var lastName = this.XrmObject().Page.getAttribute("tu_lastname");
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(lastName))
                CrmJs.Common.CheckLetters(executionContext, lastName.getName());

        }

    },

    CheckForBlacklisting: function (executionContext) {
        var accountNumber = this.XrmObject().Page.getAttribute("tu_accountnumber");
        var idNo = this.XrmObject().Page.getAttribute("tu_idno");
        var saveEvent = executionContext.getEventArgs();
        var formContext = executionContext.getFormContext();
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(accountNumber) &&
                CrmJs.Common.IsValidValue(accountNumber.getValue())) {
                var accountNumberBlacklisted = CrmJs.Common.CheckForAccountBlacklisting(accountNumber);
                if (accountNumberBlacklisted) {
                    formContext.ui.setFormNotification("ERROR.UNFAVORABLE INFORMATION EXISTS", "ERROR",
                        "ACCOUNT_BLACKLISTING");
                    saveEvent.preventDefault();
                }
                else if (CrmJs.Common.IsValidAttributeControl(idNo) &&
                    CrmJs.Common.IsValidValue(idNo.getValue())) {
                    var idNoBlacklisted = CrmJs.Common.CheckForIdBlacklisting(idNo);
                    if (idNoBlacklisted) {
                        formContext.ui.setFormNotification("ERROR.UNFAVORABLE INFORMATION EXISTS", "ERROR",
                            "ACCOUNT_BLACKLISTING");
                        saveEvent.preventDefault();
                    }
                }
                else {
                    formContext.ui.clearFormNotification("ACCOUNT_BLACKLISTING");
                }
            }
        }
    },

    OnChangeMobilePhone: function () {
        var mobilePhone = this.XrmObject().Page.getAttribute("tu_mobilephone");
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(mobilePhone)) {
                CrmJs.Common.ValidateMobilePhone(mobilePhone.getName());
            }
        }
    },

    OnSave: function (executionContext) {
        this.CheckForBlacklisting(executionContext);
    },

    OnActivate: function (SelectedControl, SelectedControlSelectedItemReferences,
        SelectedEntityTypeName) {
        var mandateId = SelectedControlSelectedItemReferences[0].Id;
        var mandate = this.XrmObject().WebApi.retrieveRecord("tu_collection", mandateId,
            "?$select=tu_idno,tu_accountnumber").then(
            function success(result) {
                if (CrmJs.Common != "undefined") {
                    if (CrmJs.Common.IsValidValue(result.tu_idno)) {
                        var idFilter = "tu_idno eq '" + result.tu_idno + "' and statecode eq 0";
                        var IdBlacklistings = CrmJs.Common.RetrieveMultiple("tu_blacklistings", idFilter, "tu_idno");
                        if (IdBlacklistings.value.length > 0) {
                            CrmJs.Sales.Mandate.XrmObject().Page.ui.setFormNotification("ERROR.UNFAVORABLE INFORMATION EXISTS", "ERROR",
                                "IDNUMBER_BLACKLISTING")
                        }
                        else if (CrmJs.Common.IsValidValue(result.tu_accountnumber)) {
                            var accountFilter = "tu_accountnumber eq '" + result.tu_accountnumber +
                                "' and statecode eq 0";
                            var accountBlacklisting = CrmJs.Common.RetrieveMultiple("tu_blacklistings", accountFilter,
                                "tu_accountnumber");
                            if (accountBlacklisting.value.length > 0) {
                                CrmJs.Sales.Mandate.XrmObject().Page.ui.setFormNotification("ERROR.UNFAVORABLE INFORMATION EXISTS", "ERROR",
                                    "ACCOUNTNUMBER_BLACKLISTING")
                            }
                            else {
                                CrmJs.Sales.Mandate.XrmObject().Page.ui.clearFormNotification("IDNUMBER_BLACKLISTING");
                                CrmJs.Sales.Mandate.XrmObject().Page.ui.clearFormNotification("ACCOUNTNUMBER_BLACKLISTING");
                                XrmCore.Commands.Activate.activateRecords(SelectedControl, SelectedControlSelectedItemReferences,
                                    SelectedEntityTypeName);
                            }
                        }
                    }
                    }
            },
            function (error) {
            });
    },

    SetCustomerFields: function (executionContext, disabled, requirement) {
        var isForeigner = this.XrmObject().Page.getAttribute("tu_isforeigner");
        var idno = this.XrmObject().Page.getAttribute("tu_idno");
        var firstName = this.XrmObject().Page.getAttribute("tu_firstname");
        var lastName = this.XrmObject().Page.getAttribute("tu_lastname");
        var emailAddress1 = this.XrmObject().Page.getAttribute("tu_emailaddress1");
        var birthdate = this.XrmObject().Page.getAttribute("tu_birthdate");
        var mobilePhone = this.XrmObject().Page.getAttribute("tu_mobilephone");

        if (CrmJs.Common.IsValidAttributeControl(isForeigner)) {
            CrmJs.Common.SetRequired(executionContext, isForeigner.getName(), requirement);
            CrmJs.Common.SetDisabled(disabled, isForeigner.getName());
        }

        if (CrmJs.Common.IsValidAttributeControl(idno)) {
            CrmJs.Common.SetRequired(executionContext, idno.getName(), requirement);
            CrmJs.Common.SetDisabled(disabled, idno.getName());
        }
        if (CrmJs.Common.IsValidAttributeControl(firstName)) {
            CrmJs.Common.SetRequired(executionContext, firstName.getName(), requirement);
            CrmJs.Common.SetDisabled(disabled, firstName.getName());
        }
        if (CrmJs.Common.IsValidAttributeControl(lastName)) {
            CrmJs.Common.SetRequired(executionContext, lastName.getName(), requirement);
            CrmJs.Common.SetDisabled(disabled, lastName.getName());
        }

        if (CrmJs.Common.IsValidAttributeControl(mobilePhone)) {
            CrmJs.Common.SetDisabled(disabled, mobilePhone.getName());
        }

        if (CrmJs.Common.IsValidAttributeControl(emailAddress1)) {
            CrmJs.Common.SetRequired(executionContext, emailAddress1.getName(), "none");
            CrmJs.Common.SetDisabled(disabled, emailAddress1.getName());
        }
    },

    SetBankingFields: function (executionContext, disabled, requirement) {
        var accountType = this.XrmObject().Page.getAttribute("tu_accounttype");
        var accountNumber = this.XrmObject().Page.getAttribute("tu_accountnumber");
        var bankName = this.XrmObject().Page.getAttribute("tu_bankname");
        var branchCode = this.XrmObject().Page.getAttribute("tu_branchcode");

        if (CrmJs.Common.IsValidAttributeControl(accountType)) {
            CrmJs.Common.SetRequired(executionContext, accountType.getName(), requirement);
            CrmJs.Common.SetDisabled(disabled, accountType.getName());
        }
        if (CrmJs.Common.IsValidAttributeControl(accountNumber)) {
            CrmJs.Common.SetRequired(executionContext, accountNumber.getName(), requirement);
            CrmJs.Common.SetDisabled(disabled, accountNumber.getName());
        }
        if (CrmJs.Common.IsValidAttributeControl(bankName)) {
            CrmJs.Common.SetRequired(executionContext, bankName.getName(), requirement);
            CrmJs.Common.SetDisabled(disabled, bankName.getName());
        }
        if (CrmJs.Common.IsValidAttributeControl(branchCode)) {
            CrmJs.Common.SetRequired(executionContext, branchCode.getName(), requirement);
            CrmJs.Common.SetDisabled(disabled, branchCode.getName());
        }
    },

    OnChangeIsPrimaryPayer: function (executionContext) {
        var isPrimaryPayer = this.XrmObject().Page.getAttribute("tu_isprimarypayer");
        var payer = this.XrmObject().Page.getAttribute("tu_payer");
        var idno = this.XrmObject().Page.getAttribute("tu_idno");
        var firstName = this.XrmObject().Page.getAttribute("tu_firstname");
        var lastName = this.XrmObject().Page.getAttribute("tu_lastname");
        var emailAddress1 = this.XrmObject().Page.getAttribute("tu_emailaddress1");
        var accountType = this.XrmObject().Page.getAttribute("tu_accounttype");
        var accountNumber = this.XrmObject().Page.getAttribute("tu_accountnumber");
        var bankName = this.XrmObject().Page.getAttribute("tu_bankname");
        var branchCode = this.XrmObject().Page.getAttribute("tu_branchcode");
        var customer = this.XrmObject().Page.getAttribute("tu_customer");
        var bank = this.XrmObject().Page.getAttribute("tu_bankdetails");
        var columns = "_tu_customer_value, tu_bankaccountdetailsid ,tu_accountnumber,statecode,tu_accounttype,tu_bankname,tu_branchcode";
        var filter = "";

        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(payer) && CrmJs.Common.IsValidAttributeControl(bank) &&
                CrmJs.Common.IsValidAttributeControl(isPrimaryPayer)) {
                if (isPrimaryPayer.getValue() == false) {
                    payer.setValue(null); bank.setValue(null);
                    CrmJs.Common.SetDisabled(true, bank.getName());
                    this.SetCustomerFields(executionContext, false, "required");
                    this.SetBankingFields(executionContext, false, "required");
                    this.ClearPayerFieldValues();
                    this.ClearBankingFieldValues();
                }
                else {
                    if (CrmJs.Common.IsValidAttributeControl(customer)) {
                        if (customer.getValue() != null) {
                            var custId = customer.getValue()[0].id;
                            CrmJs.Common.SetLookupValue(payer, custId, customer.getValue()[0].name,
                                customer.getValue()[0].entityType);
                            this.SetPayerFieldValues(custId);

                            filter = "_tu_customer_value eq '" + custId + "' and statecode eq 0";
                            var bankingDetails = CrmJs.Common.RetrieveMultiple("tu_bankaccountdetailses", filter, columns);
                            if (bankingDetails != "undefined") {
                                if (bankingDetails.value.length == 1) {
                                    CrmJs.Common.SetLookupValue(bank, bankingDetails.value[0].tu_bankaccountdetailsid,
                                        bankingDetails.value[0].tu_accountnumber, "tu_bankaccountdetails");
                                    this.SetBankingFieldValues(bankingDetails.value[0].tu_bankaccountdetailsid);
                                    CrmJs.Common.SetDisabled(true, bank.getName());
                                }
                                else {
                                    CrmJs.Common.SetDisabled(false, bank.getName());
                                }
                            }
                        }
                    }

                    this.SetCustomerFields(executionContext, true, "none");
                    this.SetBankingFields(executionContext, true, "none");

                }
            }
        }
    },

    OnChangeBank: function () {
        var bank = this.XrmObject().Page.getAttribute("tu_bankdetails");
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(bank)) {
                if (bank.getValue() != null)
                    this.SetBankingFieldValues(bank.getValue()[0].id);
                else
                    this.ClearBankingFieldValues();

            }
        }
    },

    SetPayerFieldValues: function (customerId) {
        var idno = this.XrmObject().Page.getAttribute("tu_idno");
        var firstName = this.XrmObject().Page.getAttribute("tu_firstname");
        var lastName = this.XrmObject().Page.getAttribute("tu_lastname");
        var emailAddress1 = this.XrmObject().Page.getAttribute("tu_emailaddress1");
        var mobilePhone = this.XrmObject().Page.getAttribute("tu_mobilephone");
        var columns = "firstname,lastname,emailaddress1,tu_idnopassport,contactid,mobilephone";
        var filter = "contactid eq '" + customerId + "'";
        if (CrmJs.Common != "undefined") {
            var contact = CrmJs.Common.RetrieveMultiple("contacts", filter, columns);
            if (contact != "undefined") {
                if (contact.value.length > 0) {
                    if (CrmJs.Common.IsValidAttributeControl(idno))
                        idno.setValue(contact.value[0].tu_idnopassport);
                    if (CrmJs.Common.IsValidAttributeControl(firstName))
                        firstName.setValue(contact.value[0].firstname);
                    if (CrmJs.Common.IsValidAttributeControl(lastName))
                        lastName.setValue(contact.value[0].lastname);
                    if (CrmJs.Common.IsValidAttributeControl(emailAddress1))
                        emailAddress1.setValue(contact.value[0].emailaddress1);
                    if (CrmJs.Common.IsValidAttributeControl(mobilePhone))
                        mobilePhone.setValue(contact.value[0].mobilephone)
                }
            }
        }
    },

    ClearPayerFieldValues: function () {
        var idno = this.XrmObject().Page.getAttribute("tu_idno");
        var firstName = this.XrmObject().Page.getAttribute("tu_firstname");
        var lastName = this.XrmObject().Page.getAttribute("tu_lastname");
        var emailAddress1 = this.XrmObject().Page.getAttribute("tu_emailaddress1");
        var mobilePhone = this.XrmObject().Page.getAttribute("tu_mobilephone");
        var birthDate = this.XrmObject().Page.getAttribute("tu_birthdate");

        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(idno))
                idno.setValue(null);
            if (CrmJs.Common.IsValidAttributeControl(firstName))
                firstName.setValue(null);
            if (CrmJs.Common.IsValidAttributeControl(lastName))
                lastName.setValue(null);
            if (CrmJs.Common.IsValidAttributeControl(emailAddress1))
                emailAddress1.setValue(null);
            if (CrmJs.Common.IsValidAttributeControl(mobilePhone))
                mobilePhone.setValue(null);
            if (CrmJs.Common.IsValidAttributeControl(birthDate))
                birthDate.setValue(null);
        }

    },

    SetBankingFieldValues: function (bankId) {
        var accountType = this.XrmObject().Page.getAttribute("tu_accounttype");
        var accountNumber = this.XrmObject().Page.getAttribute("tu_accountnumber");
        var bankName = this.XrmObject().Page.getAttribute("tu_bankname");
        var branchCode = this.XrmObject().Page.getAttribute("tu_branchcode");
        var columns = "_tu_customer_value, tu_bankaccountdetailsid ,tu_accountnumber,statecode,tu_accounttype,tu_bankname,tu_branchcode";
        var filter = " tu_bankaccountdetailsid eq '" + bankId + "'";
        if (CrmJs.Common != "undefined") {
            var bankingDetails = CrmJs.Common.RetrieveMultiple("tu_bankaccountdetailses", filter, columns);
            if (bankingDetails != "undefined") {
                if (bankingDetails.value.length > 0) {
                    if (CrmJs.Common.IsValidAttributeControl(accountType))
                        accountType.setValue(bankingDetails.value[0].tu_accounttype)
                    if (CrmJs.Common.IsValidAttributeControl(accountNumber))
                        accountNumber.setValue(bankingDetails.value[0].tu_accountnumber)
                    if (CrmJs.Common.IsValidAttributeControl(bankName))
                        bankName.setValue(bankingDetails.value[0].tu_bankname)
                    if (CrmJs.Common.IsValidAttributeControl(branchCode))
                        branchCode.setValue(bankingDetails.value[0].tu_branchcode)
                }
            }
        }
    },

    ClearBankingFieldValues: function () {
        var accountType = this.XrmObject().Page.getAttribute("tu_accounttype");
        var accountNumber = this.XrmObject().Page.getAttribute("tu_accountnumber");
        var bankName = this.XrmObject().Page.getAttribute("tu_bankname");
        var branchCode = this.XrmObject().Page.getAttribute("tu_branchcode");

        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(accountType))
                accountType.setValue(null)
            if (CrmJs.Common.IsValidAttributeControl(accountNumber))
                accountNumber.setValue(null)
            if (CrmJs.Common.IsValidAttributeControl(bankName))
                bankName.setValue(null)
            if (CrmJs.Common.IsValidAttributeControl(branchCode))
                branchCode.setValue(null)
        }
    },

    OnChangePayer: function () {
        var payer = this.XrmObject().Page.getAttribute("tu_payer");
        var bank = CrmJs.Sales.Mandate.XrmObject().Page.getAttribute("tu_bankdetails");
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(payer) && CrmJs.Common.IsValidAttributeControl(bank)) {
                CrmJs.Common.SetDisabled(false, bank.getName());
                if (payer.getValue() != null) {
                    this.XrmObject().Page.getControl(bank.getName()).addPreSearch(CrmJs.Sales.Mandate.BankPreSearchHandler);
                }
                else {
                    this.XrmObject().Page.getControl(bank.getName()).removePreSearch(CrmJs.Sales.Mandate.BankPreSearchHandler);
                    bank.setValue(null);
                }
            }
        }
    },

    OnChangeBankName: function (executionContext) {
        if (CrmJs.Common != "undefined") {
            CrmJs.Common.SetFormContextObject(executionContext);
            var bank = CrmJs.Common.FormContextObject.getAttribute("tu_bankname");

            if (!CrmJs.Common.IsValidAttributeControl(bank)) return;

            if (CrmJs.Common.IsValidAttributeControl(bank)
                && CrmJs.Common.IsValidAttributeControl(bank.getValue())) {
                var branchCode = CrmJs.Common.FormContextObject.getAttribute("tu_branchcode");

                branchCode.setValue(bank.getValue().toString());
            }
        }
    },

    OnChangePostponeFirstDebit: function () {
        this.SetInitialDebitDate();
    },

    SetInitialDebitDate: function () {
        var postponeFirstDebit = this.XrmObject().Page.getAttribute("tu_postpone1stdebit");
        var debitDate = this.XrmObject().Page.getAttribute("tu_debitdate");
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(postponeFirstDebit)
                && CrmJs.Common.IsValidAttributeControl(debitDate)) {
                if (postponeFirstDebit.getValue() == false) {
                    debitDate.setValue(new Date());
                }
                else {
                    var date = new Date();
                    date.setMonth(date.getMonth() + 1);
                    debitDate.setValue(date);
                }
            }

        }
    },

    ValidateDebitDate: function () {
        var debitDate = this.XrmObject().Page.getAttribute("tu_debitdate");
        var date = new Date();
        date.setHours(0, 0, 0, 0);
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(debitDate) && debitDate.getValue() != null) {
                var initialDebitDate = new Date(debitDate.getValue());
                initialDebitDate.setHours(0, 0, 0, 0);

                if (initialDebitDate < date)
                    this.XrmObject().Page.getControl("tu_debitdate").setNotification("DEBIT DATE CANNOT BE IN THE PAST");
                else
                    this.XrmObject().Page.getControl("tu_debitdate").clearNotification();
            }
        }
    },

    OnChangeAccountNumber: function () {
        var idNo = this.XrmObject().Page.getAttribute("tu_idno");
        var firstName = this.XrmObject().Page.getAttribute("tu_firstname");
        var lastName = this.XrmObject().Page.getAttribute("tu_lastname");
        var emailAddress1 = this.XrmObject().Page.getAttribute("tu_emailaddress1");
        var payer = this.XrmObject().Page.getAttribute("tu_payer");
        var accountOwner = null;
        var accountType = this.XrmObject().Page.getAttribute("tu_accounttype");
        var accountNumber = this.XrmObject().Page.getAttribute("tu_accountnumber");
        var bankName = this.XrmObject().Page.getAttribute("tu_bankname");
        var branchCode = this.XrmObject().Page.getAttribute("tu_branchcode");
        var bankingDetails = null;
        var bank = this.XrmObject().Page.getAttribute("tu_bankdetails");
        var bankColumns = "_tu_customer_value, tu_bankaccountdetailsid ,tu_accountnumber,statecode,tu_accounttype,tu_bankname,tu_branchcode";
        var contactColumns = "tu_idnopassport,firstname,lastname,emailaddress1";
        var bankFilter = "";
        var contactFilter = "";
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(accountNumber) && accountNumber.getValue() != null) {
                bankFilter = "tu_accountnumber eq '" + accountNumber.getValue() + "' and statecode eq 0";
                bankingDetails = CrmJs.Common.RetrieveMultiple("tu_bankaccountdetailses", bankFilter,
                    bankColumns);
                if (bankingDetails != "undefined") {
                    if (bankingDetails.value.length == 1) {
                        if (CrmJs.Common.IsValidAttributeControl(bank)) {
                            CrmJs.Common.SetLookupValue(bank, bankingDetails.value[0].tu_bankaccountdetailsid,
                                bankingDetails.value[0].tu_accountnumber, "tu_bankaccountdetails");
                        }
                        if (CrmJs.Common.IsValidAttributeControl(accountType))
                            accountType.setValue(bankingDetails.value[0].tu_accounttype)
                        if (CrmJs.Common.IsValidAttributeControl(accountNumber))
                            accountNumber.setValue(bankingDetails.value[0].tu_accountnumber)
                        if (CrmJs.Common.IsValidAttributeControl(bankName))
                            bankName.setValue(bankingDetails.value[0].tu_bankname)
                        if (CrmJs.Common.IsValidAttributeControl(branchCode))
                            branchCode.setValue(bankingDetails.value[0].tu_branchcode)

                        if (bankingDetails.value[0]._tu_customer_value != null) {

                            accountOwner = this.XrmObject().WebApi.retrieveRecord("contact", bankingDetails.value[0]._tu_customer_value,
                                "?$select=tu_idnopassport,firstname,lastname,emailaddress1").then(
                                    function success(result) {
                                        if (CrmJs.Common.IsValidAttributeControl(firstName))
                                            firstName.setValue(result.firstname)

                                        if (CrmJs.Common.IsValidAttributeControl(lastName))
                                            lastName.setValue(result.lastname)

                                        if (CrmJs.Common.IsValidAttributeControl(emailAddress1))
                                            emailAddress1.setValue(result.emailaddress1)
                                        if (CrmJs.Common.IsValidAttributeControl(idNo))
                                            idNo.setValue(result.tu_idnopassport)
                                        if (CrmJs.Common.IsValidAttributeControl(payer))
                                            CrmJs.Common.SetLookupValue(payer, result.contactid, firstName.getValue() + " " + lastName.getValue(),
                                                "contact")

                                    },
                                    function (error) {
                                    });
                        }
                    }
                }
            }
        }
    },

    OnChangeIdNo: function () {
        var isForeigner = this.XrmObject().Page.getAttribute("tu_isforeigner");
        var idNo = this.XrmObject().Page.getAttribute("tu_idno");
        var payer = this.XrmObject().Page.getAttribute("tu_payer");
        var bank = this.XrmObject().Page.getAttribute("tu_bankdetails");
        var birthdate = this.XrmObject().Page.getAttribute("tu_birthdate");
        var idNumberLength = 0;
        var isNumber = false;
        var citizenShip = 0;
        var controlDigit = 0;
        var lastDigit = 0;
        if (CrmJs.Common != "undefinded") {
            if (CrmJs.Common.IsValidAttributeControl(isForeigner) && isForeigner.getValue() == false) {
                if (CrmJs.Common.IsValidAttributeControl(idNo) && idNo.getValue() != null) {
                    idNumberLength = idNo.getValue().length;
                    isNumber = this.IsNumber(idNo.getValue());
                    if (idNumberLength == 13 && isNumber == true) {
                        this.XrmObject().Page.getControl("tu_idno").clearNotification();
                        citizenShip = parseInt(idNo.getValue().charAt(10));
                        controlDigit = this.GetControlDigit(idNo);
                        if (controlDigit.toString().length > 1) {
                            lastDigit = controlDigit.toString().substr(controlDigit.toString().length - 1, 1);
                        }
                        else {
                            lastDigit = controlDigit.toString();
                        }
                        if (citizenShip > 1 || lastDigit != idNo.getValue()[idNumberLength - 1]) {
                            this.XrmObject().Page.getControl("tu_idno").setNotification("INVALID_ID_NUMBER");
                        }
                        else {
                            this.XrmObject().Page.getControl("tu_idno").clearNotification();
                            this.SetCustomerIfExists(idNo);
                            CrmJs.Common.PopulateDOB(idNo, birthdate);


                        }
                    }
                    else {
                        this.XrmObject().Page.getControl("tu_idno").setNotification("INVALID_ID_NUMBER"); 
                    }
                }
            }
        }
    },

    IsNumber: function (str) {
        return !/\D/.test(str);
    },

    SetCustomerIfExists: function (idNumber) {
        var payer = this.XrmObject().Page.getAttribute("tu_payer");
        var bank = this.XrmObject().Page.getAttribute("tu_bankdetails");
        var firstName = this.XrmObject().Page.getAttribute("tu_firstname");
        var lastName = this.XrmObject().Page.getAttribute("tu_lastname");
        var emailAddress1 = this.XrmObject().Page.getAttribute("tu_emailaddress1");
        var accountType = this.XrmObject().Page.getAttribute("tu_accounttype");
        var accountNumber = this.XrmObject().Page.getAttribute("tu_accountnumber");
        var bankName = this.XrmObject().Page.getAttribute("tu_bankname");
        var branchCode = this.XrmObject().Page.getAttribute("tu_branchcode");
        var columns = "tu_idnopassport,firstname,lastname,emailaddress1";
        var filter = "tu_idnopassport eq '" + idNumber.getValue() + "' and statecode eq 0";
        if (CrmJs.Common != "undefined") {
            var customer = CrmJs.Common.RetrieveMultiple("contacts", filter, columns);
            var bankingDetails = null;
            var bankColumns = "_tu_customer_value, tu_accountnumber,statecode,tu_accounttype,tu_bankname,tu_branchcode";
            var bankFilter = "";
            if (customer.value.length > 0) {

                if (CrmJs.Common.IsValidAttributeControl(firstName))
                    firstName.setValue(customer.value[0].firstname)

                if (CrmJs.Common.IsValidAttributeControl(lastName))
                    lastName.setValue(customer.value[0].lastname)

                if (CrmJs.Common.IsValidAttributeControl(emailAddress1))
                    emailAddress1.setValue(customer.value[0].emailaddress1)

                if (CrmJs.Common.IsValidAttributeControl(payer))
                    CrmJs.Common.SetLookupValue(payer, customer.value[0].contactid, customer.value[0].firstname + " " +
                        customer.value[0].lastname, "contact")



                bankFilter = "_tu_customer_value eq '" + customer.value[0].contactid + "' and statecode eq 0";
                bankingDetails = CrmJs.Common.RetrieveMultiple("tu_bankaccountdetailses", bankFilter, bankColumns);
                if (bankingDetails != "undefined") {
                    if (bankingDetails.value.length == 1) {

                        if (CrmJs.Common.IsValidAttributeControl(accountType))
                            accountType.setValue(bankingDetails.value[0].tu_accounttype)
                        if (CrmJs.Common.IsValidAttributeControl(accountNumber))
                            accountNumber.setValue(bankingDetails.value[0].tu_accountnumber)
                        if (CrmJs.Common.IsValidAttributeControl(bankName))
                            bankName.setValue(bankingDetails.value[0].tu_bankname)
                        if (CrmJs.Common.IsValidAttributeControl(branchCode))
                            branchCode.setValue(bankingDetails.value[0].tu_branchcode)
                        if (CrmJs.Common.IsValidAttributeControl(bank))
                            CrmJs.Common.SetLookupValue(bank, bankingDetails.value[0].tu_bankaccountdetailsid,
                                bankingDetails.value[0].tu_accountnumber, "tu_bankaccountdetails")
                    }
                }
            }
        }
    },

    BankPreSearchHandler: function () {
        var bank = CrmJs.Sales.Mandate.XrmObject().Page.getAttribute("tu_bankdetails");
        var payer = CrmJs.Sales.Mandate.XrmObject().Page.getAttribute("tu_payer");
        var fetchXML = "";
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidAttributeControl(bank) && CrmJs.Common.IsValidAttributeControl(payer)) {
                fetchXML = "<filter type='and'><condition attribute = 'tu_customer' operator= 'eq' value = '" + payer.getValue()[0].id + "' /></filter >";
                CrmJs.Common.AddCustomLookupFilter(bank.getName(), fetchXML);
            }
        }
    },


    PopulateDebitDate: function (executionContext) {
        if (CrmJs.Common != "undefined") {
            CrmJs.Common.SetFormContextObject(executionContext);
            var salaryPayDay = this.XrmObject().Page.getAttribute("tu_collectionday");

            if (CrmJs.Common.IsValidAttributeControl(salaryPayDay)
                && CrmJs.Common.IsValidAttributeControl(salaryPayDay.getValue())) {
                var dateToday = new Date();
                var debitDate = this.XrmObject().Page.getAttribute("tu_debitdate");

                if (CrmJs.Common.IsValidAttributeControl(debitDate)
                    && dateToday.getDate() > salaryPayDay.getValue()
                    && !CrmJs.Common.IsValidAttributeControl(debitDate.getValue())) {
                    debitDate.setValue(new Date(dateToday.getFullYear(), (dateToday.getMonth() + 1), salaryPayDay.getValue()));
                } else if (CrmJs.Common.IsValidAttributeControl(debitDate)
                    && !CrmJs.Common.IsValidAttributeControl(debitDate.getValue())) {
                    debitDate.setValue(new Date(dateToday.getFullYear(), dateToday.getMonth(), salaryPayDay.getValue()));
                }
            }
        }
    },

    PopCollection: function (SelectedEntityTypeName, PrimaryEntityTypeName, PrimaryControl, SelectedControl) {
        if (CrmJs.Common != "undefined") {
            if (CrmJs.Common.IsValidValue(SelectedControl)) {
                var parameters = {};



                parameters["tu_customer"] = PrimaryControl;
                parameters["tu_customername"] = SelectedControl.data.entity.getEntityReference().name;
                parameters["tu_customertype"] = PrimaryEntityTypeName;



                parameters["tu_payer"] = PrimaryControl;
                parameters["tu_payername"] = SelectedControl.data.entity.getEntityReference().name;
                parameters["tu_payertype"] = PrimaryEntityTypeName;



                var pageInput = {
                    pageType: "entityrecord",
                    entityName: SelectedEntityTypeName,
                    data: parameters,
                    //entityId: ,
                    //formId: entityName == "contact" ?  :,
                };



                Xrm.Navigation.navigateTo(pageInput, CrmJs.Sales.Mandate.NavigationOptions()).then(
                    function success() {
                        SelectedControl.data.refresh();
                    },
                    function error(error) {
                        //CrmJs.Sales.Mandate.FormContextObject.ui.setFormNotification(error.message, "ERROR", "CREATE_BEN_ERROR");
                    }
                );
            }
        }
    },

    GetControlDigit: function (idNumber) {
        var d = -1;
        var a = 0;
        for (var i = 0; i < 6; i++) {
            a += parseInt(idNumber.getValue()[2 * i]);
        }

        var b = 0;
        for (var i = 0; i < 6; i++) {
            b = b * 10 + parseInt(idNumber.getValue()[2 * i + 1]);
        }
        b *= 2;

        var result = b.toString();
        var c = 0;
        for (var i = 0; i < result.length; i++) {
            c += parseInt(result[i]);
        }
        var lastSum = (a + c).toString();
        d = 10 - Int(lastSum[lastSum.length - 1]);

        return d;
    },

    NavigationOptions: function () {
        var navigationOptions = {
            target: 2,
            height: { value: 100, unit: "%" },
            width: { value: 100, unit: "%" },
            position: 1
        };

        return navigationOptions;
    }
}